#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_ezdronegps
~~~~~~~~~~~~~~~

The test script to test EZDrone GPS related functions.

:author: YM
:version: 0.1.0
:copyright:
:license:
"""

from ezdrone import EZDrone, EZDroneUtil


def nav_output_listener(self, name, msg):
    if name != 'NAV_CONTROLLER_OUTPUT':
        return

    print('[NAV] roll={msg.nav_roll}, pitch={msg.nav_pitch}, bearing={msg.nav_bearing}, target_bearing={msg.target_bearing}, wp_dist={msg.wp_dist}'.format(msg=msg))
    return


print('Connect to drone')
drone = EZDrone(connection_string='127.0.0.1:14550')

drone_status = drone.system_status
print('Current drone status: {}'.format(drone_status))

# Switch to the flight mode you want to test
test_mode = 'GUIDED'
print('Switch to {} mode'.format(test_mode))
return_code = drone.switch_mode(test_mode)
print('...return={}'.format(return_code))

flight_mode = drone.mode
print('Current mode: {}'.format(flight_mode))

# Arm the drone and takeoff
takeoff_height = 20
print('Auto arm & takeoff to {}m'.format(takeoff_height))
return_code = drone.takeoff(takeoff_height, auto_arm=True)
print('...return={}'.format(return_code))

current_height = drone.height
print('Current height: {}'.format(current_height))

# Move the drone to specified GPS location
current_location = drone.gps_location
print('Current location: {}'.format(current_location))

drone.add_message_listener('NAV_CONTROLLER_OUTPUT', nav_output_listener)

target_location = EZDroneUtil.get_gps_location(current_location[0], current_location[1], 20, 30) + (30,)
print('Move to GPS location {}'.format(target_location))
return_code = drone.move_to_gps_location(
    target_location[0], target_location[1], target_location[2], 5, relative=True)
print('...return={}'.format(return_code))

drone.remove_message_listener('NAV_CONTROLLER_OUTPUT', nav_output_listener)

# Land and disarm
print('Land & auto disarm')
return_code = drone.land(auto_disarm=True)
print('...return={}'.format(return_code))
