#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_move_by_velocity
~~~~~~~~~~~~~~~~~~~~~

The test script to test move_by_velocity function.

:author: YM
:version: 0.1.0
:copyright:
:license:
"""

from ezdrone import EZDrone, EZDroneUtil
import math
import time


print('Connect to drone')
drone = EZDrone(connection_string='127.0.0.1:14550')

if drone.mode == 'GUIDED':
    # Move the drone by velocity
    v = 1.0  # velocity in m/s
    move_params = (
        (v, 0, 0, 30, 5, True),
        ((v * math.sin(math.pi/4)), (v * math.cos(math.pi/4)), 0, 45, 5, True),
        (0, v, 0, 60, 5, True),
        ((-v * math.sin(math.pi/4)), (v * math.cos(math.pi/4)), 0, 90, 5, True),
        (-v, 0, 0, 120, 5, True),
        ((-v * math.sin(math.pi/4)), (-v * math.cos(math.pi/4)), 0, 180, 5, True),
        (0, -v, 0, 240, 5, True),
        ((v * math.sin(math.pi/4)), (-v * math.cos(math.pi/4)), 0, 270, 5, True))
    for param in move_params:
        vx, vy, vz, heading, duration, use_ned = param
        print('Move drone by velocity (vx={}, vy={}, vz={}, heading={}, duration={}, use_ned={})'.format(
            vx, vy, vz, heading, duration, use_ned))
        task_id = drone.move_by_velocity(
            vx, vy, vz, heading, duration, use_ned=use_ned, wait_done=False)
        print('...task_id={}, task_state={}'.format(
            task_id, drone.get_task_state(task_id)))
        while drone.get_task_state(task_id) != drone.TASK_DONE:
            print('...vx={v[0]}, vy={v[1]}, vz={v[2]}, heading={heading}'.format(
                v=drone.velocity, heading=drone.heading))
            print('...roll={rpy[0]}, pitch={rpy[1]}, yaw={rpy[2]}, h={h}'.format(
                rpy=drone.attitude, h=drone.height))
            time.sleep(1)
