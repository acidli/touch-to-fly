#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_ezdrone
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The test script to test EZDrone.

:author: YM
:version: 0.4.0
:copyright:
:license:
"""

from ezdrone.droneproxy import EZDrone
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] [%(asctime)s] [%(thread)d]: %(module)s: %(message)s')


print('Connect to drone')
drone = EZDrone(connection_string='127.0.0.1:14550')

drone_status = drone.system_status
print('Current drone status: {}'.format(drone_status))

# Switch to the flight mode you want to test
test_mode = 'GUIDED'
print('Switch to {} mode'.format(test_mode))
return_code = drone.switch_mode(test_mode)
print('...return={}'.format(return_code))

flight_mode = drone.mode
print('Current mode: {}'.format(flight_mode))

# Arm the drone and takeoff
print('Arm')
return_code = drone.arm_disarm(True)
print('...return={}'.format(return_code))

takeoff_height = 20
print('Takeoff to {}m'.format(takeoff_height))
return_code = drone.takeoff(takeoff_height)
print('...return={}'.format(return_code))

current_height = drone.height
print('Current height: {}'.format(current_height))

# Move the drone by velocity
move_params = (
    (2, 3, -1, 20, 5, False),
    (2, -3, 1, 10, 5, False),
    (-2, -3, -1, 15, 5, False),
    (-2, 3, 1, 30, 5, False))
for param in move_params:
    vx, vy, vz, heading, duration, use_ned = param
    print('Move drone by velocity (vx={}, vy={}, vz={}, heading={}, duration={}, use_ned={})'.format(
        vx, vy, vz, heading, duration, use_ned))
    return_code = drone.move_by_velocity(vx, vy, vz, heading, duration, use_ned=use_ned)
    print('...return={}'.format(return_code))

move_params = (
    (2, 3, -1, 30, 5, True),
    (2, -3, 1, 60, 5, True),
    (-2, -3, -1, 90, 5, True),
    (-2, 3, 1, 120, 5, True))
for param in move_params:
    vx, vy, vz, heading, duration, use_ned = param
    print('Move drone by velocity (vx={}, vy={}, vz={}, heading={}, duration={}, use_ned={})'.format(
        vx, vy, vz, heading, duration, use_ned))
    task_id = drone.move_by_velocity(
        vx, vy, vz, heading, duration, use_ned=use_ned, wait_done=False)
    print('...task_id={}, task_state={}'.format(
        task_id, drone.get_task_state(task_id)))
    while drone.get_task_state(task_id) != drone.TASK_DONE:
        print('...vx={v[0]}, vy={v[1]}, vz={v[2]}, heading={heading}'.format(
            v=drone.velocity, heading=drone.heading))
        print('...roll={rpy[0]}, pitch={rpy[1]}, yaw={rpy[2]}, h={h}'.format(
            rpy=drone.attitude, h=drone.height))
        time.sleep(1)

# Move the drone to position
move_params = (
    (-10, -15, -5, 3.5, False),
    (-10, 15, 5, 3.5, False),
    (10, 15, -5, 3.5, False),
    (10, -15, 5, 3.5, False))
for param in move_params:
    x, y, z, velocity, use_ned = param
    print('Move drone to position (x={}, y={}, z={}, velocity={}, use_ned={})'.format(
        x, y, z, velocity, use_ned))
    return_code = drone.move_to_position(x, y, z, velocity, use_ned=use_ned)
    print('...return={}'.format(return_code))

move_params = (
    (-10, -15, -5, 3.5, True),
    (-10, 15, 5, 3.5, True),
    (10, 15, -5, 3.5, True),
    (10, -15, 5, 3.5, True))
for param in move_params:
    x, y, z, velocity, use_ned = param
    print('Move drone to position (x={}, y={}, z={}, velocity={}, use_ned={})'.format(
        x, y, z, velocity, use_ned))
    task_id = drone.move_to_position(
        x, y, z, velocity, use_ned=use_ned, wait_done=False)
    print('...task_id={}, task_state={}'.format(
        task_id, drone.get_task_state(task_id)))
    while drone.get_task_state(task_id) != drone.TASK_DONE:
        print('...vx={v[0]}, vy={v[1]}, vz={v[2]}, heading={heading}'.format(
            v=drone.velocity, heading=drone.heading))
        print('...roll={rpy[0]}, pitch={rpy[1]}, yaw={rpy[2]}, h={h}'.format(
            rpy=drone.attitude, h=drone.height))
        time.sleep(1)

# Rotate the drone heading
heading, use_ned = 120, False
print('Turn the drone heading (heading={}, use_ned={})'.format(
    heading, use_ned))
return_code = drone.turn_heading(heading, use_ned)
print('...return={}'.format(return_code))

heading, use_ned = 270, True
print('Turn the drone heading (heading={}, use_ned={})'.format(
    heading, use_ned))
task_id = drone.turn_heading(heading, use_ned, wait_done=False)
print('...task_id={}, task_state={}'.format(
    task_id, drone.get_task_state(task_id)))
while drone.get_task_state(task_id) != drone.TASK_DONE:
    print('...vx={v[0]}, vy={v[1]}, vz={v[2]}, heading={heading}'.format(
        v=drone.velocity, heading=drone.heading))
    print('...roll={rpy[0]}, pitch={rpy[1]}, yaw={rpy[2]}, h={h}'.format(
        rpy=drone.attitude, h=drone.height))
    time.sleep(1)

# Land and disarm
print('Land')
return_code = drone.land()
print('...return={}'.format(return_code))

print('Disarm')
return_code = drone.arm_disarm(False)
print('...return={}'.format(return_code))

# Display current system status
drone_status = drone.system_status
print('Current drone status: {}'.format(drone_status))

# Switch to the flight mode before takeoff
test_mode = 'GUIDED'
print('Switch to {} mode'.format(test_mode))
return_code = drone.switch_mode(test_mode)
print('...return={}'.format(return_code))

flight_mode = drone.mode
print('Current mode: {}'.format(flight_mode))

# Simple takeoff and land
takeoff_alt = 20
print('Auto arm & takeoff to {}m'.format(takeoff_alt))
task_id = drone.takeoff(takeoff_alt, auto_arm=True, wait_done=False)
print('...task_id={}, task_state={}'.format(
    task_id, drone.get_task_state(task_id)))
task_state = drone.get_task_state(task_id)
while task_state is not None and task_state != drone.TASK_DONE:
    print('...h={h}, sys_status={status}'.format(
        h=drone.height, status=drone.system_status))
    time.sleep(1)
    task_state = drone.get_task_state(task_id)

drone_status = drone.system_status
print('Current drone status: {}'.format(drone_status))

print('Land & auto disarm')
task_id = drone.land(auto_disarm=True, wait_done=False)
print('...task_id={}, task_state={}'.format(
    task_id, drone.get_task_state(task_id)))
task_state = drone.get_task_state(task_id)
while task_state is not None and task_state != drone.TASK_DONE:
    print('...h={h}, sys_status={status}'.format(
        h=drone.height, status=drone.system_status))
    time.sleep(1)
    task_state = drone.get_task_state(task_id)

drone_status = drone.system_status
print('Current drone status: {}'.format(drone_status))
