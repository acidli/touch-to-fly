^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ezdrone
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.8.0 (2019-05-03)
------------------
* Change the examples to use updated move_by_velocity API.
* Modify move_by_velocity API to support the heading parameter and add new properties to access MavLink parameters and RC channels.

0.7.0 (2018-11-22)
------------------
* First release
* Add test module to test EZDrone GPS related functions.
* Implement the newly defined EZDroneGpsAPI.
* Define EZDroneGpsAPI.
* Implement the utility class EZDroneUtil.
* Add EZDroneMavlinkAPI definition and implementation.
* Lock move-related tasks to allow one task to be completed at a time.
* Support non blocked move operation and the NED coordinate system.
* Add EZDroneGeneric as the base class to process generic functions.
* Use the proxy/actor mode and implement the actor for the guided mode.
* Add EZDroneAPI definition and implementation.
