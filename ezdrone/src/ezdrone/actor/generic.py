#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
ezdrone.actor.generic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The implementation of EZDroneAPI.
It provides generic control by DroneKit.

:author: YM
:version: 0.6.0
:copyright:
:license:
"""

from .base import EZDroneBase
from ..droneapi import EZDroneMavlinkAPI
import threading
import logging


class EZDroneGeneric(EZDroneBase, EZDroneMavlinkAPI):
    def __init__(self, dronekit_vehicle):
        super(EZDroneGeneric, self).__init__(dronekit_vehicle)

        self.__message_listeners = {}
        self.__message_listeners_lock = threading.RLock()
        return

    def __mavlink_message_listener(self, dronekit_vehicle, name, message):
        logging.debug('mavlink_message_listener: {} is receieved'.format(name))
        with self.__message_listeners_lock:
            for msg_name in (name, '*'):
                if self.__message_listeners.has_key(msg_name):
                    for listener in self.__message_listeners[msg_name]:
                        logging.debug(
                            'mavlink_message_listener: calling {}'.format(listener))
                        listener(self, name, message)
                        logging.debug(
                            'mavlink_message_listener: {} is end'.format(listener))
        return

    def takeoff(self, height, auto_arm, wait_done):
        logging.warn('takeoff: not supported')
        return self.RET_NOT_SUPPORTED

    def land(self, auto_disarm, wait_done):
        logging.warn('land: not supported')
        return self.RET_NOT_SUPPORTED

    def move_by_velocity(self, velocity_x, velocity_y, velocity_z, heading, duration, use_ned, wait_done):
        logging.warn('move_by_velocity: not supported')
        return self.RET_NOT_SUPPORTED

    def move_to_position(self, x, y, z, velocity, use_ned, wait_done):
        logging.warn('move_to_position: not supported')
        return self.RET_NOT_SUPPORTED

    def move_by_attitude(self, roll, pitch, yaw, height, duration, wait_done):
        logging.warn('move_by_attitude: not supported')
        return self.RET_NOT_SUPPORTED

    def move_by_direction(self, forward, right, down, duration, wait_done):
        logging.warn('move_by_direction: not supported')
        return self.RET_NOT_SUPPORTED

    def turn_heading(self, heading, use_ned, wait_done):
        logging.warn('turn_heading: not supported')
        return self.RET_NOT_SUPPORTED

    @property
    def moving(self):
        logging.warn('moving: not supported')
        return None

    def add_message_listener(self, message_name, listener):
        message_name = str(message_name)
        logging.debug('add_message_listener: message_name={}, listener={}'.format(
            message_name, listener))

        with self.__message_listeners_lock:
            if not self.__message_listeners.has_key(message_name):
                self.__message_listeners[message_name] = set()
            listener_set = self.__message_listeners[message_name]
            listener_set.add(listener)
            self._drone.add_message_listener(
                message_name, self.__mavlink_message_listener)
        return self.RET_SUCCESS

    def remove_message_listener(self, message_name, listener):
        message_name = str(message_name)
        logging.debug('remove_message_listener: message_name={}, listener={}'.format(
            message_name, listener))

        with self.__message_listeners_lock:
            if self.__message_listeners.has_key(message_name):
                listener_set = self.__message_listeners[message_name]
                listener_set.discard(listener)
                if len(listener_set) == 0:
                    self._drone.remove_message_listener(
                        message_name, self.__mavlink_message_listener)
        return self.RET_SUCCESS
    
    @property
    def parameters(self):
        return self._drone.parameters
    
    @property
    def channels(self):
        return self._drone.channels
