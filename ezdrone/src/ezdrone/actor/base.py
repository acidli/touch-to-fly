#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
ezdrone.actor.base
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The implementation of EZDroneAPI.
It is the abstract class which provides base functions.

:author: YM
:version: 0.4.0
:copyright:
:license:
"""

from ..droneapi import EZDroneAPI
from dronekit import Vehicle, VehicleMode
import logging
import time
import threading


class EZDroneBase(EZDroneAPI):
    class Task(threading.Thread):
        # the dictionary used to store task_id/task_state as key/value
        __task_states = {}
        # the lock used to access __task_states
        __task_states_lock = threading.RLock()

        @staticmethod
        def get_task_state(task_id):
            with EZDroneBase.Task._Task__task_states_lock:
                state = EZDroneBase.Task._Task__task_states[task_id] if EZDroneBase.Task._Task__task_states.has_key(
                    task_id) else None
                logging.debug('get_task_state({}): {}'.format(task_id, state))
            return state

        def __init__(self, parent, group=None, target=None, name=None, args=(), kwargs={}):
            if not isinstance(parent, EZDroneBase):
                raise TypeError('parent is not the instance of EZDroneBase')
            super(EZDroneBase.Task, self).__init__(
                group, target, name, args, kwargs)

            self.__parent = parent
            return

        def start(self):
            super(EZDroneBase.Task, self).start()
            with self.__task_states_lock:
                self.__task_states[self.ident] = self.__parent.TASK_STARTED
            logging.debug('Task-{} is started'.format(self.name))
            return

        def run(self):
            with self.__task_states_lock:
                self.__task_states[self.ident] = self.__parent.TASK_RUNNING
            logging.debug('Task-{} is running'.format(self.name))

            super(EZDroneBase.Task, self).run()

            with self.__task_states_lock:
                self.__task_states[self.ident] = self.__parent.TASK_DONE
            logging.debug('Task-{} is done'.format(self.name))
            return

    def __init__(self, dronekit_vehicle):
        if not isinstance(dronekit_vehicle, Vehicle):
            raise TypeError('Vehicle instance from DroneKit is necessary')

        # dronekit vehicle object
        self._drone = dronekit_vehicle
        return

    def switch_mode(self, mode):
        mode = str(mode)
        logging.debug('switch_mode: {}'.format(mode))

        try:
            new_mode = VehicleMode(mode)
            start_time = time.time()
            if self._drone.mode != new_mode:
                self._drone.mode = new_mode
            while self._drone.mode != new_mode:
                elapse = time.time() - start_time
                if elapse >= 3:  # 3 seconds timeout
                    logging.warn('switch: timeout ({}s)'.format(elapse))
                    return self.RET_TIMEOUT
                logging.debug('Waiting for mode change...({}s)'.format(elapse))
                time.sleep(0.2)
            else:
                logging.info('switch: mode={} ({}s)'.format(
                    new_mode.name, (time.time() - start_time)))
                return self.RET_SUCCESS
        except KeyError:
            logging.error('Unsupported mode: {}'.format(mode))
            return self.RET_FAILURE

    def arm_disarm(self, do_arming):
        do_arming = bool(do_arming)
        logging.debug('arm_disarm: {}'.format(do_arming))

        if do_arming:
            logging.debug('Basic pre-arm checks')
            # Don't try to arm until autopilot is ready
            start_time = time.time()
            while not self._drone.is_armable:
                elapse = time.time() - start_time
                if elapse >= 10:  # 10 seconds timeout
                    logging.error('The drone is not armable')
                    return self.RET_FAILURE
                logging.debug(
                    'Waiting for vehicle to initialise...({}s)'.format(elapse))
                time.sleep(0.5)

        logging.debug('{} motors'.format(
            'Arming' if do_arming else 'Disarming'))
        start_time = time.time()
        self._drone.armed = do_arming
        while self._drone.armed != do_arming:
            elapse = time.time() - start_time
            if elapse >= 10:  # 10 seconds timeout
                logging.warn('arm_disarm: {} timeout ({}s)'.format(
                    ('arm' if self._drone.armed else 'disarm'), elapse))
                return self.RET_TIMEOUT
            logging.debug(
                'Waiting for {}...({}s)'.format(('arming' if do_arming else 'disarming'), elapse))
            time.sleep(0.5)

        logging.info('arm_disarm: {} ({}s)'.format(
            ('arm' if self._drone.armed else 'disarm'), (time.time() - start_time)))
        return self.RET_SUCCESS

    def get_task_state(self, task_id):
        return self.Task.get_task_state(task_id)

    @property
    def system_status(self):
        return self._drone.system_status.state

    @property
    def mode(self):
        return self._drone.mode.name

    @property
    def attitude(self):
        return self._drone.attitude.roll, self._drone.attitude.pitch, self._drone.attitude.yaw

    @property
    def height(self):
        return self._drone.location.global_relative_frame.alt

    @property
    def velocity(self):
        return tuple(self._drone.velocity)

    @property
    def heading(self):
        return self._drone.heading
