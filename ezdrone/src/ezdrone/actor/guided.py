#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
ezdrone.actor.guided
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The implementation of EZDroneAPI.
It provides basic movement control at APM guided mode by DroneKit.

:author: YM
:version: 0.7.0
:copyright:
:license:
"""

from .base import EZDroneBase
from ..droneapi import EZDroneGpsAPI
from dronekit import VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil
import logging
import time
import threading
import math


class EZDroneGuided(EZDroneBase, EZDroneGpsAPI):

    def __init__(self, dronekit_vehicle):
        super(EZDroneGuided, self).__init__(dronekit_vehicle)

        # the lock used to do move related task
        self.__move_lock = threading.RLock()
        # the event used to notify the task to abort
        self.__move_abort_event = threading.Event()
        # indicates whether the move related task is running
        self.__moving = False
        return

    def _start_async_task(self, group=None, target=None, name=None, args=(), kwargs={}):
        '''Create a new task and start it, then return its task ID'''
        if name is None and not target is None:
            try:
                name = target.__name__
            except:
                pass
        task = self.Task(self, group, target, name, args, kwargs)
        task.start()
        return task.ident

    #===========================#
    # EZDroneAPI Implementation #
    #===========================#
    def takeoff(self, height, auto_arm, wait_done):
        height = float(height)
        auto_arm = bool(auto_arm)
        wait_done = bool(wait_done)
        logging.debug('takeoff: h={}, auto_arm={}, wait={}'.format(
            height, auto_arm, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.takeoff, args=(height, auto_arm, True))

        # Confirm vehicle armed before attempting to take off
        if not self._drone.armed and (not auto_arm or self.arm_disarm(True) != self.RET_SUCCESS):
            logging.error('The drone is not armed')
            return self.RET_FAILURE

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug('takeoff: moving={}'.format(self.__moving))

            start_time = time.time()
            self._drone.simple_takeoff(height)

            # Wait until the vehicle reaches the specified height
            abort = self.__move_abort_event.is_set()
            while wait_done and not abort:
                if (height - self.height) <= 0.5:
                    logging.info('takeoff: reached target height {} ({}s)'.format(
                        height, (time.time() - start_time)))
                    break
                logging.debug('takeoff: cur_h={}, sys_status={}'.format(
                    self.height, self.system_status))
                abort = self.__move_abort_event.wait(0.5)
                if abort:
                    logging.info('takeoff: abort ({}s)'.format(
                        (time.time() - start_time)))

            self.__moving = False
            logging.debug('takeoff: moving={}'.format(self.__moving))

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    def land(self, auto_disarm, wait_done):
        auto_disarm = bool(auto_disarm)
        wait_done = bool(wait_done)
        logging.debug("land: auto_disarm={}, wait={}".format(
            auto_disarm, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.land, args=(auto_disarm, True))

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug('land: moving={}'.format(self.__moving))

            # self._drone.mode = VehicleMode("LAND")
            # create the NAV_LAND command using command_long_encode()
            msg = self._drone.message_factory.command_long_encode(
                0, 0,    # target system, target component
                mavutil.mavlink.MAV_CMD_NAV_LAND,  # command
                0,  # confirmation
                0, 0, 0,    # param 1 ~ 3 not used
                0,          # param 4, desired yaw angle (ignored in APM)
                0,          # param 5, target latitude. If zero, the Copter will land at the current latitude
                0,          # param 6, longitude
                0)          # param 7, altitude (ignored in APM)

            start_time = time.time()
            # send command to vehicle
            self._drone.send_mavlink(msg)

            # Wait until the vehicle landed on the ground
            abort = self.__move_abort_event.is_set()
            while wait_done and not abort:
                if not self._drone.armed or self.system_status == "STANDBY" or self.height <= 0.05:
                    logging.info('land: reached the ground ({}s)'.format(
                        (time.time() - start_time)))
                    break
                logging.debug('land: cur_h={}, sys_status={}'.format(
                    self.height, self.system_status))
                abort = self.__move_abort_event.wait(0.5)
                if abort:
                    logging.info('land: abort ({}s)'.format(
                        (time.time() - start_time)))

            self.__moving = False
            logging.debug('land: moving={}'.format(self.__moving))

            if auto_disarm and self.arm_disarm(False) != self.RET_SUCCESS:
                logging.error('The drone is failed to disarm')
                return self.RET_FAILURE

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    def move_by_velocity(self, velocity_x, velocity_y, velocity_z, heading, duration, use_ned, wait_done):
        """
        Move vehicle in direction based on specified velocity vectors and
        for the specified duration.

        This uses the SET_POSITION_TARGET_LOCAL_NED command with a type mask enabling only 
        velocity components 
        (http://dev.ardupilot.com/wiki/copter-commands-in-guided-mode/#set_position_target_local_ned).

        Note that from AC3.3 the message should be re-sent every second (after about 3 seconds
        with no message the velocity will drop back to zero). In AC3.2.1 and earlier the specified
        velocity persists until it is canceled. The code below should work on either version 
        (sending the message multiple times does not cause problems).

        See the above link for information on the type_mask (0=enable, 1=ignore). 
        At time of writing, acceleration and yaw bits are ignored.
        """
        velocity_x = float(velocity_x)
        velocity_y = float(velocity_y)
        velocity_z = float(velocity_z)
        heading = float(heading)
        duration = float(duration)
        use_ned = bool(use_ned)
        wait_done = bool(wait_done)
        logging.debug('move_by_velocity: vx={}, vy={}, vz={}, heading={}, duration={}, use_ned={}, wait={}'.format(
            velocity_x, velocity_y, velocity_z, heading, duration, use_ned, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.move_by_velocity, args=(velocity_x, velocity_y, velocity_z, heading, duration, use_ned, True))

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug('move_by_velocity: moving={}'.format(self.__moving))

            frame = mavutil.mavlink.MAV_FRAME_LOCAL_OFFSET_NED if use_ned else mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED
            msg = self._drone.message_factory.set_position_target_local_ned_encode(
                0,       # time_boot_ms (not used)
                0, 0,    # target system, target component
                frame,  # frame
                0b0000101111000111,  # type_mask (only speeds and yaw enabled)
                0, 0, 0,  # x, y, z positions (not used)
                velocity_x, velocity_y, velocity_z,  # x, y, z velocity in m/s
                # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
                0, 0, 0,
                math.radians(heading), 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
            msg_stop = self._drone.message_factory.set_position_target_local_ned_encode(
                0,       # time_boot_ms (not used)
                0, 0,    # target system, target component
                frame,  # frame
                0b0000111111000111,  # type_mask (only speeds enabled)
                0, 0, 0,  # x, y, z positions (not used)
                0, 0, 0,  # x, y, z velocity in m/s
                # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
                0, 0, 0,
                0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)

            abort = self.__move_abort_event.is_set()
            start_time = time.time()
            while wait_done and not abort:
                # send command to vehicle on 2 Hz cycle
                self._drone.send_mavlink(msg)
                logging.debug('move_by_velocity: elapse={}'.format(
                    (time.time() - start_time)))
                abort = self.__move_abort_event.wait(0.5)
                elapse = time.time() - start_time
                if elapse >= duration:
                    # stop the drone
                    self._drone.send_mavlink(msg_stop)
                    logging.info(
                        'move_by_velocity: reached the duration ({}s)'.format(elapse))
                    break
                elif abort:
                    logging.info(
                        'move_by_velocity: abort ({}s)'.format(elapse))

            self.__moving = False
            logging.debug('move_by_velocity: moving={}'.format(self.__moving))

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    def move_to_position(self, x, y, z, velocity, use_ned, wait_done):
        """
        Move vehicle in direction based on specified velocity vectors and
        for the specified duration.

        This uses the SET_POSITION_TARGET_LOCAL_NED command with a type mask enabling only 
        velocity components 
        (http://dev.ardupilot.com/wiki/copter-commands-in-guided-mode/#set_position_target_local_ned).

        Note that from AC3.3 the message should be re-sent every second (after about 3 seconds
        with no message the velocity will drop back to zero). In AC3.2.1 and earlier the specified
        velocity persists until it is canceled. The code below should work on either version 
        (sending the message multiple times does not cause problems).

        See the above link for information on the type_mask (0=enable, 1=ignore). 
        At time of writing, acceleration and yaw bits are ignored.
        """
        x = float(x)
        y = float(y)
        z = float(z)
        velocity = float(velocity)
        use_ned = bool(use_ned)
        wait_done = bool(wait_done)
        logging.debug('move_to_position: x={}, y={}, z{}, velocity:{}, use_ned={}, wait={}'.format(
            x, y, z, velocity, use_ned, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.move_to_position, args=(x, y, z, velocity, use_ned, True))

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug('move_to_position: moving={}'.format(self.__moving))

            frame = mavutil.mavlink.MAV_FRAME_LOCAL_OFFSET_NED if use_ned else mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED
            msg = self._drone.message_factory.set_position_target_local_ned_encode(
                0,       # time_boot_ms (not used)
                0, 0,    # target system, target component
                frame,  # frame
                0b0000111111111000,  # type_mask (only position enabled)
                x, y, z,  # x, y, z positions
                0, 0, 0,  # x, y, z velocity in m/s
                # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
                0, 0, 0,
                0, 0)    # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)

            start_time = time.time()
            stop_count = 0
            self._drone.airspeed = velocity
            self._drone.send_mavlink(msg)
            abort = self.__move_abort_event.is_set()
            while wait_done and not abort:
                current_airspeed = self._drone.airspeed
                stop_count = stop_count + 1 if current_airspeed <= 0.1 else 0
                if stop_count > 4:  # keep stop for 2 (0.5*4) seconds
                    logging.info(
                        'move_to_position: reached the target ({}s)'.format((time.time() - start_time)))
                    break
                logging.debug('move_to_position: elapse={}, stop_cnt={}, cur_spd={}'.format(
                    (time.time() - start_time), stop_count, current_airspeed))
                abort = self.__move_abort_event.wait(0.5)
                if abort:
                    logging.info('move_to_position: abort ({}s)'.format(
                        (time.time() - start_time)))

            self.__moving = False
            logging.debug('move_to_position: moving={}'.format(self.__moving))

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    def move_by_attitude(self, roll, pitch, yaw, z, duration, wait_done):
        logging.warn('move_by_attitude: not supported')
        return self.RET_NOT_SUPPORTED

    def move_by_direction(self, forward, right, down, duration, wait_done):
        logging.warn('move_by_direction: not supported')
        return self.RET_NOT_SUPPORTED

    def turn_heading(self, heading, use_ned, wait_done):
        """
        Send MAV_CMD_CONDITION_YAW message to point vehicle at a specified heading (in degrees).

        This method sets an absolute heading by default, but you can set the `relative` parameter
        to `True` to set yaw relative to the current yaw heading.

        By default the yaw of the vehicle will follow the direction of travel. After setting 
        the yaw using this function there is no way to return to the default yaw "follow direction 
        of travel" behaviour (https://github.com/diydrones/ardupilot/issues/2427)

        For more information see: 
        http://copter.ardupilot.com/wiki/common-mavlink-mission-command-messages-mav_cmd/#mav_cmd_condition_yaw
        """
        heading = int(heading)
        use_ned = bool(use_ned)
        wait_done = bool(wait_done)
        logging.debug('turn_heading: heading={}, use_ned={}, wait={}'.format(
            heading, use_ned, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.turn_heading, args=(heading, use_ned, True))

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug('turn_heading: moving={}'.format(self.__moving))

            relative = 0 if use_ned else 1
            direction = 1 if heading >= 0 else -1
            # create the CONDITION_YAW command using command_long_encode()
            msg = self._drone.message_factory.command_long_encode(
                0, 0,    # target system, target component
                mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
                0,  # confirmation
                abs(heading),    # param 1, yaw in degrees
                0,          # param 2, yaw speed deg/s
                direction,          # param 3, direction -1 ccw, 1 cw
                relative,  # param 4, relative offset 1, absolute angle 0
                0, 0, 0)    # param 5 ~ 7 not used

            # send command to vehicle
            start_time = time.time()
            prev_heading = self.heading
            stop_count = 0
            self._drone.send_mavlink(msg)
            abort = self.__move_abort_event.is_set()
            while wait_done and not abort:
                current_heading = self.heading
                stop_count = stop_count + \
                    1 if abs(current_heading - prev_heading) <= 1 else 0
                if stop_count > 4:  # keep stop for 2 (0.5 * 4) seconds
                    logging.info('turn_heading: reached the target ({}s)'.format(
                        (time.time() - start_time)))
                    break
                logging.debug('turn_heading: stop_cnt={}, prev={}, current={}'.format(
                    stop_count, prev_heading, current_heading))
                prev_heading = current_heading
                abort = self.__move_abort_event.wait(0.5)
                if abort:
                    logging.info('turn_heading: abort ({}s)'.format(
                        (time.time() - start_time)))

            self.__moving = False
            logging.debug('turn_heading: moving={}'.format(self.__moving))

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    @property
    def moving(self):
        return self.__moving

    #==============================#
    # EZDroneGpsAPI Implementation #
    #==============================#
    def move_to_gps_location(self, latitude, longitude, altitude, velocity, relative, wait_done):
        latitude = float(latitude)
        longitude = float(longitude)
        altitude = float(altitude)
        velocity = float(velocity)
        relative = bool(relative)
        wait_done = bool(wait_done)
        logging.debug('move_to_gps_location: lat={}, lng={}, alt={}, v={}, rel={}, wait={}'.format(
            latitude, longitude, altitude, velocity, relative, wait_done))

        # Create an asynchronous task and return immediately if there is no wait
        if not wait_done:
            return self._start_async_task(target=self.move_to_gps_location, args=(latitude, longitude, altitude, velocity, relative, True))

        # Send abort notification and acquire the lock to run the task
        self.__move_abort_event.set()
        with self.__move_lock:
            self.__move_abort_event.clear()
            self.__moving = True
            logging.debug(
                'move_to_gps_location: moving={}'.format(self.__moving))

            target_location = LocationGlobalRelative(
                latitude, longitude, altitude) if relative else LocationGlobal(latitude, longitude, altitude)
            start_time = time.time()
            stop_count = 0
            self._drone.simple_goto(target_location, airspeed=velocity)
            abort = self.__move_abort_event.is_set()
            while wait_done and not abort:
                current_airspeed = self._drone.airspeed
                stop_count = stop_count + 1 if current_airspeed <= 0.1 else 0
                if stop_count > 4:  # keep stop for 2 (0.5*4) seconds
                    logging.info(
                        'move_to_gps_location: reached the target ({}s)'.format((time.time() - start_time)))
                    break
                logging.debug('move_to_gps_location: elapse={}, stop_cnt={}, cur_spd={}'.format(
                    (time.time() - start_time), stop_count, current_airspeed))
                abort = self.__move_abort_event.wait(0.5)
                if abort:
                    logging.info('move_to_gps_location: abort ({}s)'.format(
                        (time.time() - start_time)))

            self.__moving = False
            logging.debug(
                'move_to_gps_location: moving={}'.format(self.__moving))

            return self.RET_SUCCESS if not abort else self.RET_ABORT

    @property
    def gps_location(self):
        loc = self._drone.location.global_frame
        return loc.lat, loc.lon, loc.alt
