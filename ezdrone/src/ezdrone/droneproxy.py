#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# EZDrone APIs for high level uav flight control
#
# Creator : Peter
# Update Data: 2018/8/21
#

from .droneapi import EZDroneAPI, EZDroneMavlinkAPI, EZDroneGpsAPI
from .actor import EZDroneGeneric, EZDroneGuided
from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative, Attitude
from pymavlink import mavutil  # Needed for command message definitions
import time
import math
import logging

'''
def set_attitude(roll_angle = 0.0, pitch_angle = 0.0, yaw_rate = 0.0, thrust = 0.5, duration = 0):
    """
    Note that from AC3.3 the message should be re-sent every second (after about 3 seconds
    with no message the velocity will drop back to zero). In AC3.2.1 and earlier the specified
    velocity persists until it is canceled. The code below should work on either version
    (sending the message multiple times does not cause problems).
    """

    """
    The roll and pitch rate cannot be controllbed with rate in radian in AC3.4.4 or earlier,
    so you must use quaternion to control the pitch and roll for those vehicles.
    """

    # Thrust >  0.5: Ascend
    # Thrust == 0.5: Hold the altitude
    # Thrust <  0.5: Descend
    msg = vehicle.message_factory.set_attitude_target_encode(
        0, # time_boot_ms
        1, # Target system
        1, # Target component
        0b00000000, # Type mask: bit 1 is LSB
        to_quaternion(roll_angle, pitch_angle), # Quaternion
        0, # Body roll rate in radian
        0, # Body pitch rate in radian
        math.radians(yaw_rate), # Body yaw rate in radian
        thrust  # Thrust
    )
    vehicle.send_mavlink(msg)

    start = time.time()
    while time.time() - start < duration:
        vehicle.send_mavlink(msg)
        time.sleep(0.1)
'''


class EZDrone(EZDroneAPI, EZDroneMavlinkAPI, EZDroneGpsAPI):

    def __init__(self, connection_string='/dev/ttyACM0', rate=10, heartbeat_timeout=0):
        print('Connecting to vehicle on: %s' % connection_string)
        self.vehicle = connect(connection_string, wait_ready=True, rate=rate, heartbeat_timeout=heartbeat_timeout)
        if self.vehicle is None:
            exception_string = 'Connecting to vehicle %s failed.' % connection_string
            raise Exception(exception_string)
        self.land_attitude = self.vehicle.attitude
        self.last_diff_attitude = Attitude(pitch=0.0, yaw=0.0, roll=0.0)
        print(self.land_attitude)
        print('<land_attitude_angle> : pitch: %f, yaw: %f, roll: %f' % (math.degrees(
            self.land_attitude.pitch), math.degrees(self.land_attitude.yaw), math.degrees(self.land_attitude.roll)))
        # print(self.vehicle.rangefinder)
        #print('Heading: %s' % self.vehicle.heading)
        print('System status: %s' % self.vehicle.system_status.state)
        print('Mode: %s' % self.vehicle.mode.name)

        self.__actors = {'GENERIC': EZDroneGeneric(
            self.vehicle), 'GUIDED': EZDroneGuided(self.vehicle)}

    def __find_actor(self, interface):
        current_mode = self.vehicle.mode.name
        if self.__actors.has_key(current_mode) and isinstance(self.__actors[current_mode], interface):
            return self.__actors[current_mode]
        elif self.__actors.has_key('GENERIC') and isinstance(self.__actors['GENERIC'], interface):
            return self.__actors['GENERIC']
        else:
            return None

    #==================#
    # EZDroneAPI Entry #
    #==================#
    def switch_mode(self, mode):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.switch_mode(mode)
        else:
            logging.warn(
                'switch_mode: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def arm_disarm(self, do_arming):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.arm_disarm(do_arming)
        else:
            logging.warn(
                'arm_disarm: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def takeoff(self, height, auto_arm=False, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.takeoff(height, auto_arm, wait_done)
        else:
            logging.warn(
                'takeoff: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def land(self, auto_disarm=False, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.land(auto_disarm, wait_done)
        else:
            logging.warn('land: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def move_by_velocity(self, velocity_x, velocity_y, velocity_z, heading, duration, use_ned=False, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.move_by_velocity(velocity_x, velocity_y, velocity_z, heading, duration, use_ned, wait_done)
        else:
            logging.warn(
                'move_by_velocity: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def move_to_position(self, x, y, z, velocity, use_ned=False, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.move_to_position(x, y, z, velocity, use_ned, wait_done)
        else:
            logging.warn(
                'move_to_position: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def move_by_attitude(self, roll, pitch, yaw, height, duration, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.move_by_attitude(roll, pitch, yaw, height, duration, wait_done)
        else:
            logging.warn(
                'move_by_attitude: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def move_by_direction(self, forward, right, down, duration, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.move_by_direction(forward, right, down, duration, wait_done)
        else:
            logging.warn(
                'move_by_direction: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def turn_heading(self, heading, use_ned=False, wait_done=True):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.turn_heading(heading, use_ned, wait_done)
        else:
            logging.warn(
                'turn_heading: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def get_task_state(self, task_id):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.get_task_state(task_id)
        else:
            logging.warn(
                'get_task_state: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    @property
    def system_status(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.system_status
        else:
            logging.warn(
                'system_status: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def mode(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.mode
        else:
            logging.warn('mode: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def attitude(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.attitude
        else:
            logging.warn(
                'attitude: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def height(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.height
        else:
            logging.warn(
                'height: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def velocity(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.velocity
        else:
            logging.warn(
                'velocity: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def heading(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.heading
        else:
            logging.warn(
                'heading: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def moving(self):
        actor = self.__find_actor(EZDroneAPI)
        if actor is not None:
            return actor.moving
        else:
            logging.warn(
                'moving: not supported. cur_mode={}'.format(self.mode))
            return None

    #=========================#
    # EZDroneMavlinkAPI Entry #
    #=========================#
    def add_message_listener(self, message_name, listener):
        actor = self.__find_actor(EZDroneMavlinkAPI)
        if actor is not None:
            return actor.add_message_listener(message_name, listener)
        else:
            logging.warn(
                'add_message_listener: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    def remove_message_listener(self, message_name, listener):
        actor = self.__find_actor(EZDroneMavlinkAPI)
        if actor is not None:
            return actor.remove_message_listener(message_name, listener)
        else:
            logging.warn(
                'remove_message_listener: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    @property
    def parameters(self):
        actor = self.__find_actor(EZDroneMavlinkAPI)
        if actor is not None:
            return actor.parameters
        else:
            logging.warn(
                'parameters: not supported. cur_mode={}'.format(self.mode))
            return None

    @property
    def channels(self):
        actor = self.__find_actor(EZDroneMavlinkAPI)
        if actor is not None:
            return actor.channels
        else:
            logging.warn(
                'channels: not supported. cur_mode={}'.format(self.mode))
            return None

    #=====================#
    # EZDroneGpsAPI Entry #
    #=====================#
    def move_to_gps_location(self, latitude, longitude, altitude, velocity, relative=False, wait_done=True):
        actor = self.__find_actor(EZDroneGpsAPI)
        if actor is not None:
            return actor.move_to_gps_location(latitude, longitude, altitude, velocity, relative, wait_done)
        else:
            logging.warn(
                'move_to_gps_location: not supported. cur_mode={}'.format(self.mode))
            return self.RET_NOT_SUPPORTED

    @property
    def gps_location(self):
        actor = self.__find_actor(EZDroneGpsAPI)
        if actor is not None:
            return actor.gps_location
        else:
            logging.warn(
                'gps_location: not supported. cur_mode={}'.format(self.mode))
            return None

    #======================#
    # Experimental Section #
    #======================#
    def hold_position(self, duration):
        print("hold_position")
        thrust = 0.5
        start = time.time()
        while time.time() - start < duration:
            adjust_pitch_angle, adjust_roll_angle, adjust_yaw_angle = self.adjust_attitude()
            self.set_attitude2(roll_angle=adjust_roll_angle, pitch_angle=adjust_pitch_angle,
                               yaw_angle=adjust_yaw_angle, thrust=thrust)
            time.sleep(0.1)

    def arm_and_takeoff_nogps(self, aTargetAltitude):

        print("arm_and_takeoff_nogps")

        # take off 相關參數
        # 馬達推力
        DEFAULT_TAKEOFF_THRUST = 0.55
        SMOOTH_TAKEOFF_THRUST = 0.53
        # 調整角度的上限及下限
        LOW_BOUND_ANGLE_TO_ADJUST = 1.0
        UP_BOUND_ANGLE_TO_ADJUST = 10.0

        print("Ch1: %s" % self.vehicle.channels['1'])
        print("Ch2: %s" % self.vehicle.channels['2'])
        print("Ch3: %s" % self.vehicle.channels['3'])
        print("Ch4: %s" % self.vehicle.channels['4'])
        print("Ch5: %s" % self.vehicle.channels['5'])
        print("Ch6: %s" % self.vehicle.channels['6'])
        print("Vehicle mode %s" % self.vehicle.mode.name)
        print("Arming motors")

        self.vehicle.armed = False
        time.sleep(1)

        # Copter should arm in GUIDED_NOGPS mode
        self.vehicle.mode = VehicleMode("GUIDED_NOGPS")
        self.vehicle.armed = True

        while not self.vehicle.armed:
            print(" Waiting for arming...")
            self.vehicle.armed = True
            time.sleep(1)

        print("Taking off!")
        print('=====\n')
        thrust = DEFAULT_TAKEOFF_THRUST
        adjust_attitude = Attitude(pitch=0.0, yaw=0.0, roll=0.0)
        #last_diff_attitude = Attitude(pitch = 0.0, yaw = 0.0, roll = 0.0)
        while True:
            # vehicle.rangefinder.distance
            current_altitude = self.vehicle.location.global_relative_frame.alt
            print(" Altitude: %f  Desired: %f" %
                  (current_altitude, aTargetAltitude))

            # Trigger just below target alt.
            if current_altitude >= (aTargetAltitude * 0.95):
                print("Reached target altitude")
                break
            elif current_altitude >= (aTargetAltitude * 0.7):
                thrust = SMOOTH_TAKEOFF_THRUST

            adjust_pitch_angle, adjust_roll_angle, adjust_yaw_angle = self.adjust_attitude()

            # 上升至 0.1 m 後 pitch, roll, yaw angle 調整才生效?
            # if current_altitude <= 0.1:
            #   adjust_pitch_angle = 0.0
            #   adjust_yaw_angle = 0.0
            #   adjust_roll_angle = 0.0

            print('<adjust_pitch_angle> : %f' % adjust_pitch_angle)
            print('<adjust_yaw_angle> : %f' % adjust_yaw_angle)
            print('<adjust_roll_angle> : %f' % adjust_roll_angle)

            print('=====\n')

            #set_attitude(thrust = thrust)
            self.set_attitude2(roll_angle=adjust_roll_angle, pitch_angle=adjust_pitch_angle,
                               yaw_angle=adjust_yaw_angle, thrust=thrust)
            time.sleep(0.2)

    # =======
    # APIs for internal use of EZDrone class
    # =======

    def adjust_attitude(self, mode=0):  # mode == 0 for takeoff, mode == 1 for others
        # 調整角度的上限及下限
        LOW_BOUND_ANGLE_TO_ADJUST = 1.0
        UP_BOUND_ANGLE_TO_ADJUST = 30.0
        # 差距的 threshold
        #DIFF_THRESHOLD = 0.5
        adjust_attitude = Attitude(pitch=0.0, yaw=0.0, roll=0.0)
        p = 0
        r = 0
        y = 0
        if mode == 0:
            y = self.land_attitude.yaw

        # pitch 及 roll 朝角度 0 去修正, 無人機頭像朝起飛的頭像修正
        adjust_attitude.pitch = math.degrees(p - self.vehicle.attitude.pitch)
        adjust_attitude.roll = math.degrees(r - self.vehicle.attitude.roll)
        adjust_attitude.yaw = math.degrees(y - self.vehicle.attitude.yaw)

        print('<adjust angle> : %s' % adjust_attitude)
        print('<last diff attitude> : %s' % self.last_diff_attitude)

        if self.last_diff_attitude.pitch == 0.0 and self.last_diff_attitude.roll == 0.0 and self.last_diff_attitude.yaw == 0.0:
            self.last_diff_attitude.pitch = adjust_attitude.pitch
            self.last_diff_attitude.roll = adjust_attitude.roll
            self.last_diff_attitude.yaw = adjust_attitude.yaw
        else:
            # 上次的差距比本次偵測到的差距小則不調整

            # adjust_attitude.pitch 與 last_diff_attitude.pitch 需同號 i.e. adjust_attitude.pitch * last_diff_attitude.pitch > 0
            # pitch
            if (adjust_attitude.pitch * self.last_diff_attitude.pitch > 0) and (math.fabs(adjust_attitude.pitch) < math.fabs(self.last_diff_attitude.pitch)):
                self.last_diff_attitude.pitch = adjust_attitude.pitch
                adjust_attitude.pitch = 0
            else:
                self.last_diff_attitude.pitch = adjust_attitude.pitch
            # roll
            if (adjust_attitude.roll * self.last_diff_attitude.roll > 0) and (math.fabs(adjust_attitude.roll) < math.fabs(self.last_diff_attitude.roll)):
                self.last_diff_attitude.roll = adjust_attitude.roll
                adjust_attitude.roll = 0
            else:
                self.last_diff_attitude.roll = adjust_attitude.roll
            # yaw
            if (adjust_attitude.yaw * self.last_diff_attitude.yaw > 0) and (math.fabs(adjust_attitude.yaw) < math.fabs(self.last_diff_attitude.yaw)):
                self.last_diff_attitude.yaw = adjust_attitude.yaw
                adjust_attitude.yaw = 0
            else:
                self.last_diff_attitude.yaw = adjust_attitude.yaw

        print('<current_angle>: pitch: %f, yaw: %f, roll: %f, dist.:%f' % (math.degrees(self.vehicle.attitude.pitch), math.degrees(
            self.vehicle.attitude.yaw), math.degrees(self.vehicle.attitude.roll), self.vehicle.location.global_relative_frame.alt))
        print('<adjust angle (after)> : %s' % adjust_attitude)

        adjust_pitch_angle = 0.0
        adjust_roll_angle = 0.0
        adjust_yaw_angle = 0.0

        # 檢查調整角度的上下限
        abs_pitch = math.fabs(adjust_attitude.pitch)
        if abs_pitch >= LOW_BOUND_ANGLE_TO_ADJUST and abs_pitch <= UP_BOUND_ANGLE_TO_ADJUST:
            adjust_pitch_angle = adjust_attitude.pitch

        abs_roll = math.fabs(adjust_attitude.roll)
        if abs_roll >= LOW_BOUND_ANGLE_TO_ADJUST and abs_roll <= UP_BOUND_ANGLE_TO_ADJUST:
            adjust_roll_angle = adjust_attitude.roll

        abs_yaw = math.fabs(adjust_attitude.yaw)
        if abs_yaw >= LOW_BOUND_ANGLE_TO_ADJUST and abs_yaw <= UP_BOUND_ANGLE_TO_ADJUST:
            adjust_yaw_angle = adjust_attitude.yaw

        return (adjust_pitch_angle, adjust_roll_angle, adjust_yaw_angle)

    def to_quaternion(self, roll=0.0, pitch=0.0, yaw=0.0):
        """
        Convert degrees to quaternions
        """
        t0 = math.cos(math.radians(yaw * 0.5))
        t1 = math.sin(math.radians(yaw * 0.5))
        t2 = math.cos(math.radians(roll * 0.5))
        t3 = math.sin(math.radians(roll * 0.5))
        t4 = math.cos(math.radians(pitch * 0.5))
        t5 = math.sin(math.radians(pitch * 0.5))

        w = t0 * t2 * t4 + t1 * t3 * t5
        x = t0 * t3 * t4 - t1 * t2 * t5
        y = t0 * t2 * t5 + t1 * t3 * t4
        z = t1 * t2 * t4 - t0 * t3 * t5

        return [w, x, y, z]

    def set_attitude2(self, roll_angle=0.0, pitch_angle=0.0, yaw_angle=0.0, thrust=0.5, duration=0):
        # Thrust >  0.5: Ascend
        # Thrust == 0.5: Hold the altitude
        # Thrust <  0.5: Descend
        msg = self.vehicle.message_factory.set_attitude_target_encode(
            0,  # time_boot_ms
            1,  # Target system
            1,  # Target component
            0b00000000,  # Type mask: bit 1 is LSB
            self.to_quaternion(roll_angle, pitch_angle,
                               yaw_angle),  # Quaternion
            0,  # Body roll rate in radian
            0,  # Body pitch rate in radian
            0,  # Body yaw rate in radian
            thrust  # Thrust
        )
        self.vehicle.send_mavlink(msg)

        start = time.time()
        while time.time() - start < duration:
            self.vehicle.send_mavlink(msg)
            time.sleep(0.1)
