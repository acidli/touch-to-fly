#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
ezdrone.droneutil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The utility functions for using ezdrone.

:author: YM
:version: 0.1.0
:copyright:
:license:
"""

import math


class EZDroneUtil(object):
    @staticmethod
    def rpy_to_quaternion(roll, pitch, yaw):
        """
        Convert degrees to quaternions
        """
        t0 = math.cos(math.radians(yaw * 0.5))
        t1 = math.sin(math.radians(yaw * 0.5))
        t2 = math.cos(math.radians(roll * 0.5))
        t3 = math.sin(math.radians(roll * 0.5))
        t4 = math.cos(math.radians(pitch * 0.5))
        t5 = math.sin(math.radians(pitch * 0.5))

        w = t0 * t2 * t4 + t1 * t3 * t5
        x = t0 * t3 * t4 - t1 * t2 * t5
        y = t0 * t2 * t5 + t1 * t3 * t4
        z = t1 * t2 * t4 - t0 * t3 * t5

        return w, x, y, z

    @staticmethod
    def get_gps_location(original_lat, original_lng, distance_north, distance_east):
        """
        Returns the latitude/longitude `distance_north` and `distance_east` metres from the
        specified `original_lat` and `original_lng`.

        The function is useful when you want to move the drone around specifying locations relative to
        the current drone position.

        The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.

        For more information see:
        http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
        """
        earth_radius = 6378137.0  # Radius of "spherical" earth
        # Coordinate offsets in radians
        offset_lat = distance_north/earth_radius
        offset_lng = distance_east / \
            (earth_radius*math.cos(math.pi*original_lat/180))

        # New position in decimal degrees
        new_lat = original_lat + (offset_lat * 180/math.pi)
        new_lng = original_lng + (offset_lng * 180/math.pi)

        return new_lat, new_lng

    @staticmethod
    def get_gps_distance(lat1, lng1, lat2, lng2):
        """
        Returns the ground distance in metres between `lat1, lng1` and `lat2, lng2`.

        This method is an approximation, and will not be accurate over large distances and close to the
        earth's poles. It comes from the ArduPilot test code:
        https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
        """
        distance_lat = lat2 - lat1
        distance_lng = lng2 - lng1
        return math.sqrt((distance_lat*distance_lat) + (distance_lng*distance_lng)) * 1.113195e5

    @staticmethod
    def get_gps_bearing(lat1, lng1, lat2, lng2):
        """
        Returns the bearing between `lat1, lng1` and `lat2, lng2`.

        This method is an approximation, and may not be accurate over large distances and close to the
        earth's poles. It comes from the ArduPilot test code:
        https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
        """
        off_x = lng2 - lng1
        off_y = lat2 - lat1
        bearing = 90.00 + math.atan2(-off_y, off_x) * 57.2957795
        if bearing < 0:
            bearing += 360.00
        return bearing
