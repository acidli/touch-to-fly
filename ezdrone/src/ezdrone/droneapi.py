#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
ezdrone.ezdroneapi
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EZDrone API Definition

This module defines the API of EZDrone.

API uses the drone body or NED coordinate system.
In the body coordinate system, +X is forwarding, +Y is right, and +Z is down.
In the NED coordinate system, +X is North, +Y is East, +Z is Down, and zero yaw angle is North.

Distance unit: meter (m)
Duration unit: second (s)
Angle unit: degree (deg)

:author: YM
:version: 0.8.0
:copyright:
:license:
"""

import abc


class EZDroneAPI(object):
    __metaclass__ = abc.ABCMeta

    RET_SUCCESS = 0         # Return code represents the operation is successful
    RET_FAILURE = -1        # Return code represents the operation has failure
    RET_ABORT = -2          # Return code represents the operation is aborted
    RET_TIMEOUT = -3        # Return code represents the operation is timeout
    RET_NOT_SUPPORTED = -4  # Return code represents the operation is not supported

    TASK_DONE = 0       # State code represents the task is done
    TASK_STARTED = 1    # State code represents the task is started
    TASK_RUNNING = 2    # State code represents the task is running

    @abc.abstractmethod
    def switch_mode(self, mode):
        """
        Switch flight mode.

        Parameters:
            mode (string) - The flight mode to be switched.

        Returns:
            An integer value that indicates whether the operation was successful.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def arm_disarm(self, do_arming):
        """
        Arm or disarm the drone.

        Parameters:
            do_arming (bool) - True value means arming the drone; otherwise disarming.

        Returns:
            An integer value that indicates whether the operation was successful.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def takeoff(self, height, auto_arm, wait_done):
        """
        Takeoff the drone and fly to the target height.

        Parameters:
            height (float) - The target height relative to the home position in meters.
            auto_arm (bool) - Indicate whether arm the drone automatically.
            wait_done (bool) - If the value is true, the function will return
                after the drone is reached specified height; otherwise, the
                function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def land(self, auto_disarm, wait_done):
        """
        Make the drone landing.

        Parameters:
            auto_disarm (bool) - Indicate whether disarm the drone automatically.
            wait_done (bool) - If the value is true, the function will return
                after the drone is landed; otherwise, the function will return
                immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def move_by_velocity(self, velocity_x, velocity_y, velocity_z, heading, duration, use_ned, wait_done):
        """
        Move the drone by setting the velocity.

        Parameters:
            velocity_x (float) - The velocity of forward(+)/backward(-) in meters per second.
            velocity_y (float) - The velocity of right(+)/left(-) in meters per second.
            velocity_z (float) - The velocity of down(+)/up(-) in meters per second.
            heading (int) - The absolute or relative angle of the drone yaw heading in degrees.
            duration (float) - The movement duration in seconds.
            use_ned (bool) - If the value is true, the coordinate frame uses NED; otherwise,
                it uses the drone body frame.
            wait_done (bool) - If the value is true, the function will return after the
                movement duration is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def move_to_position(self, x, y, z, velocity, use_ned, wait_done):
        """
        Move the drone to target position (x,y,z) with specified velocity.

        Parameters:
            x (float) - The x coordinate of the target position in meters.
            y (float) - The y coordinate of the target position in meters.
            z (float) - The z coordinate of the target position in meters.
            velocity (float) - The movement velocity in meters per second.
            use_ned (bool) - If the value is true, the coordinate frame uses NED; otherwise,
                it uses the drone body frame.
            wait_done (bool) - If the value is true, the function will return after the
                destination is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def move_by_attitude(self, roll, pitch, yaw, height, duration, wait_done):
        """
        Move the drone with specific attitude and height.

        Parameters:
            roll (float) - The roll angle of the drone in degree.
            pitch (float) - The pitch angle of the drone in degree.
            yaw (float) - The yaw angle of the drone in degree.
            height (float) - The target height in meters.
            duration (float) - The movement duration in seconds.
            wait_done (bool) - If the value is true, the function will return after the
                movement duration is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def move_by_direction(self, forward, right, down, duration, wait_done):
        """
        Move the drone with specific direction.

        Parameters:
            forward (int) - Positive value means moving forward; otherwise moving backward.
            right (int) - Positive value means moving right; otherwise moving left.
            down (int) - Positive value means moving down; otherwise moving up.
            duration (float) - The movement duration in seconds.
            wait_done (bool) - If the value is true, the function will return after the
                movement duration is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def turn_heading(self, heading, use_ned, wait_done):
        """
        Point drone at a specified heading (in degrees).

        Parameters:
            heading (int) - The absolute or relative angle of the drone yaw heading in degrees.
            use_ned (bool) - If the value is true, the coordinate frame uses NED; otherwise,
                it uses the drone body frame.
            wait_done (bool) - If the value is true, the function will return after the
                yaw angle is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_task_state(self, task_id):
        """
        Get the current state of the task specified by task_id.

        Parameters:
            task_id (integer) - The positive value returned by move related task.

        Returns:
            An integer value that indicates the current state.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def system_status(self):
        """
        Get current system status.

        Parameters:
            None

        Returns:
            A string that indicates current system status, or "None" if not supported.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def mode(self):
        """
        Get the current flight mode.

        Parameters:
            None

        Returns:
            A string that indicates current flight mode, or "None" if not supported.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def attitude(self):
        """
        Get the current attitude.

        Parameters:
            None

        Returns:
            A tuple representing the current roll, pitch, and yaw angle (in degrees)
            in the drone body coordinate system, or "None" if not supported.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def height(self):
        """
        Get the current height.

        Parameters:
            None

        Returns:
            A float value that indicates current height relative to the home position,
            or "None" if not supported.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def velocity(self):
        """
        Get the current velocity.

        Parameters:
            None

        Returns:
            A tuple representing the x / y / z component (in meters per second) 
            of the current velocity in the NED coordinate system, or "none" if not supported.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def heading(self):
        """
        Get the current heading of the drone.

        Parameters:
            None

        Returns:
            An integer value that indicates current heading (in degrees) of the 
            drone in the NED coordinate system, or "None" if not supported
        """
        raise NotImplementedError

    @abc.abstractproperty
    def moving(self):
        """
        Get the current state of the drone move operation.

        Parameters:
            None

        Returns:
            A boolean value that indicates whether the current move operation is done,
            or "None" if not supported.
        """
        raise NotImplementedError


class EZDroneMavlinkAPI(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def add_message_listener(self, message_name, listener):
        '''
        Adds a message listener function that will be called every time the specified message is received.

        The listener function must have three arguments:
            ezdrone - the current ezdrone instance.
            name - the name of the message that was intercepted.
            message - the actual message (a pymavlink class).
                      See https://www.samba.org/tridge/UAV/pymavlink/apidocs/classIndex.html for details.

        For example:
            # Listener method for new messages
            def my_listener(self, name, msg):
                pass

            ezdrone.add_message_listener('HEARTBEAT', my_listener)

        Parameters:
            message_name (string) - The name of the message to be intercepted by the listener function (or ‘*’ to get all messages).
            listener (object) - The listener function that will be called if a message is received.

        Returns:
            None
        '''
        raise NotImplementedError

    @abc.abstractmethod
    def remove_message_listener(self, message_name, listener):
        '''
        Removes a message listener (that was previously added using add_message_listener()).

        Parameters:
            message_name (string) - The name of the message for which the listener is to be removed (or ‘*’ to remove an ‘all messages’ observer).
            listener (object) - The listener callback function to remove.

        Returns:
            None
        '''
        raise NotImplementedError
    
    @abc.abstractproperty
    def parameters(self):
        """
        The (editable) parameters for this drone (:py:class:`collections.MutableMapping`).

        The code fragment below shows how to get and set the value of a parameter.

        .. code:: python

            # Print the value of the THR_MIN parameter.
            print "Param: %s" % drone.parameters['THR_MIN']

            # Change the parameter value to something different.
            drone.parameters['THR_MIN']=100
        """
        raise NotImplementedError
    
    @abc.abstractproperty
    def channels(self):
        """
        The RC channel values from the RC Transmitter (:py:class:`Channels`).

        The attribute can also be used to set and read RC Override (channel override) values
        via :py:attr:`Vehicle.channels.override <dronekit.Channels.overrides>`.

        To read the channels from the RC transmitter:

        .. code:: python

            # Get all channel values from RC transmitter
            print "Channel values from RC Tx:", drone.channels

            # Access channels individually
            print "Read channels individually:"
            print " Ch1: %s" % drone.channels['1']
            print " Ch2: %s" % drone.channels['2']

        To set channel overrides:

        .. code:: python

            # Set and clear overrids using dictionary syntax (clear by setting override to none)
            drone.channels.overrides = {'5':None, '6':None,'3':500}

            # You can also set and clear overrides using indexing syntax
            drone.channels.overrides['2'] = 200
            drone.channels.overrides['2'] = None

            # Clear using 'del'
            del drone.channels.overrides['3']

            # Clear all overrides by setting an empty dictionary
            drone.channels.overrides = {}

        Read the channel overrides either as a dictionary or by index. Note that you'll get
        a ``KeyError`` exception if you read a channel override that has not been set.

        .. code:: python

            # Get all channel overrides
            print " Channel overrides: %s" % drone.channels.overrides
            # Print just one channel override
            print " Ch2 override: %s" % drone.channels.overrides['2']
        """
        raise NotImplementedError


class EZDroneGpsAPI(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def move_to_gps_location(self, latitude, longitude, altitude, velocity, relative, wait_done):
        """
        Go to a specified global location (latitude, longitude, altitude) with specified velocity.

        Parameters:
            latitude (float) - The x position in WGS84 coordinate frame.
            longitude (float) - The y position in WGS84 coordinate frame
            altitude (float) - The altitude in meters.
            velocity (float) - The movement velocity in meters per second.
            relative (bool) - If the value is false, the altitude relative to the mean sea level (MSL); 
                otherwise, relative to the home location.
            wait_done (bool) - If the value is true, the function will return after the
                destination is reached; otherwise, the function will return immediately.

        Returns:
            An integer value that indicates whether the operation was successful.
            When wait_done is false, the return value is the task id.
        """
        raise NotImplementedError

    @abc.abstractproperty
    def gps_location(self):
        """
        Get the current GPS location.

        Parameters:
            None

        Returns:
            A tuple representing the latitude / longitude / altitude relative to the mean sea level (MSL). 
        """
        raise NotImplementedError
