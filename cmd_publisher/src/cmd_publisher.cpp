/*
 * Copyright (C) 2008, Morgan Quigley and Willow Garage, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Stanford University or Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 

#include "cmd_publisher.h"

  

namespace Roi_Info {

CmdReceiver::CmdReceiver():
m_CmdToPublish(CMD_TO_PUBLISH_NONE),
Image_W(0), 
Image_H(0),
n(),
private_node_handle("~"),
goROImsg(), goDirmsg(),
camera_info_sub(),
roi_pub(),
mission_pub(),
abort_pub(),
turnHeading_pub(),
shiftMov_pub(),
surround_pub()
//isupdateMission(false) 
{     
    control_channel_init(szCtrl_Port);
    register_control_action((char *)"goROI", (action_handler_func)handle_goROI, (void *)this);
    register_control_action((char *)"goDir", (action_handler_func)handle_goDir, (void *)this);
    register_control_action((char *)"shiftMov", (action_handler_func)handle_shiftMov, (void *)this);
    register_control_action((char *)"turnHeading", (action_handler_func)handle_turnHeading, (void *)this);
    register_control_action((char *)"Abort", (action_handler_func)handle_stop, (void *)this);
    register_control_action((char *)"hSurrounding", (action_handler_func)handle_hSurrounding, (void *)this);

    register_control_action((char *)"EZCmd", (action_handler_func)handle_EZCmd, (void *)this);
    start_control_channel_loop(); 

    roi_pub = n.advertise<cmd_publisher::roi>("cmd_publisher/roi", 1000);
    camera_info_sub = n.subscribe("/camera_info/camera_info", 100, &CmdReceiver::camerainfoCallback, this);  

    mission_pub = n.advertise<cmd_publisher::mission>("cmd_publisher/mission", 1000);       

    poi_pub = n.advertise<cmd_publisher::poi>("poi", 1000);
    abort_pub = n.advertise<cmd_publisher::abort>("cmd_publisher/abort", 1000);
    turnHeading_pub = n.advertise<cmd_publisher::turnHeading>("cmd_publisher/turnHeading", 1000);
    shiftMov_pub = n.advertise<cmd_publisher::shiftMov>("cmd_publisher/shiftMov", 1000);
    surround_pub = n.advertise<cmd_publisher::surround>("cmd_publisher/surround", 1000);


}


CmdReceiver::~CmdReceiver(){} 





void CmdReceiver::publishMission()
{

  //  if(isupdateMission)
    {

        mission_pub.publish(missionmsg);


 //       isupdateMission=false;
    }




}



void CmdReceiver::setMission(std::string mission)
{
    clearMission();
    missionmsg.mission=mission;
    
}



void CmdReceiver::setMission(std::string mission,bool wait_done)
{
    clearMission();
    missionmsg.mission=mission;
    missionmsg.wait_done = wait_done;
}


void CmdReceiver::setMission(std::string mission,float z,bool wait_done)
{

    clearMission();
    missionmsg.mission=mission;
    missionmsg.z = z;  
    missionmsg.wait_done = wait_done;
}

 



void CmdReceiver::setMission(std::string mission,float z,bool use_ned,bool wait_done)
{

    clearMission();
    missionmsg.mission=mission;
    missionmsg.z = z;  
    missionmsg.use_ned = use_ned;
    missionmsg.wait_done = wait_done;
}

 




void  CmdReceiver::setMission(std::string mission,float x,float y,float z,float velocity,float duration,bool use_ned,bool wait_done)
{
 
    clearMission();
    missionmsg.mission=mission;
    missionmsg.x = x;
    missionmsg.y = y;
    missionmsg.z = z; 
    missionmsg.velocity = velocity;
    missionmsg.duration = duration;
    missionmsg.use_ned = use_ned;
    missionmsg.wait_done = wait_done;

}




void  CmdReceiver::clearMission()
{
 
    missionmsg.mission.clear();
    missionmsg.x = 0;
    missionmsg.y = 0;
    missionmsg.z = 0; 
    missionmsg.velocity = 0;
    missionmsg.duration = 0;
    missionmsg.use_ned = false;
    missionmsg.wait_done = false;

}

 
 


void* CmdReceiver::handle_EZCmd(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;
    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;

    if(strcmp(s->EZCmd, "mov_to_pos")==0) {
        //mov to pos
        ROS_DEBUG("###\n\n\n\n\nMov to pos, (X,Y,Z)=(%f,%f,%f), speed=%f, flag=%d, %d\n", 
                s->EZ_axis_x, s->EZ_axis_y, s->EZ_axis_z, s->EZ_speed_for_mov_to_pos, s->EZ_ned, s->EZ_block);
        
         

        pCmdReceiver->setMission("mov_to_pos", s->EZ_axis_x, s->EZ_axis_y, s->EZ_axis_z, s->EZ_speed_for_mov_to_pos,0, s->EZ_ned,  s->EZ_block);
  //      pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    }

    if(strcmp(s->EZCmd, "mov_by_vel")==0) {
        //mov by vel
        ROS_DEBUG("###\n\nMov by velocity, (VX,VY,VZ)=(%f,%f,%f), duraiton=%f, flag=%d, %d\n", 
                s->EZ_axis_x, s->EZ_axis_y, s->EZ_axis_z, s->EZ_duration_for_mov_by_vel, s->EZ_ned, s->EZ_block);
        pCmdReceiver->setMission("mov_by_vel",s->EZ_axis_x, s->EZ_axis_y, s->EZ_axis_z,0, s->EZ_duration_for_mov_by_vel, s->EZ_ned, s->EZ_block);
    //    pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    }





    if(strcmp(s->EZCmd, "turn_heading")==0) {
        //turn_heading
        ROS_DEBUG("###\n\nturn_heading, (Z)=(%f),  flag=%d, %d\n", 
                s->EZ_axis_z, s->EZ_ned, s->EZ_block);
        pCmdReceiver->setMission("turn_heading",s->EZ_axis_z, s->EZ_ned, s->EZ_block);
 //       pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    }

 
    if(strcmp(s->EZCmd, "takeoff")==0) {
        //takeoff
        ROS_DEBUG("###\n\ntakeoff, (Z)=(%f),    flag=%d \n", 
                s->EZ_axis_z,   s->EZ_block);
        pCmdReceiver->setMission("takeoff",s->EZ_axis_z, s->EZ_block);
   //     pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    } 






    if(strcmp(s->EZCmd, "land")==0) {
        //land
        ROS_DEBUG("###\n\nland,  flag= %d\n", 
                 s->EZ_block);
        pCmdReceiver->setMission("land", s->EZ_block);
   //     pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    }




    if(strcmp(s->EZCmd, "guidedMode")==0) {
        //land
        ROS_DEBUG("###\n\nSwitchToGUIDED\n");
        pCmdReceiver->setMission("guided");
  //      pCmdReceiver->isupdateMission=true;
        pCmdReceiver->publishMission();
    }
 

 




    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}

void* CmdReceiver::handle_goROI(struct control_conn *s, void *para, int *ret)
{

    int send_num = 0; 
    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;
    
    int TL_x=std::min(s->LX,s->RX);
    int TL_y=std::min(s->LY,s->RY);

    int BR_x=std::max(s->LX,s->RX);
    int BR_y=std::max(s->LY,s->RY);

    int _roi_x = floor((TL_x * pCmdReceiver->Image_W) / s->AgainstImageWidth);
    int _roi_y = floor((TL_y * pCmdReceiver->Image_H) / s->AgainstImageHeight);
 
    int _roi_w = floor((BR_x * pCmdReceiver->Image_W) / s->AgainstImageWidth) - _roi_x;
    int _roi_h = floor((BR_y * pCmdReceiver->Image_H) / s->AgainstImageHeight) - _roi_y;


    pCmdReceiver->set_goROI_Msg(_roi_x, _roi_y, _roi_w, _roi_h, pCmdReceiver->Image_W, pCmdReceiver->Image_H);
    pCmdReceiver->publish_goROI_Msg();

#if 0 
    pCmdReceiver->roi_x = floor((TL_x * pCmdReceiver->Image_W) / s->AgainstImageWidth);
    pCmdReceiver->roi_y = floor((TL_y * pCmdReceiver->Image_H) / s->AgainstImageHeight);
 
    pCmdReceiver->roi_width = floor((BR_x * pCmdReceiver->Image_W) / s->AgainstImageWidth) - pCmdReceiver->roi_x;
    pCmdReceiver->roi_height = floor((BR_y * pCmdReceiver->Image_H) / s->AgainstImageHeight) - pCmdReceiver->roi_y;
#endif    
    ROS_WARN("\n\n\ngo ROI: (%d,%d),(%d,%d) ->(%d,%d),(%d,%d) \n", TL_x, TL_y,BR_x, BR_y, 
                                                       _roi_x, _roi_y,  _roi_x+_roi_w,_roi_y+ _roi_h);

    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}

void* CmdReceiver::handle_goDir(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;
    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;

    int _x = floor((s->DirX * pCmdReceiver->Image_W) / s->AgainstImageWidth);
    int _y = floor((s->DirY * pCmdReceiver->Image_H) / s->AgainstImageHeight);

    pCmdReceiver->set_goDir_Msg(_x, _y, pCmdReceiver->Image_W, pCmdReceiver->Image_H);

#if 0
    struct TouchInputCmd *pCmd= (struct TouchInputCmd *)para;

    pCmd->Dir_X = floor((s->DirX * pCmd->Image_W) / s->AgainstImageWidth);
    pCmd->Dir_Y = floor((s->DirY * pCmd->Image_H) / s->AgainstImageHeight);

    printf("\n\n\ngo Dir (%d,%d) against (%d, %d)->(%d,%d) \n", s->DirX, s->DirY, s->AgainstImageWidth, s->AgainstImageHeight,
                                              pCmd->Dir_X, pCmd->Dir_Y);
#endif

    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}

void* CmdReceiver::handle_shiftMov(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;

    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;

    double _Speed_X = s->SpeedX;
    double _Speed_Y = s->SpeedY;
    double _ShiftMovDuration = s->ShiftMovDuration;


    #if 1
        _ShiftMovDuration = 2;
    #endif

   
    ROS_DEBUG("\n\n\n\n\nshift mov dir=(%f, %f) , duration=%f\n\b\b",_Speed_X, _Speed_Y, _ShiftMovDuration);


    pCmdReceiver->set_shiftMov_Msg( _Speed_X, _Speed_Y, _ShiftMovDuration);

    pCmdReceiver->publish_shiftMov_Msg();


    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}


void* CmdReceiver::handle_turnHeading(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;

    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;

    int _dir_horizontal = floor((s->DirX * pCmdReceiver->Image_W) / s->AgainstImageWidth);
    ROS_DEBUG("\n\n\n\n\nturn Heading (%d) against (%d) => (%d)\n\n\n\n\n", s->DirX, s->AgainstImageWidth,   _dir_horizontal);

 

    pCmdReceiver->set_turnHeading_Msg(_dir_horizontal, pCmdReceiver->Image_W);

    pCmdReceiver->publish_turnHeading_Msg();







    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}

void* CmdReceiver::handle_stop(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;

  //  struct TouchInputCmd *pCmd= (struct TouchInputCmd *)para;
    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;
    

    ROS_DEBUG("\n\n\nSTOP ALL TASKS!!!\n\n\n\n");
    
    pCmdReceiver->goAbortmsg.Abort = true;
    pCmdReceiver->publish_abort_Msg();





    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}

void* CmdReceiver::handle_hSurrounding(struct control_conn *s, void *para, int *ret)
{
    int send_num = 0;

    CmdReceiver* pCmdReceiver=( CmdReceiver*)para;

    int hDir = s->HDir;
    double hDegree = s->HDegree;
    double radius = s->Radius;
    int againstROI = s->AgainstROI;

    pCmdReceiver->set_surround_Msg( hDir,hDegree, radius,againstROI);

    pCmdReceiver->publish_surround_Msg();




    ROS_DEBUG("\n\n\n\n\nhSurrounding (against %s), dir=%s, degree=%f , radius=%f\n\b\b", (againstROI)?"ROI":"nothing", (hDir==0)?"cw":"ccw", hDegree, radius );
    printf("\n\n\n\n\nhSurrounding (against %s), dir=%s, degree=%f , radius=%f\n\b\b", (againstROI)?"ROI":"nothing", (hDir==0)?"cw":"ccw", hDegree, radius);

    //to acid: launch your cmd here

    send_num = sprintf((char *)s->send_buf, "S|0\r\n\r\n");
#if (LOG_LEVEL >=2)
    fprintf(stderr, "%s\n", s->send_buf);
#endif
    *ret = send_num;
    return NULL;
}


#if 0
void CmdReceiver::camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg, void *pEnvObj)
{
    ROS_DEBUG("camerainfoCallback: -A");
    struct TouchInputCmd *pCmd= (struct TouchInputCmd *)pEnvObj;
    pCmd->Image_W = msg->width;
    pCmd->Image_H = msg->height;



  //  test.Image_W= msg->width;
  //  test.Image_H= msg->height;
}

#else
void CmdReceiver::camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg)
{
  //  ROS_DEBUG("camerainfoCallback: -B, %d %d",msg->width,msg->height  );
 //   struct TouchInputCmd *pCmd= &(this->LastCmd);
    Image_W = msg->width;
    Image_H = msg->height;
 
}
#endif

#if 0
void CmdReceiver::publishROI()
{
    if(updateROI)
    {
        roimsg.x= roi_x;//roi_x;
        roimsg.y= roi_y;//roi_y;
        roimsg.roi_width= roi_width;//roi_width;
        roimsg.roi_height= roi_height;//roi_height;
        roimsg.image_width= Image_W;// Image_W;
        roimsg.image_height= Image_H;//Image_H;  

        printf("roimsg => (%d,%d,%d,%d,%d,%d)\n",   roimsg.x,  roimsg.y, 
                                                roimsg.roi_width, roimsg.roi_height, 
                                              roimsg.image_width, roimsg.image_height );

        if( roimsg.roi_width!=0&&roimsg.roi_height!=0&&roimsg.image_width!=0&&roimsg.image_height!=0)
            roi_pub.publish(roimsg);

        updateROI=false;
    }
 
}


void CmdReceiver::publishPOI()
{
 //   if(updateROI)
    {
        poimsg.x= poi_x;//roi_x;
        poimsg.y= poi_y;//roi_y;
        

        printf("poimsg => (%d,%d )\n",   poimsg.x,  poimsg.y);
                                        

    //    if( roimsg.roi_width!=0&&roimsg.roi_height!=0&&roimsg.image_width!=0&&roimsg.image_height!=0)
            poi_pub.publish(poimsg);

    //    updateROI=false;
    }
   
 
}


#endif



void CmdReceiver::publish_abort_Msg()
{
        ROS_DEBUG("goAbortmsg...........",goAbortmsg.Abort);
        abort_pub.publish(goAbortmsg);

        m_CmdToPublish = CMD_TO_PUBLISH_NONE;  
            
}

void CmdReceiver::publish_goROI_Msg()
{
    if(m_CmdToPublish = CMD_TO_PUBLISH_GOROI) {
        ROS_DEBUG("roimsg => (%d,%d,%d,%d,%d,%d)\n",   goROImsg.x,  goROImsg.y, 
                                                goROImsg.roi_width, goROImsg.roi_height, 
                                              goROImsg.image_width, goROImsg.image_height );
     
    
        if( goROImsg.roi_width!=0 && goROImsg.roi_height!=0 && 
            goROImsg.image_width!=0 && goROImsg.image_height!=0)
            roi_pub.publish(goROImsg);

        m_CmdToPublish = CMD_TO_PUBLISH_NONE;  
            
         

     
    }
}

void CmdReceiver::set_goROI_Msg( int roi_x, int roi_y,   /*Top-left point (x,y)*/
                                int roi_w, int roi_h,   /*width and height of the ROI*/
                                int image_w, int image_h   /*image width and height which above coordiantes reference to*/ )
{
    goROImsg.x = roi_x;
    goROImsg.y = roi_y;
    goROImsg.roi_width = roi_w;
    goROImsg.roi_height = roi_h;
    goROImsg.image_width= image_w;
    goROImsg.image_height= image_h;  

    m_CmdToPublish = CMD_TO_PUBLISH_GOROI;
}

void CmdReceiver::publish_goDir_Msg()
{
    if(m_CmdToPublish = CMD_TO_PUBLISH_GODIR) {
        ROS_DEBUG("dirmsg => (%d,%d,%d,%d,%d,%d)\n",   goDirmsg.x,  goDirmsg.y, 
                                                    goDirmsg.image_width, goDirmsg.image_height );

        if( goDirmsg.image_width!=0 && goDirmsg.image_height!=0)
            poi_pub.publish(goDirmsg);

        m_CmdToPublish = CMD_TO_PUBLISH_NONE;       
    }
}

void CmdReceiver::set_goDir_Msg( int x, int y,   /*point (x,y)*/
                                 int image_w, int image_h   /*image width and height which above coordiantes reference to*/ )
{
    goDirmsg.x = x;
    goDirmsg.y = y;
    goDirmsg.image_width= image_w;
    goDirmsg.image_height= image_h;  

    m_CmdToPublish = CMD_TO_PUBLISH_GODIR;
}

void CmdReceiver::publish_shiftMov_Msg()
{
    if(m_CmdToPublish = CMD_TO_PUBLISH_SHIFTMOVE) {
        ROS_DEBUG("shiftMovmsg => (%f,%f,%f)\n",   shiftMovmsg.speedy,  shiftMovmsg.speedz, shiftMovmsg.duration);

        if( shiftMovmsg.speedy!=0 || shiftMovmsg.speedz!=0){
            shiftMov_pub.publish(shiftMovmsg);
           ROS_DEBUG("shiftMov_pub PUBLISH-----------------------------------");
        }

        m_CmdToPublish = CMD_TO_PUBLISH_NONE;       
    }
}

void CmdReceiver::set_shiftMov_Msg( double dSpeedY, double dSpeedZ, /*speed in horizontal and vertical */
                                    double dDuration)
{
    shiftMovmsg.speedy = dSpeedY;
    shiftMovmsg.speedz = dSpeedZ;
    shiftMovmsg.duration = dDuration;

    m_CmdToPublish = CMD_TO_PUBLISH_SHIFTMOVE;
}



void CmdReceiver::publish_turnHeading_Msg()
{
    if(m_CmdToPublish = CMD_TO_PUBLISH_TURNHEADING) {
        ROS_DEBUG("turnHeadingmsg => (%d,%d)\n",  turnHeadingmsg.disp_horizontal,  turnHeadingmsg.image_width);

        if( turnHeadingmsg.image_width!=0 && turnHeadingmsg.disp_horizontal!=0){
            turnHeading_pub.publish(turnHeadingmsg);
        
        }

        m_CmdToPublish = CMD_TO_PUBLISH_NONE;       
    }
}

void CmdReceiver::set_turnHeading_Msg( int disp_horizontal,     /*the horizontal movement in px*/
                                       int image_w  )
{
    turnHeadingmsg.disp_horizontal = disp_horizontal;
    turnHeadingmsg.image_width = image_w;

    m_CmdToPublish = CMD_TO_PUBLISH_TURNHEADING;
}


void CmdReceiver::publish_surround_Msg()
{
    
    surround_pub.publish(surroundmsg);
    ROS_DEBUG("surroun_pub PUBLISH-----------------------------------");

}

void CmdReceiver::set_surround_Msg( unsigned int hDir, 
                                    double hDegree, 
                                    double radius,
                                    unsigned int AgainstROI)
{

    surroundmsg.Dir = hDir;
    surroundmsg.Degree = hDegree;
    surroundmsg.radius = radius;
    surroundmsg.AgainstROI = AgainstROI;




}





} 

