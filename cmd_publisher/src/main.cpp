#include "cmd_publisher.h"
 


void set_logger_level(int level)
{


    enum{Debug=0,Info=1,Warn=2,Error=3,Fatal=4,None=5};

    switch(level)
    {
        case  Debug: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Info: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Warn: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Warn)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Error: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Error)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
      //  case  None: 
     //       if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::None)) 
    //            ros::console::notifyLoggerLevelsChanged(); 
     //   break;

        default:
 
        break;

    }
   
    
}


 

int main(int argc, char **argv)
{ 
  
  ros::init(argc, argv, "roi"); 

  set_logger_level(2);






  Roi_Info::CmdReceiver roio;
  ros::Rate loop_rate(10); 

  bool test=false;
  while (ros::ok())
  {
    //joe note:
    //later work:
    //the roimsg should only be published if mode==1
    //the poimsg should only be published if mode==2
    //for mode==0, nothing should be done
    /*ROS_INFO("mode=%d, ROI=[%d,%d,%d,%d], POI=[%d,%d]. image=[%d,%d]n", 
                          CurrentCmd.mode, CurrentCmd.ROI_LT_X, CurrentCmd.ROI_LT_Y, 
                          CurrentCmd.ROI_W, CurrentCmd.ROI_H, 
                          CurrentCmd.Center_X, CurrentCmd.Center_Y, 
                          CurrentCmd.Image_W, CurrentCmd.Image_H);*/
 
    
     

      ros::spinOnce(); //put this here or outside the if block

      loop_rate.sleep();  
      
  }


  return 0;
}
 
