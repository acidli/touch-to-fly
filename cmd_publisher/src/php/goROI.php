<?php
	$data = json_decode(file_get_contents('php://input'), true);
	
	$x1 = isset($data["x1"])?$data["x1"]:"";
	$y1 = isset($data["y1"])?$data["y1"]:"";
	$x2 = isset($data["x2"])?$data["x2"]:"";
	$y2 = isset($data["y2"])?$data["y2"]:"";
	$width = isset($data["w"])?$data["w"]:"";
	$height = isset($data["h"])?$data["h"]:"";
	
	if($x1=="" || $x2==""){
		$res["code"] ="2211";				
	}else if($y1=="" || $y2==""){
		$res["code"] ="2212";
	}else if($width==""){
		$res["code"] ="2214";			
	}else if($height==""){
		$res["code"] ="2215";						
	}
	else {
		$res = getList();
	}
	
	
	
	//if ($errcode != "") echo "E|".$errcode; 
	//else echo $qry_res ;
	echo json_encode($res);
	
	function getList(){
		global $x1,$x2,$y1,$y2,$width,$height;
		if(($x1<0)||($x1>$width) || ($x2<0)||($x2>$width)){
			$res["code"] ="2311";	
			return $res;
		}
		else if (($y1<0)||($y1>$height) || ($y2<0)||($y2>$height)){
			$res["code"] ="2312";	
			return $res;			
		}
		else if ( $width <= 0){
			$res["code"] ="2314";	
			return $res;			
		}
		else if ( $height <= 0 ){
			$res["code"] ="2315";	
			return $res;			
		}	
		$serverip="127.0.0.1";
			
		$ctrlport=1021;
		$fp = fsockopen($serverip, $ctrlport, $errno, $errstr, 5);
		if (!$fp) 
		{
			$res["code"]="2361"; // cannot open socket
			return $res;
		} 
		else 
		{
			$req="goROI\r\nLX: $x1\r\nLY: $y1\r\nRX: $x2\r\nRY: $y2\r\nIMAGEW: $width\r\nIMAGEH: $height\r\n\r\n";
			fwrite($fp, $req);
			if (!feof($fp)) {
				$response=fgets($fp, 1024);
				list($status,$error,$code)=explode("|",$response);
				if($status=="S")
				{		$res["code"]=0;
				}
				else
					//$retMsg= "$error : $code <br>";
					$res["code"]="2362"; //set Error
			}
			fclose($fp);				
		}
		return $res;		
	}	
	
?>
