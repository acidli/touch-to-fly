<?php
	$data = json_decode(file_get_contents('php://input'), true);
	
	$x = isset($data["x"])?$data["x"]:"0.0";
	$y = isset($data["y"])?$data["y"]:"0.0";
	$duration = isset($data["duration"])?$data["duration"]:"1.0";
	
	if($x==""){
		$res["code"] ="2211";	//theoratically it is not gonna happened		
	}
	else if($y==""){
		$res["code"] ="2212";	//theoratically it is not gonna happened	
	}
	else if($duration==""){
		$res["code"] ="2231";	//theoratically it is not gonna happened	
	}
	else {
		$res = getList();
	}
	//if ($errcode != "") echo "E|".$errcode; 
	//else echo $qry_res ;
	echo json_encode($res);
	
	function getList(){
		global $x,$y,$duration;
		if(($x<-1.0)||($x>1.0)){
			$res["code"] ="2311";	
			return $res;
		}
		else if(($y<-1.0)||($y>1.0)){
			$res["code"] ="2312";	
			return $res;
		}
		else if(($duration<0.0)||($duration>2.0)){
			$res["code"] ="2331";	
			return $res;
		}

		//this command allows empty input (all the shfit direction value are '0')
		//and in such case just skip the command
		if( $x==0 && $y==0) {
			$res["code"]=0;
			return $res;
		}

		$serverip="127.0.0.1";
		
		$ctrlport=1021;
		$fp = fsockopen($serverip, $ctrlport, $errno, $errstr, 5);
		if (!$fp) 
		{
			$res["code"]="2361"; // cannot open socket
			return $res;
		} 
		else 
		{
			$req="shiftMov\r\nSpeedX: $x\r\nSpeedY: $y\r\nDuration: $duration\r\n\r\n";
			fwrite($fp, $req);
			if (!feof($fp)) {
				$response=fgets($fp, 1024);
				list($status,$error,$code)=explode("|",$response);
				if($status=="S")
				{		$res["code"]=0;
				}
				else
					//$retMsg= "$error : $code <br>";
					$res["code"]="2362"; //set Error
			}
			fclose($fp);				
		}
		return $res;		
	}	
	
?>
