<?php
	$data = json_decode(file_get_contents('php://input'), true);
	
	$x = isset($data["x"])?$data["x"]:"";
	$y = isset($data["y"])?$data["y"]:"";
	$width = isset($data["w"])?$data["w"]:"";
	$height = isset($data["h"])?$data["h"]:"";
	
	if($x==""){
		$res["code"] ="2211";				
	}
	else if($y==""){
		$res["code"] ="2212";
	}
	else if($width==""){
		$res["code"] ="2214";			
	}
	else if($height=""){
		$res["code"] ="2215";						
	}
	else {
		$res = getList();
	}
	
	//if ($errcode != "") echo "E|".$errcode; 
	//else echo $qry_res ;
	echo json_encode($res);
	
	function getList(){
		global $x,$y,$width,$height;
		if(($x<0)||($x>$width)){
			$res["code"] ="2311";	
			return $res;
		}
		else if (($y<0)||($y>$height)){
			$res["code"] ="2312";	
			return $res;			
		}		
		else if ( $width <= 0){
			$res["code"] ="2314";	
			return $res;			
		}
		else if ( $height <= 0 ){
			$res["code"] ="2315";	
			return $res;			
		}
		
		$serverip="127.0.0.1";
		
		$ctrlport=1021;
		$fp = fsockopen($serverip, $ctrlport, $errno, $errstr, 5);
		if (!$fp) 
		{
			$res["code"]="2361"; // cannot open socket
			return $res;
		} 
		else 
		{
			$req="goDir\r\nDirX: $x\r\nDirY: $y\r\nIMAGEW: $width\r\nIMAGEH: $height\r\n\r\n";
			fwrite($fp, $req);
			if (!feof($fp)) {
				$response=fgets($fp, 1024);
				list($status,$error,$code)=explode("|",$response);
				if($status=="S")
				{		$res["code"]=0;
				}
				else
					//$retMsg= "$error : $code <br>";
					$res["code"]="2362"; //set Error
			}
			fclose($fp);				
		}
		return $res;		
	}	
	
?>
