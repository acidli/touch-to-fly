<?php
	$data = json_decode(file_get_contents('php://input'), true);
	
	$dir = isset($data["dir"])?$data["dir"]:"";
	$degree = isset($data["degree"])?$data["degree"]:"";
	$radius = isset($data["radius"])?$data["radius"]:"0.0";
	$againstROI = isset($data["againstROI"])?$data["againstROI"]:"1";
	
	if($dir==""){
		$res["code"] ="1231";	//dir is empty		
	}
	else if($degree==""){
		$res["code"] ="1232";	//degree is empty
	}
	else {
		$res = getList();
	}

    //if ($errcode != "") echo "E|".$errcode; 
	//else echo $qry_res ;
	echo json_encode($res);
	
	function getList(){
		global $degree,$dir,$radius, $againstROI;
		if(($degree<3.0)||($degree>360.0)){
			$res["code"] ="2332";	
			return $res;
		}
		else if( (int)$radius!=0 && ((float)$radius > 50.0 || (float)$radius < 3.0) ){
			$res["code"] ="2333";	
			return $res;
		}

        $dir_index = 0;
        if($dir == "cw") {
            $dir_index=0;
        }
        else if($dir == "ccw") {
            $dir_index=1;
        }
        else {
			$res["code"] ="2335";	
			return $res;
		}
		
		if($againstROI ==0 && (int)$radius==0) {
			$res["code"] = "2334";
			return $res;
		}

        $serverip="127.0.0.1";
		
		$ctrlport=1021;
		$fp = fsockopen($serverip, $ctrlport, $errno, $errstr, 5);
		if (!$fp) 
		{
			$res["code"]="2361"; // cannot open socket
			return $res;
		} 
		else 
		{
			$req="hSurrounding\r\nHDir: $dir_index\r\nHDegree: $degree\r\nRadius: $radius\r\nAgainstROI: $againstROI\r\n\r\n";
			fwrite($fp, $req);
			if (!feof($fp)) {
				$response=fgets($fp, 1024);
				list($status,$error,$code)=explode("|",$response);
				if($status=="S")
				{		$res["code"]=0;
				}
				else
					//$retMsg= "$error : $code <br>";
					$res["code"]="2362"; //set Error
			}
			fclose($fp);				
		}
		return $res;		
	}	
	
?>
