<?php
	$data = json_decode(file_get_contents('php://input'), true);

	$cmd = isset($data["cmd"])?$data["cmd"]:"";
	/*
		commands available:
			mov_to_pos: in this case the value in axis_x/axis_y/axis_z
				    represents the position on each axis ; and speed is the moving speed
			mov_by_vel: in this case the value in axis_x/axis_y/axis_z
				    represntes the velocity along each axis; and duration is the moving duration
			mov_by_att: int his case the value in axis_x/axus_y/axis_z
				    represents the rotation angle to each axis and height is the altitude to keep
			turn_heading: in this case the value in axis_z is for yaw angle
				    and axis_x/axis_y are don't-care
			takeoff:  int this case the value in axis_z is for the height for take-off
                                    and axis_x/axis_y are don't care;
				    when calling this command it automatically do following steps
				    1. switch_mode("GUIDED")
				    2. arm_disarm(True)
				    3. takeoff(axis_z)
			land:      int this case all axis_x/axis_y/axis_z are don't-care;
				    when called, it automaticcally do following steps
				    1. land()
				    2. arm_disarm(False)
			to_mode:   switch the flight mode, in this command all parameters are ignored except "mode";
				  currently the valid valie is 'guided'
	*/

	$axis_x = isset($data["axis_x"])?$data["axis_x"]:"";
	$axis_y = isset($data["axis_y"])?$data["axis_y"]:"";
	$axis_z = isset($data["axis_z"])?$data["axis_z"]:"";

	$speed_for_mov_to_pos = isset($data["speed_for_mov_to_pos"])?$data["speed_for_mov_to_pos"]:"3";		//1m/s (3.6Km/h) if not set 
	$duration_for_mov_by_vel = isset($data["duration_for_mov_by_vel"])?$data["duration_for_mov_by_vel"]:"1";	//1 sec if not set
#if 0	//currently not enabled
	$height_for_mov_by_att = isset($data["height"])?$data["height"]:"";		//mandatory, NO default value
#endif
	$flag_ned = isset($data["ned"])?$data["ned"]:"0";	//DO NOT use ned system by default
	$flag_block = isset($data["block"])?$data["block"]:"0"; //DO NOT use blocking mdoe by default

	$mode = isset($data["mode"])?$data["mode"]:"";
	
	if($cmd!="mov_to_pos" && $cmd!="mov_by_vel" && 
	   $cmd!="turn_heading" && $cmd!="takeoff" && $cmd!="land" && cmd!="to_mode")
	{
		$qry_res["code"]="2351";	//invalid command
		$qry_res["cmd"]=$cmd;
	}
	else {
		if($cmd=="mov_to_pos") {
			if ($axis_x == ""){
				$qry_res["code"] ="2211";
			}
			else if ($axis_y == ""){
				$qry_res["code"] ="2212";
			}
			else if ($axis_z == ""){
				$qry_res["code"] ="2213";
			}
			else {
				//mov_to_pos
				$qry_res = mov_to_pos();
			}
		}
		else if($cmd=="mov_by_vel") {
			if ($axis_x == ""){
				$qry_res["code"] ="2211";
			}
			else if ($axis_y == ""){
				$qry_res["code"] ="2212";
			}
			else if ($axis_z == ""){
				$qry_res["code"] ="2213";
			}
			else {
        	    //mov_by_vel
				$qry_res = mov_by_vel();
			}		
		}
		else if($cmd=="turn_heading") {
			if( ($axis_z == "")) {
				$qry_res["code"]="2213";
			}
			else {
				//turn
				$qry_res = turn_heading();
			}
		}
		else if($cmd=="takeoff") {
			if( ($axis_z == "")) {
				$qry_res["code"]="2213";
				//takeoff
			}
			else {
				$qry_res = takeoff();
			}
		}

	        else if($cmd=="land") {
        		//land
			$qry_res = land();
        	}
                else if($cmd=="to_mode") {
                        //land
                        $qry_res = to_mode();
                }
		else {
			$qry_res["code"]="2351";
		}
	}
	
	//if ($errcode != "") echo "E|".$errcode; 
	//else echo $qry_res ;
	echo json_encode($qry_res);
	
	function mov_to_pos() {
		global $axis_x,$axis_y,$axis_z, $speed_for_mov_to_pos, $flag_ned, $flag_block;
		$req="EZCmd\r\ncmd: mov_to_pos\r\naxis_x: $axis_x\r\naxis_y: $axis_y\r\naxis_z: $axis_z\r\nspeed_for_mov_to_pos: $speed_for_mov_to_pos\r\nned: $flag_ned\r\nblock: $flag_block\r\n\r\n";
		return _send($req);
	}

	function mov_by_vel() {
		global $axis_x,$axis_y,$axis_z, $duration_for_mov_by_vel, $flag_ned, $flag_block;
                $req="EZCmd\r\ncmd: mov_by_vel\r\naxis_x: $axis_x\r\naxis_y: $axis_y\r\naxis_z: $axis_z\r\nduration_for_mov_by_vel: $duration_for_mov_by_vel\r\nned: $flag_ned\r\nblock: $flag_block\r\n\r\n";
		return _send($req);
	}

	function turn_heading() {
		global $axis_z, $flag_ned, $flag_block;
                $req="EZCmd\r\ncmd: turn_heading\r\naxis_z: $axis_z\r\nned: $flag_ned\r\nblock: $flag_block\r\n\r\n";
		return _send($req);
	}

	function takeoff() {
		global $axis_z, $flag_block;
                $req="EZCmd\r\ncmd: takeoff\r\naxis_z: $axis_z\r\nblock: $flag_block\r\n\r\n";
		return _send($req);
	}

	function land() {
                global $flag_block;
                $req="EZCmd\r\ncmd: land\r\nblock: $flag_block\r\n\r\n";
		return _send($req);
	}

        function to_mode() {
                global $mode;
                $req="EZCmd\r\ncmd: to_mode\r\nmode: $mode\r\n\r\n";
                return _send($req);
        }

	function _send($req){
		$serverip = "127.0.0.1";
		$ctrlport=1021;
		$fp = fsockopen($serverip, $ctrlport, $errno, $errstr, 5);
		if (!$fp) 
		{
			$res["code"]="2361"; // cannot open socket
			return $res;
		} 
		else 
		{
			fwrite($fp, $req);
			if (!feof($fp)) {
				$response=fgets($fp, 1024);
				list($status,$error,$code)=explode("|",$response);
				if($status=="S")
				{		$res["code"]=0;
				}
				else
					//$retMsg= "$error : $code <br>";
					$res["code"]="2362"; //set Error
			}
			fclose($fp);				
		}
		return $res;		
	}	
	
?>
