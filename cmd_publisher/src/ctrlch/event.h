

#ifndef _EVENT_H
#define _EVENT_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_OSLinux) //Wen-Fang: 2009.3.12
	#include <sys/time.h>
#else
	#include <sys/timeb.h>
#endif

#define EVENT_F_DISABLED	0
#define EVENT_F_ENABLED		1
#define EVENT_F_REMOVE		2
#define EVENT_F_RUNNING		4

struct socket_event;
struct timer_event;

typedef void (*callback) ( void *event, void *struc_parameter);

struct socket_event {
	struct socket_event *prev;
	struct socket_event *next;
	callback func; 	//event handler
	void *data;		//event data
	int status;		//event status
	int fd;			// socket fd
	int write;		// read/write type flag
	int isRTSPfd;		// RTSP listening socket or not 
	char debug_name[32]; 
}socket_event;


struct timer_event {
	struct timer_event *prev;
	struct timer_event *next;
	callback func; 	//event handler
	void *data;		//event data
	int status;		//event status
#if defined(_OSLinux)
	struct timeval due_time;	// due time of the event
#else
	//SYSTEMTIME	due_time;
	struct _timeb due_time;	//Wen-Fang: 2009.3.12
#endif
	int timer;		// timer interval
	char debug_name[32];
}timer_event;

struct event_info {
	struct socket_event *e;
	int type;
	void *data;
};

void *CC_event_loop( void *arg );

struct socket_event *CC_add_socket_event( int fd, int write, unsigned int status, callback f, void *d );
void CC_remove_socket_event( struct socket_event *e );

struct timer_event *CC_add_timer_event( int msec, unsigned int status, callback f, void *d );
void CC_remove_timer_event( struct timer_event *e );

#ifdef __cplusplus
}
#endif

#endif /* EVENT_H */

