#include "event.h"
#include "ControlChannel.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

static struct control_conn *cc_conn_list = NULL;
static int ccsocket_num = 0;

extern int end_loop;
#define MAXCTRLCHANNELUSER 5

static void controlchan_do_read( struct event_info *e, void *d );
void controlchan_drop_conn( struct control_conn *conn );
int process_control_cmd(struct control_conn *c);
int prepare_control_reply_msg(struct control_conn *c);



void* handle_other(struct control_conn *s, int *ret);
void* handle_UpdateROI(struct control_conn *s, int *ret);

/* Create a global config context pointing to the global_statements array
 * defined in global_config.h */
static struct ctrl_action_def global_module = {
	NULL, "OTHER", (action_handler_func)handle_other, NULL
};

//Wen-Fang: check socket status before send, 2012.5.25
int check_socket_ready(int socket)
{
	int fd_max;
	fd_set wfds;
	struct timeval tv;
	int ret;

	fd_max = socket;
	FD_ZERO(&wfds);
	FD_SET((unsigned int)socket, &wfds);
	tv.tv_sec = 0;
	tv.tv_usec = 100;

	ret = select(fd_max + 1, NULL, &wfds, NULL, &tv);
	if(ret == 0)
	{
#ifdef LOG
		printf("select socket timeout, timeout time = %d", tv.tv_usec);
#endif
		return 0;
	}			
	else if(ret == -1)
	{
#ifdef LOG
		printf("select socket error %d", WSAGetLastError());
#endif
		return -1;		
	}
	else 
	{
		if (FD_ISSET( socket, &wfds)) return 1;
		else 
		{
#ifdef LOG
			printf("select socket return success, but socket not in FD_SET??");
#endif
			return -1;
		}
	}

}

static struct ctrl_action_def *new_action( char *action_name)
{
	struct ctrl_action_def *action_def = NULL;

	action_def = (struct ctrl_action_def *)malloc( sizeof( struct ctrl_action_def ) );
	if(action_def == NULL) return NULL; // by hcc 10/14

	memset(action_def, 0, sizeof(struct ctrl_action_def)); //initial value
	action_def->next=NULL;
	strcpy(action_def->action_name,action_name);

	return action_def;
}

int register_control_action(char *action_str, action_handler_func para_handler, void *para)
{
	struct ctrl_action_def *reg_action, *last_action;

	reg_action = last_action = NULL;

	reg_action=new_action(action_str);
	if(reg_action == NULL) return -1;

	reg_action->action_handler = para_handler;
	reg_action->para = para;

	for( last_action = &global_module; last_action->next; last_action = last_action->next );
	last_action->next=reg_action;

	return 0;
}

void control_conn_disconnect( struct control_conn *c )
{
	close(c->fd);
#ifdef LOG
	printf("Close control channel connection from IP %s: %d", inet_ntoa( c->client_addr.sin_addr  ),c->fd);
#endif
}

static void controlchan_do_accept( struct event_info *ei, void *d )
{
	int *listen_fd = (int *)d;
	int fd;
	socklen_t  addrlen;
	struct sockaddr_in addr;
	struct control_conn *c;
	int sendbuf = 2048;
	int socket_timeout = 1000; //1 sec;  //Wen-Fang: 2012.5.31, for set socket send timeout

	addrlen = sizeof( addr );
	if( ( fd = accept( *listen_fd, (struct sockaddr *)&addr, &addrlen ) ) < 0 )
	{
#ifdef LOG
		printf("Accept new connection error : %s", strerror( errno ));
#endif
		return;
	}
	
	if(ccsocket_num >= MAXCTRLCHANNELUSER)
	{
		//fprintf(stderr, "no available session can be used\n");
#ifdef LOG
		printf("Accept new connection error: No available session, current connection : %d", ccsocket_num);
#endif
		close(fd);
		return;
	}else
		ccsocket_num++;	

#ifdef LOG
	printf("");
	printf("Accept new connection: IP: %s fd: %d", inet_ntoa(addr.sin_addr), fd);
	printf("current ctrl channel user = %d", ccsocket_num);
#endif

#if defined(_OSLinux)
	if( setsockopt( fd, SOL_SOCKET, SO_SNDBUF, (char *)&sendbuf, sizeof(sendbuf)) < 0 )
		fprintf(stderr, "Setsockopt error : %s", strerror( errno ) );
						
	//fprintf(stderr, "Accept connection from %s\n", inet_ntoa( addr.sin_addr ));
	fcntl( fd, F_SETFL, O_NONBLOCK );
#endif // _OSLinux
#if defined (_OSWIN32) //Wen-Fang: 2012.5.31, set socket send timeout
	if (setsockopt( fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&socket_timeout, sizeof(socket_timeout)) < 0)
#ifdef LOG
		printf("socket %d set send timeout failed. error = %d", fd, WSAGetLastError());
#endif
#endif
	
	
	c = (struct control_conn *)malloc( sizeof( struct control_conn ) );
#ifdef LOG
	printf("new ctrl_conn = %p", c);
#endif
	if(c == NULL)	
	{
#ifdef LOG
		printf("Accept new connection: malloc memory for control_conn failed");
#endif
		ccsocket_num--; //2011.4.12, Wen-Fang
		return; // by hcc 10/14
	}

	memset(c, 0, sizeof( struct control_conn ));
	c->read_event = CC_add_socket_event( fd, 0, 0, (callback)controlchan_do_read, c );
	if (c->read_event == NULL)
	{
#ifdef LOG
		printf("Accept new connection: add control channel read_event failed");
#endif
		free(c);
		ccsocket_num--;
		return;
	}
	
#ifdef LOG
	printf("add socket event %d(%p) - control command", fd, c->read_event);
#endif
	c->next = cc_conn_list;
	if( c->next ) c->next->prev = c;
	c->prev = NULL;
	c->fd = fd;
	c->client_addr = addr;
	cc_conn_list = c;
	c->msg_bufPtr = c->msg_buffer;
	c->msg_bufEnd = c->msg_buffer + MSG_BUFFER_SIZE - 1;//leave room for '\0'
	
	return;

}


static void controlchan_do_read( struct event_info *e, void *d )
{
	struct control_conn *c = (struct control_conn *)d;
	int		data_len=0, len;

#if defined(_OSLinux)
	data_len = read(c->fd, c->rcv_buf, sizeof( c->rcv_buf )-1 );
#endif // _OSLinux

#if defined(_OSWIN32)
	data_len = recv(c->fd, c->rcv_buf, sizeof( c->rcv_buf )-1, 0 );
#endif // _OSWIN32
	
	if(data_len < 0) { // receive remote side disconnect connection -> data_len will be 0
#if defined(_OSWIN32)
#ifdef LOG
		printf("controlchan_do_read: recv length < 0, error = %d", WSAGetLastError());
#endif
#endif // _OSWIN32
		controlchan_drop_conn( c );
		return;
	}
	else if (data_len == 0)
	{
#ifdef LOG
		printf("controlchan_do_read: socket %d recv length = 0, socket closed!", c->fd);
#endif
		controlchan_drop_conn( c );
		return;
	}

	c->rcv_len = data_len;
	if( c->rcv_len > sizeof( c->rcv_buf ) - 1 ) //would it be happened??
	{
		//printf("Request message is too long\n" );
#ifdef LOG
		printf("controlchan_do_read: Request message is over receiver buffer, len = %d", data_len);
#endif
		return;
	}

	c->rcv_buf[c->rcv_len] = 0;

    // See if this is an interleaved data packet
	do
	{
		len = process_control_cmd( c );
	} while(len > 0);

	if (len <0)
	{
#ifdef LOG
		printf("process_control_cmd return error %d", len);
#endif
		controlchan_drop_conn(c);
		return;
	}

	return;
}



void* handle_other(struct control_conn *s, int *ret)
{
	int send_num = 0;
	send_num = sprintf((char *)s->send_buf, "S|000|Handle Other OK|\n");
	fprintf(stderr, "%s\n", s->send_buf);
	*ret = send_num;
	return NULL;
}


int prepare_control_reply_msg(struct control_conn *c)
{
	struct ctrl_action_def *action_def;
	int ret;

	for( action_def = &global_module; action_def != NULL; action_def = action_def->next )
	{
		if (!strcasecmp(action_def->action_name, c->action))
		{
			action_def->action_handler(c, action_def->para, &ret);
			//action_def->action_handler2(c->rcv_buf, c->rcv_len);
			return ret;
		}
	}

	if (action_def == NULL) //No action name matched!
	{
		global_module.action_handler(c, NULL, &ret);
		return ret;
	}
		

	return 0;
}

void parse_control_parameter(char* line, struct control_conn *c)
{
	char seps[] = " :";
	char *name, *value;

	name = strtok(line, seps);
	value = strtok(NULL, seps);
	printf("key: %s => value %s\n", name, value);
	if (name == NULL || value == NULL)
		return;
	
	if (!strcasecmp(name, "account"))
	{
		memset(c->Account, 0, sizeof(c->Account));
		strcpy(c->Account, value);
	}
	else if (!strcasecmp(name, "Livename"))
	{
		memset(c->livename, 0, sizeof(c->livename));
		strcpy(c->livename, value);
	}
	else if (!strcasecmp(name, "DestVSS"))
	{
		memset(c->DestAddr, 0, sizeof(c->DestAddr));
		strcpy(c->DestAddr, value);
	}
	else if (!strcasecmp(name, "DestPort"))
	{
		c->DestPort = (unsigned short)atoi(value);
	}
	//starting of Touch-to-Fly Ctrl Commands
	else if (!strcasecmp(name, "DirX"))
	{
		c->DirX= atoi(value);
	}
	else if (!strcasecmp(name, "DirY"))
	{
		c->DirY= atoi(value);
	}
	else if (!strcasecmp(name, "LX"))
	{
		c->LX= atoi(value);
	}
	else if (!strcasecmp(name, "LY"))
	{
		c->LY= atoi(value);
	}
	else if (!strcasecmp(name, "RX"))
	{
		c->RX= atoi(value);
	}
	else if (!strcasecmp(name, "RY"))
	{
		c->RY= atoi(value);
	}
	else if (!strcasecmp(name, "SpeedX"))
	{
		c->SpeedX= atof(value);
	}
	else if (!strcasecmp(name, "SpeedY"))
	{
		c->SpeedY= atof(value);
	}
	else if (!strcasecmp(name, "Duration"))
	{
		c->ShiftMovDuration= atof(value);
	}
	else if (!strcasecmp(name, "IMAGEW"))
	{
		c->AgainstImageWidth= atoi(value);
	}
	else if (!strcasecmp(name, "IMAGEH"))
	{
		c->AgainstImageHeight= atoi(value);
	}
	//surrounding (including horizontal and vertical or mixed)
	else if (!strcasecmp(name, "HDir"))
	{
		c->HDir= atoi(value);
	}
	else if (!strcasecmp(name, "HDegree"))
	{
		c->HDegree= atof(value);
	}
	else if (!strcasecmp(name, "VDegree"))
	{
		c->VDegree= atof(value);
	}
	else if (!strcasecmp(name, "Radius"))
	{
		c->Radius= atof(value);
	}	
	else if (!strcasecmp(name, "AgainstROI"))
	{
		
		c->AgainstROI= atoi(value);
		printf(">>>>>>> %d", c->AgainstROI);
	}	
	//end of Touch-to-Fly Ctrl Commands
	//starting of C2V Ctrl Commands
	else if (!strcasecmp(name, "GimbalPan"))
	{
		c->GimbalPan= atof(value);
	}
	else if (!strcasecmp(name, "GimbalTilt"))
	{
		c->GimbalTilt= atof(value);
	}
	else if (!strcasecmp(name, "ZOOM"))
	{
		c->Zoom= atoi(value);
	}
	//end of C2V Ctrl Commands
	/*start of handling attributes for EZCmd */
	else if (!strcasecmp(name, "cmd"))
	{
		memset(c->EZCmd, 0, sizeof(c->EZCmd));
		strcpy(c->EZCmd, value);
	}
	else if (!strcasecmp(name, "axis_x"))
	{
		c->EZ_axis_x= atof(value);
	}
	else if (!strcasecmp(name, "axis_y"))
	{
		c->EZ_axis_y= atof(value);
	}
	else if (!strcasecmp(name, "axis_z"))
	{
		c->EZ_axis_z= atof(value);
	}	
	else if (!strcasecmp(name, "speed_for_mov_to_pos"))
	{
		c->EZ_speed_for_mov_to_pos= atof(value);
	}
	else if (!strcasecmp(name, "duration_for_mov_by_vel"))
	{
		c->EZ_duration_for_mov_by_vel= atof(value);
	}
	else if (!strcasecmp(name, "ned"))
	{
		c->EZ_ned= atoi(value);
	}
	else if (!strcasecmp(name, "block"))
	{
		c->EZ_block= atoi(value);
	}
	return;
}


/*-----------------------------------------------------------------------------------
Control Command Format:

ActionName\r\n 
ParameterName: ParameterValue\r\n
[more parameter...\r\n]
\r\n

//example: 
RELAY\r\n
Livename: pcam0001\r\n
DestVSS: 192.168.1.2\r\n
DestPort: 554\r\n
\r\n

parse_control_parameter:
�i�s�W�s��parameter�W�ٻP���oparameter value
�i�bstruct control_conn�s�W�ܼ��x�s parameter value
-----------------------------------------------------------------------------------*/

int process_control_cmd(struct control_conn *c)
{
	int	data_len=0;
	int	buffer_diff;
	int ret;
	unsigned char *ptr;
	int ctrlmsg_len, reply_len = 0;
	char *token;
	char seps[] = "\r\n";
	char *cmd, *line;
	
	data_len = c->rcv_len - c->rcv_read_offset;

	if (data_len == 0)
	{
#ifdef LOG
		printf("process_control_cmd: recv data_len = 0, no new data");
#endif
		return data_len;
	}else if (data_len < 0)
	{
#ifdef LOG
		printf("process_control_cmd: error recv data_len = %d", data_len);
#endif
		return ERR_INVALID_BUFFERDATA;
	}

	//search the end  of the request
	buffer_diff = c->msg_bufPtr - c->msg_buffer;
	if (buffer_diff < 0)
	{
#ifdef LOG
		printf("process_control_cmd: error data_len in message_buffer = %d", buffer_diff);
#endif
		return ERR_INVALID_BUFFERDATA;
	}

	// over msg_buffer size
	if (buffer_diff + data_len > MSG_BUFFER_SIZE - 1)  
	{
		memcpy(c->msg_bufPtr, c->rcv_buf + c->rcv_read_offset, MSG_BUFFER_SIZE - buffer_diff - 1);
		*c->msg_bufEnd = '\0';
	}
	else
	{
		memcpy(c->msg_bufPtr, c->rcv_buf + c->rcv_read_offset, data_len);
	}

#ifdef LOG
	printf("ctrl msg from fd %d : %s", c->fd, c->msg_bufPtr);
#endif

	//find the end of control command
	while (data_len > 0)
	{
		if (c->msg_bufPtr +1 > (c->msg_bufEnd - 1))
		{
#ifdef LOG
			printf("process_control_cmd: Control message is too long (over size 4096)");
#endif
			return ERR_CTRLMSG_TOOLONG;
		}

		data_len--;
		c->msg_bufPtr++;
		ptr = c->msg_bufPtr;
		if (  (ptr >= c->msg_buffer + 2 && !memcmp(ptr-2, "\n\n", 2)) ||
			  (ptr >= c->msg_buffer + 4 && !memcmp(ptr-4, "\r\n\r\n", 4))  ) 
		{
			ctrlmsg_len = c->msg_bufPtr - c->msg_buffer;
			*(c->msg_bufPtr + 1) = '\0';

			cmd = (char *)c->msg_buffer;
			token = strtok(cmd, "\r\n");
			cmd += strlen(token)+strlen("\r\n");
			
			if (token)
			{
				sprintf(c->action, "%s", token);
				fprintf(stderr, "control action = %s\n", c->action);
			}

		
			while (line = strtok(cmd, "\r\n"))
			{
				cmd += strlen(line) + strlen("\r\n");
				parse_control_parameter(line, c);
			}
			reply_len = prepare_control_reply_msg(c); 

			if (reply_len > 0) //send message in send_buf to control request
			{
				ret = check_socket_ready(c->fd); //select socket before send, Wen-Fang: 2012.5.25
				if (ret > 0)
				{
					ret = send(c->fd, c->send_buf, strlen((char *)c->send_buf), 0);
					if(ret <= 0)
					{
#ifdef LOG
						printf("process_control_cmd: send return error %d", WSAGetLastError());
#endif
						return ERR_SEND_REPLYMSG;
					}
				}
				else
				{
					return ERR_SEND_REPLYMSG;
				}
			}
			
			
			c->rcv_read_offset = c->rcv_len - data_len;
			memset(c->msg_buffer, 0, sizeof(c->msg_buffer));
			c->msg_bufPtr = c->msg_buffer;

			return data_len;
		}
	}
	
	//data_len = 0, it should receive more data from socket
	return 0;

}

void controlchan_drop_conn( struct control_conn *conn ) //modify by Wen-Fang: 2012.11.6
{
	
	if (conn) //2011.4.12, Wen-Fang
	{
		if (conn->fd)
		{
			//printf("cc_drop_conn: conn->fd = %d", conn->fd);
			ccsocket_num--;	
			fprintf(stderr, "controlchan_drop_conn user number=%d\n", ccsocket_num);
			
			control_conn_disconnect( conn ); //close socket
			
			if (conn->read_event)
			{
				CC_remove_socket_event( conn->read_event );
				conn->read_event = NULL; //2011.4.12, Wen-Fang
			}
			
			if( conn->next ) conn->next->prev = conn->prev;
			if( conn->prev ) conn->prev->next = conn->next;
			else cc_conn_list = conn->next;
			
			conn->fd = 0;
		}
		else
#ifdef LOG
			printf("cc_drop_conn: conn->fd = 0");
#endif
		
		if(!conn->bIsCasterConnecting)
		{
			//bIscasterConnecting is set when post video thread is finished
			//printf("cc_drop_conn: bIsCasterConnecting = 0");
			free( conn );
			conn = NULL; //2011.4.12, Wen-Fang
		}
		//else
			//printf("cc_drop_conn: bIsCasterConnecting = 1");
	}
	
	return;
}

int control_channel_init(unsigned short port)
{
	struct sockaddr_in addr;
	int opt, fd;
	int *listen_fd;
	struct socket_event *e;
//	int i;

#if defined(_OSWIN32)
	WORD		wVersionRequested;
	WSADATA		wsaData;
	int iErr;

	wVersionRequested = MAKEWORD( 2, 2 );
	iErr = WSAStartup( wVersionRequested, &wsaData );
	if ( iErr != 0 )
	{
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		return -1;
	}
#endif // _OSWIN32

	if( ( fd = socket( PF_INET, SOCK_STREAM, 0 ) ) < 0 )
	{
		printf("control_channel_init(): error creating listen socket: %s\n", strerror( errno ) );
		return -1;
	}
	opt = 1;
	if( setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof( opt ) ) < 0 ) {
		printf("setsockopt error: %s\n", strerror( errno ) );
	}
				
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = 0;
	addr.sin_port = htons( port );
	if( bind( fd, (struct sockaddr *)&addr, sizeof( addr ) ) < 0 )
	{
		printf("control_channel_init(): bind socket error: %s\n", strerror( errno ) );
		close( fd );
		return -1;
	}
	if( listen( fd, 5 ) < 0 )
	{
		printf("control_channel_init(): listen socket error: %s", strerror( errno ) );
		close( fd );
		return -1;
	}

	listen_fd =  (int *)malloc(sizeof(int));
	if(listen_fd == NULL) // by hcc 10/14
	{
		printf("control_channel_init(): malloc (listen_fd) fail\n");
		return -1; 
	}

	//printf("malloc\n");
	*listen_fd = fd;
	
	//struct socket_event *add_socket_event( int fd, int write, unsigned int status, callback f, void *d )
	//d: private data for fd
	e = CC_add_socket_event( fd, 0, 0, (callback)controlchan_do_accept, (void *)listen_fd);
	if(e == NULL)
	{
		printf("control_channel_init(): add_socket_event fail\n");
		return -1;
	}

	e->isRTSPfd = 1;
	
	return 0;
}



void start_control_channel_loop()
{

#if defined(_OSLinux)
	pthread_t controlchannel_thread;
#endif

#if defined(_OSWIN32)
	HANDLE controlchannel_thread;
	DWORD controlchannel_threadID;
#endif

#if defined(_OSLinux)
	pthread_create( &controlchannel_thread, NULL, CC_event_loop, NULL);	
#endif // _OSLinux

	//create thread for running event_loop
#if defined(_OSWIN32)
	controlchannel_thread = (HANDLE) _beginthreadex(NULL, 256, CC_event_loop, NULL, 0, &controlchannel_threadID);
	if (!controlchannel_thread)
#ifdef LOG
		printf("para_handler(): create stream_loop thread failed, err = %d(%s) ", errno, strerror(errno));
#endif
#endif // _OSWIN32


	return;
}

void end_control_channel_loop()
{
	end_loop = 1;
	return;
}
