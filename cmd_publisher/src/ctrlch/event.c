/* *********************************************************************
 * 	Description :												
 *		
 *
 * 	Authors : 
 *		Jen-Yu Yu <KevinYu@itri.org.tw>
 *		Zheng-sheng weng <weng@itri.org.tw>
 * 		Hsin-hua lee <Daisylee@itri.org.tw>
 * 	
 *	Copyright(c) 2007 Information & Communication Research Laboratories,
 * 	Industrial Technology Research Institute (http://www.itri.org.tw)
 * 	All rights reserved.
 *
 ************************************************************************/

#define FD_SETSIZE      1024  //Wen-Fang:2009.3.19

#include "event.h"
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>

static struct socket_event *socket_event_list = NULL;
static struct timer_event *timer_event_list = NULL;
static int socket_event_list_length = 0;
static int timer_event_list_length = 0;

int end_loop = 0; //Wen-Fang: for stopping program

#if defined(_OSLinux)
static int time_ago(  struct timeval *tr )
{
	struct timeval now;
	int diff;
	gettimeofday( &now, NULL );
	diff=( ( now.tv_sec - tr->tv_sec ) * 1000000+ now.tv_usec - tr->tv_usec + 500 ) / 1000;

	return diff;
}
#else
//Wen-Fang: 2009.3.12
static int time_ago(  struct _timeb *tr )
{
	struct _timeb now;
	int diff;
	_ftime( &now );

	//printf("now: %d:%d, due: %d:%d, \n", now.time, now.millitm, tr->time, tr->millitm);
	diff=( ( now.time - tr->time ) * 1000 + now.millitm - tr->millitm + 50 );

	if (diff > 1000) //5sec
#ifdef LOG
		printf("time_ago diff = %d, now= %I64d, due = %I64d", diff, now.time, tr->time);
#endif
	return diff;
}
#endif

#if defined(_OSLinux)
static void time_add( struct timeval *tr, int msec )
{
	tr->tv_sec += msec / 1000;
	tr->tv_usec += ( msec % 1000 ) * 1000;
	if( tr->tv_usec >= 1000000 )
	{
		tr->tv_usec -= 1000000;
		++tr->tv_sec;
	}
}
#else
//Wen-Fang: 2009.3.12
static void time_add( struct _timeb *tr, int msec )
{
	tr->time += msec / 1000;
	tr->millitm += (msec % 1000);
	
	if( tr->millitm >= 1000 )
	{
		tr->millitm -= 1000;
		++tr->time;
	}
}

#endif

#if defined(_OSLinux)
static void time_future(struct timeval *tr, int msec )
{
	gettimeofday( tr, NULL );
	time_add( tr, msec );
}
#else
//Wen-Fang: 2009.3.12
static void time_future(struct _timeb *tr, int msec )
{
	_ftime( tr );
	time_add( tr, msec );
}
#endif


#if defined(_OSLinux)
static void schedule_timer_event( struct timer_event *e, struct timeval *tr )
#else
//Wen-Fang: 2009.3.12
static void schedule_timer_event( struct timer_event *e, struct _timeb *tr )
#endif
{
	if( tr )
		e->due_time= *tr;
	else if( e->status & EVENT_F_ENABLED )
		time_add( &(e->due_time), e->timer );
	else
		time_future( &(e->due_time), e->timer);

	e->status &= ~EVENT_F_REMOVE;
	e->status |= EVENT_F_ENABLED;
#ifdef SHOW_COMPLETE_MESSAGE
	printf("[VStreamer] schedule_timer_event : %d\n",e->timer);
#endif
}


static struct timer_event *new_timer_event( callback f, void *d )
{
	struct timer_event *e = NULL;
	e = (struct timer_event *)malloc( sizeof( struct timer_event ) );
	if(e == NULL) return NULL;
	
	//printf("timer event pointer: %p", e);
	memset(e, 0, sizeof(struct timer_event));
	//e->prev = NULL;
	//e->next = NULL;
	e->func = f;
	e->data = d;
	//e->status = 0;
	//e->timer = 0;
	return e;
}

struct timer_event *CC_add_timer_event( int msec, unsigned int status, callback f, void *d )
{
	struct timer_event *e = NULL;

	e = new_timer_event( f, d );
	if (e==NULL) return NULL;

	e->status = status | EVENT_F_ENABLED;
	e->timer = msec;
	e->next = timer_event_list;
	if( e->next ) e->next->prev = e;
#if defined(_OSLinux)
	gettimeofday(&(e->due_time), NULL); 
#else
	//GetSystemTime(&(e->due_time));
	_ftime( &(e->due_time) );	//Wen-Fang: 2009.3.12
#ifdef LOG
	printf("add timer event, due_time = %I64d", e->due_time.time);
#endif
#endif

	timer_event_list = e;
	timer_event_list_length++;
	schedule_timer_event( e, NULL );

	return e;
}

static void strip_timer_events( struct timer_event **list )
{
	struct timer_event *e, *n;
	e = n = NULL;

	for( e = *list; e; e = n )
	{
		n = e->next;
		if( e->status & EVENT_F_REMOVE )
		{
			if( e->next ) e->next->prev = e->prev;
			if( e->prev ) e->prev->next = e->next;
			else *list = e->next;
#ifdef LOG
			printf("remove event (%p) from event list, current timer event list length = %d", e, timer_event_list_length-1);
#endif
			free(e);
			e=NULL;
			timer_event_list_length--;

		}
	}
}

void CC_remove_timer_event( struct timer_event *e )
{
	if(&e->status)
	{
		//printf("set timer (%p) as remove status", e);
		e->status |= EVENT_F_REMOVE;
		e->status &= ~( EVENT_F_RUNNING | EVENT_F_ENABLED );
	}
	else
	{
		fprintf(stderr,"e->status is invalid\n");
	}
}


static void set_timer_event_enabled( struct timer_event *e, int enabled )
{
	e->status &= ~EVENT_F_ENABLED;
	if( enabled ) e->status |= EVENT_F_ENABLED;
}

static struct socket_event *new_socket_event( callback f, void *d )
{
	struct socket_event *e = NULL;

	e = (struct socket_event *)malloc( sizeof( struct socket_event ) );
	if(e == NULL) return NULL; // by hcc 10/14

	memset(e, 0, sizeof(struct socket_event));
	//e->prev = NULL;
	//e->next = NULL;
	e->func = f;
	e->data = d;
	//e->status = 0;
	//e->fd = 0;
	//e->write = 0;
	//e->isRTSPfd= 0;
	memset(e->debug_name, 0, sizeof(e->debug_name));
	return e;
}


struct socket_event *CC_add_socket_event( int fd, int write, unsigned int status, callback f, void *d )
{
	struct socket_event *e = NULL;
	e = new_socket_event( f, d );
	if(e == NULL) return NULL;

	socket_event_list_length++;

	//set it as enable default
	e->status = status | EVENT_F_ENABLED;
	e->fd = fd;
	e->write = write;
	if (write == 1)
		fprintf(stderr, "");
	e->next = socket_event_list;
	if( e->next ) e->next->prev = e;
	socket_event_list = e;
	return e;
}


static void strip_socket_events( struct socket_event **list )
{
	struct socket_event *e, *n;
	e = n = NULL;

	for( e = *list; e; e = n )
	{
		n = e->next;
		if( e->status & EVENT_F_REMOVE )
		{
			if( e->next ) e->next->prev = e->prev;
			if( e->prev ) e->prev->next = e->next;
			else *list = e->next;
#ifdef LOG
			printf("remove socket %d (%p) from event list, current socket event list length = %d", e->fd, e,socket_event_list_length-1);
#endif
			free(e);
			e=NULL;
			socket_event_list_length--;
		}
	}
}

void CC_remove_socket_event( struct socket_event *e )
{
	e->status |= EVENT_F_REMOVE;
	e->status &= ~( EVENT_F_RUNNING | EVENT_F_ENABLED );

	//printf("set socket %d(%p) as remove status", e->fd,e);
}


static void set_socket_event_enabled( struct socket_event *e, int enabled )
{
	e->status &= ~EVENT_F_ENABLED;
	if( enabled ) e->status |= EVENT_F_ENABLED;
}

#if defined(_OSLinux)
void *CC_event_loop( void *arg )
#endif // _OSLinux

#if defined(_OSWIN32)
unsigned __stdcall CC_event_loop( void *arg )
#endif // _OSWIN32
{
	struct socket_event *se;
	struct timer_event *te;
	int highfd, ret;
	fd_set rfds, wfds;
	struct timeval next_timer, *find_timer;
	int diff_msec, next_msec = 0;

	do {
//===timer event====	
		find_timer = NULL;
		next_msec = 0;
		for( te = timer_event_list; te; te = te->next )
			if( te->status & EVENT_F_ENABLED )
			{
				diff_msec = -time_ago( &te->due_time );

				if (diff_msec < -1000) //system time might be set to the future time, Wen-Fang: 2011.8.25
				{
#ifdef LOG
					printf("Strange timer, diff_msec = %d", diff_msec);
#endif
				}

				if( diff_msec < 5 ) diff_msec = 0;
				if( ! find_timer || diff_msec < next_msec ) next_msec = diff_msec;
				find_timer = &next_timer;
				te->status |= EVENT_F_RUNNING;
			} else te->status &= ~EVENT_F_RUNNING;

		if( find_timer )
		{
			find_timer->tv_sec = next_msec / 1000;
			find_timer->tv_usec = ( next_msec % 1000 ) * 1000;
		}

//=========================
		
		FD_ZERO( &rfds );
		FD_ZERO( &wfds );
		highfd = -1;
		
		for( se = socket_event_list; se; se = se->next )
		{
			if( se->status & EVENT_F_ENABLED )
			{
				FD_SET( (unsigned int)se->fd, se->write ? &wfds : &rfds );
				if( se->fd> highfd )
					highfd = se->fd;
				se->status |= EVENT_F_RUNNING;
			} else se->status &= ~EVENT_F_RUNNING;
		}

//		ret = select( highfd + 1, &rfds, &wfds, NULL, (struct timeval *) 0);
		ret = select( highfd + 1, &rfds, &wfds, NULL, find_timer);
		if (ret < 0)
		{
#ifdef LOG
			printf("socket select return error");
#endif
		}

//====handle timer event=======

		for( te = timer_event_list; te; te = te->next )
		{
			if( ! ( te->status & EVENT_F_RUNNING ) ) continue;
			if( end_loop ) break;
			diff_msec = -time_ago( &te->due_time);


			if (diff_msec > ((te->timer)*2) ) //system time might be set as earlier time, Wen-Fang: 2011.8.25
			{
#ifdef LOG
				char time_string[30];

				sprintf(time_string,"%s", ctime(&te->due_time.tv_sec));
				time_string[strlen(time_string)-1] = '\0';

				printf("System timer has been set to earlier time, diff_msec = %d, timer = %d (name: %s)", diff_msec, te->timer, te->debug_name);
				printf("Orignial Due time: %s", time_string);
#endif
				time_future(&(te->due_time), te->timer);
			}
			
			if (diff_msec < -(te->timer)) //system time might be set to the future time, Wen-Fang: 2011.8.25
			{
#ifdef LOG
				char time_string[30];

				sprintf(time_string,"%s", ctime(&te->due_time.tv_sec));
				time_string[strlen(time_string)-1] = '\0';

				printf("System timer has been set to future time, diff_msec = %d, timer = %d (name: %s)", diff_msec, te->timer, te->debug_name);
				printf("Orignial Due time: %s", time_string);
#endif
				time_future(&(te->due_time), te->timer);

				//diff_msec = -time_ago( &te->due_time); //skip timer_event this time;
			}

			if( diff_msec < 5 )
			{
				schedule_timer_event( te, NULL );
				if (te->func)
					(*te->func)( te, te->data );
			}
		}
//======================

		
		if( ret > 0 && !end_loop)
		{		
			for( se = socket_event_list; se; se = se->next )
			{
				if( ! ( se->status & EVENT_F_RUNNING ) ) continue;
				if( end_loop ) break;
				if( FD_ISSET( se->fd, se->write ? &wfds : &rfds ) )
				{

					if(!se->isRTSPfd){
#if defined(_OSLinux)
						int nread;
						ioctl(se->fd, FIONREAD, &nread);
						if(nread == 0) {
							CC_remove_socket_event(se);
							fprintf(stderr, "Event: removing client on fd %d\n", se->fd);
						}
#endif // _OSLinux
					}
					(*se->func)( se, se->data );
				}
			}
		}

		strip_timer_events( &timer_event_list );
		strip_socket_events( &socket_event_list );

		if (end_loop) {
#ifdef LOG
			printf("Exit Event Loop");
#endif
		}

	} while( !end_loop);


#if defined(_OSWIN32)
	_endthreadex(0);
#endif
	return 0;
}

