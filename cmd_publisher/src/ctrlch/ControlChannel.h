#ifndef ___CONTROL_CHANNEL___H___
#define ___CONTROL_CHANNEL___H___

#include <netinet/in.h>

//#ifdef __cplusplus
//extern "C" {
//#endif

#define MSG_BUFFER_SIZE 4096

#define ERR_INVALID_BUFFERDATA	-101
#define ERR_CTRLMSG_TOOLONG		-102
#define ERR_SEND_REPLYMSG		-103


struct control_conn
{
	struct control_conn *next;
	struct control_conn *prev;
	int fd;
	struct sockaddr_in client_addr;
	struct socket_event *read_event;
	//struct socket_event *write_event;
	unsigned char rcv_buf[MSG_BUFFER_SIZE];
	unsigned int rcv_len;
	unsigned int rcv_read_offset;
	unsigned char send_buf[MSG_BUFFER_SIZE];
	unsigned char msg_buffer[MSG_BUFFER_SIZE];
	unsigned char *msg_bufPtr;
	unsigned char *msg_bufEnd;
	int bIsCasterConnecting; //set as true, when do_caster is running, Wen-Fang: 2012.11.6

	//control parameters
	//int action;
	char action[32];
	char Account[128];
	char livename[128];
	char DestAddr[16];
	unsigned short DestPort;

	//attributes for Touch-to-Fly control commands
	//goDir, goROI, shiftMov, turnHeading
	//stop (no paramter)
	int LX, LY;		//for goROI
	int RX, RY;		//for ROI
	int DirX, DirY;	//for goDir, turnHeading
	double SpeedX, SpeedY;	//for shiftMov
	double ShiftMovDuration;	//for shiftMov
	int AgainstImageWidth;
	int AgainstImageHeight;

	//attributes for surrounding (including horizontal and vertical or mixed) cmds
	double Radius;
	double HDegree, VDegree;
	int    HDir;
	int	   AgainstROI;

	//attributes for C2V control commands
	double GimbalPan, GimbalTilt;
	int	   Zoom;

	//attributes for EZcmd
	char EZCmd[32];
	double EZ_axis_x, EZ_axis_y, EZ_axis_z;
	
	double EZ_speed_for_mov_to_pos;
	double EZ_duration_for_mov_by_vel;
	
	int EZ_ned, EZ_block;
};

typedef void* (*action_handler_func)(struct control_conn *s, void *para, int *ret);


struct ctrl_action_def{
	struct ctrl_action_def  *next; //Daisy
	char action_name[256];//Daisy
	//void (*action_handler)( struct control_conn *ctrl_conn, void *para, int *ret);
	action_handler_func action_handler;
	void *para;
};

int register_control_action(char *action_str, action_handler_func para_handler, void *para);

int control_channel_init(unsigned short port);
void controlchan_drop_conn( struct control_conn *conn );
void start_control_channel_loop();
void end_control_channel_loop();

//#ifdef __cplusplus
//}
//#endif

#endif       
