#ifndef CMD_PUBLISHER_H_
#define CMD_PUBLISHER_H_

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <camera_info_manager/camera_info_manager.h>    //joe added
#include "cmd_publisher/roi.h"
#include "cmd_publisher/poi.h"      //joe added
#include "cmd_publisher/abort.h"
#include "cmd_publisher/shiftMov.h"
#include "cmd_publisher/turnHeading.h"      
#include "cmd_publisher/surround.h"   
#include "cmd_publisher/mission.h"  
#include <boost/bind.hpp>
#include <sstream> 

 

extern "C" {
#include "../src/ctrlch/ControlChannel.h"
 
}

static unsigned short szCtrl_Port = 1021;

struct TouchInputCmd
{
    int  mode;     //currently, 0->No OP; 1->go ROI; 2->toward POI
    
    int  ROI_LT_X, ROI_LT_Y;    //for goROI
    int  ROI_W, ROI_H;          //for goROI
    
    int  Dir_X, Dir_Y;          //for goDir , turnHeading
    
    double Speed_X, Speed_Y;    ///for shift
    double ShiftMovDuration;            //for shift
    int  Image_W, Image_H;
};

namespace Roi_Info{

class CmdReceiver
{ 

public:
    
    //define some constant
    const static int CMD_TO_PUBLISH_NONE=0;
    const static int CMD_TO_PUBLISH_GOROI=1;
    const static int CMD_TO_PUBLISH_GODIR=2;
    const static int CMD_TO_PUBLISH_SHIFTMOVE=3;
    const static int CMD_TO_PUBLISH_TURNHEADING=4;
    const static int CMD_TO_PUBLISH_ABORT=5;

    CmdReceiver();
    ~CmdReceiver();

    //callbacks for corresponding HTTP commands
    static void* handle_goROI(struct control_conn *s, void *para, int *ret);            //goROI
    static void* handle_goDir(struct control_conn *s, void *para, int *ret);            //goDIR
    static void* handle_towardPOI(struct control_conn *s, void *para, int *ret);        //towardPOI, should be replaced by goDir in the future
    static void* handle_turnHeading(struct control_conn *s, void *para, int *ret);      //turnHeading
    static void* handle_shiftMov(struct control_conn *s, void *para, int *ret);         //shiftMov
    static void* handle_stop(struct control_conn *s, void *para, int *ret);             //Abort
    static void* handle_hSurrounding(struct control_conn *s, void *para, int *ret);    //hSurrounding
    static void* handle_EZCmd(struct control_conn *s, void *para, int *ret);            //direct command input

    void publish_goROI_Msg();
    void set_goROI_Msg( int roi_x, int roi_y,   /*Top-left point (x,y)*/
                        int roi_w, int roi_h,   /*width and height of the ROI*/
                        int image_w, int image_h   /*image width and height which above coordiantes reference to*/ );

    void publish_goDir_Msg();
    void set_goDir_Msg(int x, int y,        /*point (x,y)*/
                        int image_w, int image_h    /*image width and height which above coordiantes reference to*/ );

    void publish_shiftMov_Msg();
    void set_shiftMov_Msg(double dSpeedY, double dSpeedZ, /*speed in horizontal and vertical */
                              double dDuration                /*duration for the movement, in sec*/);

    void publish_turnHeading_Msg();
    void set_turnHeading_Msg(int disp_horizontal,     /*the horizontal movement in px*/
                                 int image_w                /*image width which above coordinates reference to*/);

    void publish_abort_Msg();
    void set_abort_Msg();




    void publish_surround_Msg();
    void set_surround_Msg( unsigned int hDir, 
                            double hDegree, 
                            double radius,
                            unsigned int AgainstROI);





    void publishPOI();
    
    
    
    int getMode(){return m_CmdToPublish;};
    int getImage_W(){return Image_W;};
    int getImage_H(){return Image_H;};
#if 0
    bool isupdateROI(){return updateROI;};
#endif

    //methods for EZCmd
    void publishMission(); 
    void setMission(std::string mission);
    void setMission(std::string mission,bool wait_done);
    void setMission(std::string mission,float z,bool wait_done); 
    void setMission(std::string mission,float z,bool use_ned,bool wait_done); 
    void setMission(std::string mission,float x,float y,float z,float velocity,float duration,bool use_ned,bool wait_done);
    void clearMission();


    //callback method for subscribed camera_info topic
#if 0
    void camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg, void *pEnvObj);
#else
    void camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg);
#endif

 
private:
   


    int m_CmdToPublish;             

#if 0
    int mode;
    bool updateROI;

    int roi_x,roi_y;
    int poi_x,poi_y;
    int roi_width, roi_height; 
#endif
    int Image_W, Image_H;

    ros::NodeHandle n;
    ros::NodeHandle private_node_handle;
    cmd_publisher::roi goROImsg;
    cmd_publisher::poi goDirmsg;  
    cmd_publisher::abort goAbortmsg;
    cmd_publisher::shiftMov shiftMovmsg;
    cmd_publisher::turnHeading turnHeadingmsg;
    cmd_publisher::surround surroundmsg;


    //topic subscriber and publisher
    ros::Subscriber camera_info_sub; 
    ros::Publisher roi_pub;  
    ros::Publisher poi_pub;
    ros::Publisher abort_pub;
    ros::Publisher turnHeading_pub;
    ros::Publisher shiftMov_pub;
    ros::Publisher surround_pub;
    ros::Publisher mission_pub;  
    cmd_publisher::mission missionmsg;
  //  bool isupdateMission;
   

#if 0
    class InputCmd
    {
public:
        int  m_CmdType;             //goROI:1, goDir:2
                                    //turnHeading: 4, shiftMov: 5, stop:8
        int  ROI_LT_X, ROI_LT_Y;    //for goROI
        int  ROI_W, ROI_H;          //for goROI
        
        int  Dir_X, Dir_Y;          //for goDir , turnHeading
        
        double Speed_X, Speed_Y;    ///for shift
        double ShiftMovDuration;            //for shift        

        InputCmd() {
            m_CmdType=0;
            ROI_LT_X = ROI_LT_Y =0; 
            ROI_W = ROI_H = 0 ;
            Dir_X = Dir_Y = 0;
            Speed_X = Speed_Y = 0;
            ShiftMovDuration = 0;
        }
    };  
    InputCmd    m_LastCmd;  
#endif

};



}

#endif