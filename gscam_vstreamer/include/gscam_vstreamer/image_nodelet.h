#ifndef __GSCAM_VSTREAMER_IMAGE_NODELET_H
#define __GSCAM_VSTREAMER_IMAGE_NODELET_H

#include <nodelet/nodelet.h>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

namespace gscam_vstreamer_nodelet {
  class ImageNodelet : public nodelet::Nodelet
  {
  public:
    ImageNodelet();
    ~ImageNodelet();

    virtual void onInit();
    void  do_image();
    static void  imageCallback(const boost::shared_ptr<gscam_vstreamer::topic> &msg);
  private:
    boost::scoped_ptr<boost::thread> image_thread;
  };
}

#endif // infdef __GSCAM_VSTREAMER_IMAGE_NODELET_H
