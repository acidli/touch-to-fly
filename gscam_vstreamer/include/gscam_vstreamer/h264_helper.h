

#define H264_NALU_TYPE_UNKNOWN	0
#define	H264_NALU_TYPE_nIDR		1
#define	H264_NALU_TYPE_IDR		5
#define	H264_NALU_TYPE_SEI		6
#define	H264_NALU_TYPE_SPS		7	//sequence parameter set
#define	H264_NALU_TYPE_PPS		8	//picture parameter set
#define	H264_NALU_DELIMITER		9	


int	H264_GetNALUType(const unsigned char* pNALHeader);

int Should_Skip(int nalu_type);