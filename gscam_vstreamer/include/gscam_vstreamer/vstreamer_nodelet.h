#ifndef __GSCAM_VSTREAMER_VSTREAMER_NODELET_H
#define __GSCAM_VSTREAMER_VSTREAMER_NODELET_H

#include <nodelet/nodelet.h>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

namespace gscam_vstreamer_nodelet {
  class VstreamerNodelet : public nodelet::Nodelet
  {
  public:
    VstreamerNodelet();
    ~VstreamerNodelet();

    virtual void onInit();
  private:
    boost::scoped_ptr<boost::thread> vstreamer_thread;
  };
}

#endif // infdef __GSCAM_VSTREAMER_VSTREAMER_NODELET_H
