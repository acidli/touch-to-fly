
#ifndef VSTREAMERAPI_H
#define VSTREAMERAPI_H

#ifdef __cplusplus
extern "C"
{
#endif


typedef enum _VSTREAMER_API_RESULT_CODE
{
	VSAPI_RESULT_OK											= 1,
	VSAPI_RESULT_PARAMETER_CONFIGURE_PATH_IS_NULL			= 2,
	VSAPI_RESULT_PARAMETER_CONFIGURE_LENGTH_IS_INVALID		= 3,
	VSAPI_RESULT_PARAMETER_CONFIGURE_BUFFER_IS_NULL			= 4,
	VSAPI_RESULT_PARAMETER_LENGTH_IS_INVALID				= 5,
	VSAPI_RESULT_PARAMETER_PAYLOAD_IS_NULL					= 6,
	VSAPI_RESULT_PARAMETER_TIMESTAMP_IS_LESS_ZERO			= 7,
	VSAPI_RESULT_PARAMETER_LAST_SLICE_IS_LESS_ZERO			= 8,
	VSAPI_RESULT_PARSER_CONFIGURE_FILE_ERROR				= 9,
	VSAPI_RESULT_CREATE_EVENT_THREAD_FAIL					= 10,
	VSAPI_RESULT_CALLNUM_ERROR								= 11,
	VSAPI_RESULT_GET_FREE_BUFFER_IS_NULL					= 12,
	VSAPI_RESULT_SKIP_P_FRAME								= 13,
	VSAPI_RESULT_PARAMETER_SERIAL_NUMBER_IS_NULL			= 14,
	VSAPI_RESULT_VIDEO_FRAME_HAS_NOT_START_CODE				= 15,
	VSAPI_RESULT_PARAMETER_INIT_STRUCT_IS_NULL				= 16,
	VSAPI_RESULT_SET_CONFIGURE_FILE_ERROR					= 17,
	VSAPI_RESULT_CALLBACK_IS_NULL							= 18,
	VSAPI_RESULT_PARAMETER_INDEX_IS_INVALID					= 19
}VSTREAMER_API_RESULT_CODE;

typedef enum _COMMAND_TYPE
{
	COMMAND_TYPE_VIDEO_QOS									= 1,
}COMMAND_TYPE;


typedef int (*UpperCallBack)(COMMAND_TYPE cmd, int param1, int param2, void *arg, int param3); 

int Init(char *confPath, char *serialNo, UpperCallBack fnCallBack, void *arg);

// video
int SetVideoInfo_Ext(int width, int height, int frameRate, unsigned char callnum, void *param);
int SendH264_Ext(int payloadLen, unsigned char *payload, unsigned char callnum, void *param); 




#ifdef __cplusplus
}
#endif
#endif
