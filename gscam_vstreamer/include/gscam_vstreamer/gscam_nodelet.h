#ifndef __GSCAM_GSCAM_NODELET_H
#define __GSCAM_GSCAM_NODELET_H

#include <nodelet/nodelet.h>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

namespace gscam_vstreamer_nodelet {
  class GSCamNodelet : public nodelet::Nodelet
  {
  public:
    GSCamNodelet();
    ~GSCamNodelet();

    virtual void onInit();
  private:
    boost::scoped_ptr<boost::thread> gscam_thread;
  };

  class TopicCamNodelet : public nodelet::Nodelet
  {
  public:
    TopicCamNodelet();
    ~TopicCamNodelet();

    virtual void onInit();
  private:
    boost::scoped_ptr<boost::thread> topiccam_thread;
  };
}

#endif // infdef __GSCAM_GSCAM_NODELET_H
