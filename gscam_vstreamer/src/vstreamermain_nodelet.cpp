#include "gscam_vstreamer/nxjson.h"
#include "gscam_vstreamer/aes.h"  
#include "gscam_vstreamer/base64.h"
#include "gscam_vstreamer/h264_helper.h"  
#include "gscam_vstreamer/vstreamerapi.h"
#include "gscam_vstreamer/vstreamer_nodelet.h"
#include <gst/gst.h>  
#include <string.h>  
#include <stdlib.h>
#include <stdio.h>
#include <gst/app/gstappsink.h>  
#include <pthread.h>
#include <sys/ipc.h>
#include "ros/ros.h"
#include "gscam_vstreamer/topic.h"
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <boost/thread.hpp>
#include <ros/package.h>
#define DEFAULT_1080P_BITRATE 2000000
#define DEFAULT_720P_BITRATE 1000000
#define VSRC_WIDTH  "1280"
#define VSRC_HEIGHT "720"
#define VSRC_FRAMERATE "30"
#define PI3_SERIAL_NO_LENGTH 16
#define SPS_PPS_SIZE   64
#define MAX_IMAGE_DIR_PATH_LENGTH 64
#define MAX_IMAGE_FILENAME_LENGHT  64
#define JSONBUFSIZ 512
#define SNAPSHOT_KEYID 2001
#define GST_VSTREAMER_CONFIG_FILE  "gst_vstreamer.conf"
#define GST_PIPELINE_STRING_BUF_LENGTH   1024

#define SUBSCRIBE_QUEUE_SIZE 10

#define TX2_SERIAL_NO   "TX2TTF_001"

PLUGINLIB_EXPORT_CLASS(gscam_vstreamer_nodelet::VstreamerNodelet, nodelet::Nodelet) 

#define H264_MAX_SIZE 256*1024
#define H264_MAP_MAX_SIZE 64*1024

// IPC snapshot image parameters
typedef struct msg_st{
	long int msg_type;
	char buf[JSONBUFSIZ];
} JSONMESSAGE;

typedef struct  
{  
    GMainLoop 		*loop;  
    GstElement 		*src_pipeline; 
    char 		spspps_buf[SPS_PPS_SIZE];
    int 		spspps_size;
    char 		imageDir[MAX_IMAGE_DIR_PATH_LENGTH];
    char 		imageFileName[MAX_IMAGE_FILENAME_LENGHT];
    char 		serialNo[PI3_SERIAL_NO_LENGTH + 4];
    pthread_t  		snapshot_ipc_thread;
    pthread_mutex_t 	mutex;
    int 		snapshot_flag;
    int			vstreamer_init_done;
    int 		video_width;
    int 		video_height;
    int			video_framerate;
    unsigned char 	*h264_ptr; // for SendH264_Ext() length >= 64KB temporary buffer point
    int                 h264_len;
} ProgramData;  

//GstElement *gst_pipeline=NULL; 
ProgramData *data = NULL;  
std::string package_path;
  
static int Upper_CallBack_Handler(COMMAND_TYPE cmd, int param1, int param2, void *arg, int param3)
{
    if(cmd == COMMAND_TYPE_VIDEO_QOS)
    {
        printf("Video QOS: ");
        printf("framerate=%d ", param1);
        printf("bitrate=%d\n", param2);
    }
    else
        printf("Command is Invalid\n");
    return 0;
}

unsigned int hcc_stamp=0;
unsigned int h264bs_cb_seq=0;

//void h264bsCallback(const gscam_vstreamer::topic::ConstPtr &msg)
void h264bsCallback(const boost::shared_ptr<gscam_vstreamer::topic> &msg)
{
  //ROS_INFO("vstreamer: receive size=%d stamp=%lu seq=%d", msg->size, msg->stamp, msg->seq);

  //printf("h264bsCallback: msg=%x seq=%d\n", msg, msg->seq);

  if(h264bs_cb_seq == 0)
     h264bs_cb_seq = msg->seq;

  if(msg->seq != (h264bs_cb_seq+1) && h264bs_cb_seq != 0)
     ROS_WARN("h264bs_seq: out of sequence (%d:%d)", h264bs_cb_seq+1, msg->seq);

  h264bs_cb_seq = msg->seq;

  if(msg->size == 27)
     ROS_DEBUG("h264bsCallback: is size 27");

  if(hcc_stamp == 0)
     hcc_stamp = msg->stamp;

  if(hcc_stamp == msg->stamp)
  {
     ROS_WARN("h264bsCallback: is the same stamp size=%d", msg->size);
  }

  hcc_stamp = msg->stamp;

  if (!data->vstreamer_init_done) {
       // -- call vstreamer ---
       long lRet;
       char path_str[256]="";

       sprintf(path_str, "%s", package_path.c_str());

       printf("VStreamer.conf path_str=%s\n", path_str);

//       lRet = Init((char *)"./", data->serialNo, Upper_CallBack_Handler, NULL);
       lRet = Init(path_str, data->serialNo, Upper_CallBack_Handler, NULL);
       if (lRet != VSAPI_RESULT_OK) {
      		printf("[VStreamer MSG]: Failed to init vstreamer (%d) !!!\n", lRet);
    		return; 
       }
 
       //lRet = SetVideoInfo_Ext(atoi(VSRC_WIDTH), atoi(VSRC_HEIGHT), atoi(VSRC_FRAMERATE), 213, NULL);
       lRet = SetVideoInfo_Ext(data->video_width, data->video_height, data->video_framerate, 213, NULL);
       if (lRet != VSAPI_RESULT_OK) {
         	printf("[VStreamer MSG]: Failed to startup vstreamer (%d) !!!\n", lRet);
    		return;
       }
       data->vstreamer_init_done = 1;
  } // if( !data->vstreamer_init_done )

  // call SendH264_Ext()
  /*
  unsigned char *ptr = (unsigned char*)&msg->buffer;
  printf("sendh264, size=%d, [%02x%02x%02x%02x %02x%02x%02x%02x]\n", msg->size, 
        *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5) ,*(ptr+6) ,*(ptr+7) );
  */
  SendH264_Ext(msg->size, (unsigned char*)&msg->buffer, 213, (void *)msg->stamp);

}

/*
static void handle_sigterm(int sig)
{
    g_print("[handle_sigterm] exit program (%d) !\n", sig);
    if(gst_pipeline != NULL) {
	    gst_element_set_state(gst_pipeline, GST_STATE_NULL);
	    gst_object_unref (gst_pipeline);
    }
    exit( 1 );
}
*/

static void writeLog(char * stringToWrite)
{
    FILE *f;
    if( stringToWrite != NULL) {
        f = fopen("./gst_vstreamer.log", "w");
    	fwrite(stringToWrite, sizeof(char), strlen(stringToWrite), f);
    	fflush(f);
    }
    else {
        f = fopen("./gst_vstreamer.log", "w+");
    }
    fclose(f);
}

static void LoopForever()
{
    while(1)
       sleep(10);
}

//int main(int argc, char **argv)
void do_vstreamer()
{
#define TEMP_BUFFER_SIZE 64
//   ProgramData *data = NULL;  
    gchar *string = NULL;  
    GstBus *bus = NULL;  
    GstElement *h264enc_sink = NULL;  
    GstElement *jpegenc_sink = NULL;  
    char temp[TEMP_BUFFER_SIZE]={0};
    gchar *vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=1280, height=720, framerate=10/1");
    gchar *h264_profile = (gchar *)"baseline";
    gchar *h264_bitrate_control = (gchar *)"default";
    int h264_bitrate = DEFAULT_720P_BITRATE; // 1.2 Mb

    printf("\n\n******** do_vstreamer\n\n\n");
    ros::NodeHandle n;

    package_path = ros::package::getPath("gscam_vstreamer");
    
    data = g_new0 (ProgramData, 1); 
    ros::Subscriber sub = n.subscribe("h264bs", SUBSCRIBE_QUEUE_SIZE, h264bsCallback, ros::TransportHints().tcpNoDelay(true));

    data->h264_ptr = (unsigned char *)malloc(H264_MAX_SIZE); // MAX_SIZE=256KB for SendH264_Ext()
    data->h264_len = 0;
    memset(data->serialNo, 0x00, PI3_SERIAL_NO_LENGTH+4);

    {
        FILE *fcpuinfo = fopen("/proc/cpuinfo", "r");
        char cpuinfo[4096] = {0};
        size_t size = 0;
        char *str = NULL;
        
		if( fcpuinfo == NULL ) {
	   		//printf("Fail to open /proc/cpuinfo !\n");
	   		sprintf(temp, "Fail to open sn. !\n");
       		writeLog(temp);
       		LoopForever();
		}

		size = fread(cpuinfo, 1, 4096, fcpuinfo);
		if( size <= 0 ) {
		    fclose( fcpuinfo );
			//printf("Fail to read /proc/cpuinfo !\n");
	   		sprintf(temp, "Fail to read sn. !\n");
       		writeLog(temp);
       		LoopForever();
		}
        
        fclose( fcpuinfo );
                
        //printf("\n\ncpu info : %s \n", cpuinfo);
        
        str = strstr( cpuinfo, "Serial" );
        if( str != NULL ){
            str = strstr( str, ":" );
            if( str != NULL ){
                str += 2;
                size = strlen(str);
            	printf("str : %s ; strlen(str) %d\n", str, size);
            	if( size > PI3_SERIAL_NO_LENGTH) {
            	    size = PI3_SERIAL_NO_LENGTH;
				}
	        	memcpy(data->serialNo, str, size);
			data->serialNo[16] = '\0';
        	}
        }
        else {
            strcpy (data->serialNo, TX2_SERIAL_NO);
        }
        //printf("SN : %s", data->serialNo);
     
    }

#if 0    
    //  ================================
    //  Validate Pi3 serial no list
    //  ================================
    {
    #define SUPPORT_LIST_FILE "support_list.txt"
    char found = 0;
    struct AES_ctx ctx;
    //_KEY = b'\x0e\x20\x00\x0e\x20\x00\x12\x34\x0e\x20\x00\x0e\x20\x00\xab\xcd'
    uint8_t key[] = { 0x0e, 0x20, 0x00, 0x0e, 0x20, 0x00, 0x12, 0x34, 0x0e, 0x20, 0x00, 0x0e, 0x20, 0x00, 0xab, 0xcd };
    //_IV = b'\x05\x06\x07\x08\x01\x02\x03\x04\x00\x00\x0a\x0b\x0c\x0d\x0e\x0f'
    uint8_t iv[]  = { 0x05, 0x06, 0x07, 0x08, 0x01, 0x02, 0x03, 0x04, 0x00, 0x00, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
    size_t base64EncodeDataSize = b64_get_encoded_buffer_size(PI3_SERIAL_NO_LENGTH);
    int i;
    uint8_t dataBuffer[PI3_SERIAL_NO_LENGTH+1] = {0};
    uint8_t *base64EncodeDataBuffer = malloc(base64EncodeDataSize);
    size_t readSize = 0;
    FILE *f;
    if( base64EncodeDataBuffer == NULL ) {
         printf("Fail to allocate buffer for base64EncodeData !\n");
         writeLog("Fail to allocate buffer for base64EncodeData !\n");
         LoopForever();
    }
    
	f = fopen(SUPPORT_LIST_FILE, "r");
	if( f == NULL ) {
	   printf("Fail to open %s !\n", SUPPORT_LIST_FILE);
	   sprintf(temp, "Fail to open %s !\n", SUPPORT_LIST_FILE);
       writeLog(temp);
       LoopForever();
	}
	
	while( 1 ) {
	     readSize = fread( (char*)base64EncodeDataBuffer, sizeof(uint8_t), (base64EncodeDataSize + 2), f ); // read base64 encode size + '\r\n'; 24+2 = 26 bytes
printf("readSize=%d\n");
	     if( feof(f) ) {
	         break;
	     }
	     b64_decode(base64EncodeDataBuffer, base64EncodeDataSize, dataBuffer); //base64 encode size = 24
	     
	     printf("Base64 Decode : ");
         for( i = 0; i < PI3_SERIAL_NO_LENGTH; i++){
              printf("%x ", dataBuffer[i]);
         }
         printf("\n");
         
         AES_init_ctx_iv(&ctx, key, iv);
         AES_CBC_decrypt_buffer(&ctx, dataBuffer, PI3_SERIAL_NO_LENGTH);
         
         printf("Decrypt String: %s; serial No : %s\n", dataBuffer, data->serialNo);
         
         if( !strcmp( data->serialNo, dataBuffer ) ){
              printf("Found serial no. (%s) in support list !\n", dataBuffer);
         	  found = 1;
         	  break;
         }
	}
    /*// test 
    // 3ve3YtMn9xzqpySjrLJ8wQ==
    base64EncodeDataBuffer[0] = '3';
    base64EncodeDataBuffer[1] = 'v';
    base64EncodeDataBuffer[2] = 'e';
    base64EncodeDataBuffer[3] = '3';
    base64EncodeDataBuffer[4] = 'Y';
    base64EncodeDataBuffer[5] = 't';
    base64EncodeDataBuffer[6] = 'M';
    base64EncodeDataBuffer[7] = 'n';
    base64EncodeDataBuffer[8] = '9';
    base64EncodeDataBuffer[9] = 'x';
    base64EncodeDataBuffer[10] = 'z';
    base64EncodeDataBuffer[11] = 'q';
    base64EncodeDataBuffer[12] = 'p';
    base64EncodeDataBuffer[13] = 'y';
    base64EncodeDataBuffer[14] = 'S';
    base64EncodeDataBuffer[15] = 'j';
    base64EncodeDataBuffer[16] = 'r';
    base64EncodeDataBuffer[17] = 'L';
    base64EncodeDataBuffer[18] = 'J';
    base64EncodeDataBuffer[19] = '8';
    base64EncodeDataBuffer[20] = 'w';
    base64EncodeDataBuffer[21] = 'Q';
    base64EncodeDataBuffer[22] = '=';
    base64EncodeDataBuffer[23] = '=';
    
    b64_decode(base64EncodeDataBuffer, base64EncodeDataSize, dataBuffer);//base64 encode size = 24
    
    printf("Base64 Decode : ");
    for(i = 0; i < PI3_SERIAL_NO_LENGTH; i++){
        printf("%x ", dataBuffer[i]);
    }

    printf("\n");
    AES_init_ctx_iv(&ctx, key, iv);
    AES_CBC_decrypt_buffer(&ctx, dataBuffer, PI3_SERIAL_NO_LENGTH);
    
    printf("Decrypt String: %s\n", dataBuffer); */
              
    free( base64EncodeDataBuffer );
    fclose(f);
    
    if( !found ) {
       sprintf(temp, "Serial no. (%s) not support !\n", data->serialNo);
       writeLog(temp);
       LoopForever();
    }

    sprintf(temp, "Serial no. (%s) support !\n", data->serialNo);
    writeLog(temp);
    
    }
#endif      
    printf("Serial no. (%s) support !\n", data->serialNo);

    g_print("setup signal !\n");
  
/*  
    signal( SIGPIPE, SIG_IGN );
    signal( SIGTERM, handle_sigterm );
    signal( SIGINT, handle_sigterm );
    signal( SIGUSR1,handle_sigterm );
    signal( SIGALRM, SIG_IGN);
*/
   
    pthread_mutex_init(&(data->mutex), NULL);

    data->video_width = atoi(VSRC_WIDTH);
    data->video_height = atoi(VSRC_HEIGHT);
    data->video_framerate = atoi(VSRC_FRAMERATE);
#if 1   
    // read gst_vstreamer.conf (JSON)
    {
    	FILE *f = fopen(GST_VSTREAMER_CONFIG_FILE, "r");
		if( f == NULL ) {
	   		printf("vstreamer Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
	   		sprintf(temp, "Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
       		writeLog(temp);
		}
		else {
			uint8_t json_buffer[256] = {0};
			size_t readSize = fread( (char*)json_buffer, sizeof(uint8_t), sizeof(json_buffer), f );
			fclose(f);
			printf("gst_vstreamer.conf (Size:%d)\n%s \n", readSize, json_buffer);
			const nx_json* json=nx_json_parse((char *)json_buffer, 0);
    		if (json) {
    	 		//printf("Version=%s\n", nx_json_get(json, "Version")->text_value);
         		//printf("SnapShot_Image_Directory=%s\n", nx_json_get(json, "SnapShot_Image_Directory")->text_value);
         		sprintf(data->imageDir,"%s", nx_json_get(json, "SnapShotImageDirectory")->text_value);
         		printf("Snapshot Image Directory: %s\n", data->imageDir);   
#if 0
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "VideoResolution")->text_value);
         		printf("Video Resolution: %s\n", temp);
         		if(strcmp(temp,"1080p") == 0){
         		   data->video_width = 1920;
    			   data->video_height = 1080;
         		}
         		data->video_framerate = nx_json_get(json, "VideoFrameRate")->int_value;
         		if((data->video_framerate < 1) ||  (data->video_framerate > 30)){
         			printf("INVALID Video Frame Rate: %d\n", data->video_framerate);
         			data->video_framerate = atoi(VSRC_FRAMERATE);
         		}
         		else {
 	         		printf("Video Frame Rate: %d\n", data->video_framerate);
 	         	}
         		vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=%d, height=%d, framerate=%d/1", data->video_width, data->video_height, data->video_framerate);
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264Profile")->text_value);
         		printf("H264Profile: %s\n", temp);
         		if((strcmp(temp,"main") == 0) || (strcmp(temp,"high") == 0)){
         		   h264_profile = g_strdup_printf("%s", temp);
         		}    
         		h264_bitrate = nx_json_get(json, "H264BitRate")->int_value;  
         		if((h264_bitrate < 10) || (h264_bitrate > 10000000)) {
         			printf("INVALID H.264 bitrate: %d\n", h264_bitrate);
         			if(data->video_width == 1920) {
	         			h264_bitrate = DEFAULT_1080P_BITRATE;
	         		}
         			else {
	         			h264_bitrate = DEFAULT_720P_BITRATE;
	         		}
         		}
         		else {
	         		printf("H.264 bitrate: %d\n", h264_bitrate);	
	         	}
	         	memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264BitRateControl")->text_value);
         		printf("H264BitRateControl: %s\n", temp);
         		if((strcmp(temp,"variable") == 0) || (strcmp(temp,"constant") == 0) || (strcmp(temp,"constant-skip-frames") == 0)\
         		 || (strcmp(temp,"variable-skip-frames") == 0) || (strcmp(temp,"default") == 0) || (strcmp(temp,"disable") == 0)){
         			h264_bitrate_control = g_strdup_printf("%s", temp);
         		}
#endif
         		nx_json_free(json);
    		} // if(json)
    	} // if(f)
    }
#endif    
        
  //ros::spin();

  ros::Rate r(100); // 10 hz
  while (1)
  {
     ros::spinOnce();
     //r.sleep();
     sleep(1);
  }

}




void set_logger_level(int level)
{


    enum{Debug=0,Info=1,Warn=2,Error=3,Fatal=4,None=5};

    switch(level)
    {
        case  Debug: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Info: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Warn: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Warn)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Error: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Error)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
      //  case  None: 
     //       if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::None)) 
    //            ros::console::notifyLoggerLevelsChanged(); 
     //   break;

        default:
 
        break;

    }
   
    
}


namespace gscam_vstreamer_nodelet {
  VstreamerNodelet::VstreamerNodelet() :
    nodelet::Nodelet()
  {
	ROS_INFO("vstreamer - Constructor");
  }

  VstreamerNodelet::~VstreamerNodelet() 
  {
	ROS_INFO("vstreamer - Discnstructor");
    vstreamer_thread->join();
  }

  void VstreamerNodelet::onInit()
  {

    set_logger_level(3);
    ROS_INFO("vstreamer - onInit");
    vstreamer_thread.reset(new boost::thread(boost::bind(&do_vstreamer)));

  }
}

