#include "gscam_vstreamer/h264_helper.h"
#include <stdlib.h>


int	H264_GetNALUType(const unsigned char* pNALHeader)
{
	if ( (*pNALHeader ) & 0x80 ) {
		return H264_NALU_TYPE_UNKNOWN;
	}
	return (*pNALHeader) & 0x1f;
}

int Should_Skip(int nalu_type)
{
	if( nalu_type != H264_NALU_TYPE_nIDR &&
		nalu_type != H264_NALU_TYPE_IDR	&& 
		nalu_type != H264_NALU_TYPE_SPS	&&
		nalu_type != H264_NALU_TYPE_PPS	) 
	{
		return 1;		//should skip
	}

	return 0;
}