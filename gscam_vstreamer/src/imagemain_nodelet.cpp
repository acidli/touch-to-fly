#include "ros/ros.h"
#include "gscam_vstreamer/topic.h"
#include "gscam_vstreamer/videoconvert.h"
#include "gscam_vstreamer/image_nodelet.h"
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <boost/thread.hpp>
#include <sensor_msgs/Image.h>

#define VIDEO_WIDTH 1280
#define VIDEO_HEIGHT 720
#define RGB_SIZE VIDEO_WIDTH*VIDEO_HEIGHT*3
#define SUBSCRIBE_QUEUE_SIZE 10
#define PUBLISH_QUEUE_SIZE 10

 
unsigned int image_cb_seq=0;
ros::Publisher image_rgb_pub;

PLUGINLIB_EXPORT_CLASS(gscam_vstreamer_nodelet::ImageNodelet, nodelet::Nodelet) 

/* sensor_msgs/Image.msg
header
uint32 height 		# number of rows
uint32 width 		# number of columns
string encoding		# Encoding of pixels -- channel meaning, ordering, size
uint8 is_bigendian	# is this data bigendian?
uint32 step 		# Full row length in bytes
uint8[] data		# actual matrix data, size is (step * rows)
*/

std::string RGB8 = "rgb8";

unsigned int pre_time2=0;
int hcc_cnt2=0;
//void imageCallback(const gscam_vstreamer::topic::ConstPtr &msg)

//int main(int argc, char **argv)


namespace gscam_vstreamer_nodelet {
  ImageNodelet::ImageNodelet() :
    nodelet::Nodelet()
  {
    ROS_INFO("image - Constructor");
  }

  ImageNodelet::~ImageNodelet() 
  {
    ROS_INFO("image - Discnstructor");
    image_thread->join();
  }

  void ImageNodelet::onInit()
  {
    image_thread.reset(new boost::thread(boost::bind(&gscam_vstreamer_nodelet::ImageNodelet::do_image,this)));
   
  }

  void ImageNodelet::do_image()
  {
  printf("\n\n***** do_image\n\n");

    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe("image", SUBSCRIBE_QUEUE_SIZE, imageCallback);

    image_rgb_pub = n.advertise<sensor_msgs::Image>("image_rgb", 10);

    ros::Rate r(100); // 10 hz
    while (1)
    {
      ros::spinOnce();
      r.sleep();
    }

  }



  void  ImageNodelet::imageCallback(const boost::shared_ptr<gscam_vstreamer::topic> &msg)
  {
    FILE *f;
    char f_buf[100] = {0};
    char f1_buf[100] = {0};
    struct timespec      tm, now;
    unsigned int        curr_time=0;
    sensor_msgs::Image rgb_msg;

    //ROS_INFO("image: receive image size=%d stamp=%x seq=%d", msg->size, msg->stamp, msg->seq);

    clock_gettime(CLOCK_MONOTONIC, &tm);
          curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
          hcc_cnt2++;

          if(pre_time2 == 0)
        pre_time2 = curr_time ;

    if( curr_time - pre_time2 > 1000 )
    {
        ROS_DEBUG("hcc_cnt2=%d", hcc_cnt2);
        hcc_cnt2 = 0;
        pre_time2 = curr_time ;
    }

    if(image_cb_seq == 0)
      image_cb_seq = msg->seq;

    //if(msg->seq != (image_cb_seq+1) && image_cb_seq != 0)
      //printf("image_seq: out of sequence (%d:%d)\n", image_cb_seq+1, msg->seq);

    image_cb_seq = msg->seq;

  //  yuv420p_to_rgb24((unsigned char *)&msg->buffer, (unsigned char *)&rgb_buffer, VIDEO_WIDTH, VIDEO_HEIGHT);

    rgb_msg.data.resize(RGB_SIZE);

    yuv420p_to_rgb24((unsigned char *)&msg->buffer, (unsigned char *)rgb_msg.data.data(), VIDEO_WIDTH, VIDEO_HEIGHT);

    rgb_msg.header.stamp.fromSec(ros::WallTime::now().toSec());
    rgb_msg.height = VIDEO_HEIGHT;
    rgb_msg.width = VIDEO_WIDTH;
    rgb_msg.encoding = RGB8;
    rgb_msg.is_bigendian = true;
    rgb_msg.step = VIDEO_WIDTH * 3;

    image_rgb_pub.publish(rgb_msg);

  #if 0
    sprintf(f_buf,"/tmp/image%d.rgb", image_cnt);
    sprintf(f1_buf,"/tmp/image%d.yuv", image_cnt);
    
    f = fopen(f_buf, "wb");
    if( f != NULL) {
      fwrite(&rgb_buffer, 1, RGB_SIZE, f);
          fclose(f);
    } // if(f)

    f = fopen(f1_buf, "wb");
    if( f != NULL) {
      fwrite(&msg->buffer, 1, msg->size, f);
          fclose(f);
    } // if(f)
    
    image_cnt++;
  #endif

  }

}

