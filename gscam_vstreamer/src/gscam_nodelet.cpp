/* GStreamer 
 * 
 * appsink-src.c: example for using appsink and appsrc. 
 * 
 * Copyright (C) 2008 Wim Taymans <wim.taymans@gmail.com> 
 * 
 * This library is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version. 
 * 
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Library General Public License for more details. 
 * 
 * You should have received a copy of the GNU Library General Public 
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA 02110-1301, USA. 
 */  

#include <gst/gst.h>  
#include <string.h>  
#include <stdlib.h>
#include <stdio.h>
#include <gst/app/gstappsink.h>  
#include <gst/app/gstappsrc.h>
#include <gst/gst.h>  
#include <pthread.h>
#include "ros/ros.h"
#include <ros/package.h>
#include "gscam_vstreamer/topic.h"
#include <ros/time.h>
#include <sys/time.h>
#include "gscam_vstreamer/gscam_nodelet.h"
#include "gscam_vstreamer/nxjson.h"
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <boost/thread.hpp>
#include <image_transport/image_transport.h>

PLUGINLIB_EXPORT_CLASS(gscam_vstreamer_nodelet::GSCamNodelet, nodelet::Nodelet) 
PLUGINLIB_EXPORT_CLASS(gscam_vstreamer_nodelet::TopicCamNodelet, nodelet::Nodelet) 

#define DEFAULT_1080P_BITRATE 2000000
#define DEFAULT_720P_BITRATE 1000000
#define VSRC_WIDTH  "1280"
#define VSRC_HEIGHT "720"
#define VSRC_FRAMERATE "30"
#define PI3_SERIAL_NO_LENGTH 16
#define SPS_PPS_SIZE   64
#define MAX_IMAGE_DIR_PATH_LENGTH 64
#define MAX_IMAGE_FILENAME_LENGHT  64
#define JSONBUFSIZ 512
#define SNAPSHOT_KEYID 2001
#define GST_VSTREAMER_CONFIG_FILE  "gst_vstreamer.conf"
#define GST_PIPELINE_STRING_BUF_LENGTH   1024

#define H264_MAX_SIZE 256*1024
#define H264_MAP_MAX_SIZE 64*1024

#define MAX_SHARED_PTR_ARRAY_SIZE 10
#define PUBLISH_QUEUE_SIZE 10

typedef struct  
{  
    GMainLoop 		*loop;  
    GstElement 		*src_pipeline; 
    char 		spspps_buf[SPS_PPS_SIZE];
    int 		spspps_size;
    char 		imageDir[MAX_IMAGE_DIR_PATH_LENGTH];
    char 		imageFileName[MAX_IMAGE_FILENAME_LENGHT];
    pthread_mutex_t 	mutex;
    int 		snapshot_flag;
    int			vstreamer_init_done;
    int 		video_width;
    int 		video_height;
    int			video_framerate;
    unsigned char 	*h264_ptr; // for SendH264_Ext() length >= 64KB temporary buffer point
    int                 h264_len;
} ProgramData;  

typedef boost::shared_ptr<gscam_vstreamer::topic> topic_ptr;

double time_offset_;

GstElement *gst_pipeline=NULL; 
ros::Publisher h264bs_pub;
ros::Publisher image_pub;

int image_src_caps_set = 0;
GstElement *image_src = NULL;
image_transport::Subscriber image_sub;
#if 0
boost::shared_ptr<gscam_vstreamer::topic> image_msg(new gscam_vstreamer::topic);
boost::shared_ptr<gscam_vstreamer::topic> h264bs_msg(new gscam_vstreamer::topic);
#else
topic_ptr image_msg[MAX_SHARED_PTR_ARRAY_SIZE];
topic_ptr h264bs_msg[MAX_SHARED_PTR_ARRAY_SIZE];
#endif
unsigned int image_seq=0;
unsigned int h264bs_seq=0;
int image_shared_ptr_id=0;
int h264bs_shared_ptr_id=0;

static void handle_sigterm(int sig)
{
    g_print("[handle_sigterm] exit program (%d) !\n", sig);
    if(gst_pipeline != NULL) {
	    gst_element_set_state(gst_pipeline, GST_STATE_NULL);
	    gst_object_unref (gst_pipeline);
    }
    exit( 1 );
}

/**
 * Gets the gstreamer format for the corresponding ros image encoding
 */
void get_format(const char *ros_encoding, char **gst_type, char **gst_format) {
        *gst_type=(char*)"video/x-raw";
        if (strcmp(ros_encoding, "rgb8") == 0) {
                *gst_format=(char*)"RGB";
        } else if (strcmp(ros_encoding, "mono16") == 0) {
                *gst_format=(char*)"GRAY16_BE";
        } else if (strcmp(ros_encoding, "bayer_grbg8") == 0) {
                *gst_type=(char*)"video/x-bayer";
                *gst_format=(char*)"grbg";
        } else {
                ROS_INFO("Unsupported image format %s\n", ros_encoding);
        }
}

static int get_nal_unit(unsigned char *d, int frame_len, int *header_type)
{
    int i = 0;
	int j;

    if (d[i] == 0x00 && d[i + 1] == 0x00 && d[i + 2] == 0x00 && d[i + 3] == 0x01) {
        if(d[i+4] == 0x67 || d[i+4] == 0x27)
            *header_type = 7;
        else if ( d[i+4] == 0x68 || d[i+4] == 0x28 )
            *header_type = 8;
        else if ( d[i+4] == 0x65 || d[i+4] == 0x25 )
            *header_type = 5; 
        else 
            *header_type = 0;
        i = i + 4;        
        
    }
    else {
        printf(" can not find header start code\n");
        return 0;
    }
    while (i < frame_len) {
        if (d[i] == 0x00 && d[i + 1] == 0x00 && d[i + 2] == 0x00 && d[i + 3] == 0x01)
            break;
        i++;
    }
    return i;
}

static int process_image(const void *p, int size) 
{
        int frame_len = 0;
        int total_len = 0;
        int nal_len;
        int is_sps_pps_header;
        int slice_len = 0;
        int ret=0;
        unsigned char *st_buf,*pstr;

        frame_len = size;
        
        st_buf =  (unsigned char *)p;
        while (total_len < frame_len) {
        
               // parser multi NAL unit
				int is_sps_pps_header;
		            
				nal_len = get_nal_unit(&(st_buf[total_len]), frame_len - total_len,&is_sps_pps_header);
                pstr = &st_buf[total_len];
                
                if (nal_len == 0) {
                    int i = 0;
                    break; // skip error continue run
                }
                
				slice_len = slice_len  + nal_len;
				if(is_sps_pps_header == 5 && slice_len == frame_len) {
                    ret = 2;  
				}
				else if(is_sps_pps_header == 0 && slice_len == frame_len) {
					ret = 3;
				}else if((is_sps_pps_header == 7 || is_sps_pps_header == 8 ) && slice_len <= frame_len) { // send sps pps header
                    ret = 1;
				}
				else
				{
					printf ( "else - nal_type=%d slice_len=%d frame_len=%d\n", is_sps_pps_header, slice_len, frame_len );
				}
             
                total_len = total_len + nal_len;
          } // while end

          return ret;
}

unsigned int pre_time=0;
int hcc_cnt=0;
static GstFlowReturn on_new_sample_from_image_sink (GstElement * sink_elm, ProgramData * data)  
{  
    GstSample *sample = NULL;  
    GstBuffer *buffer;  
    GstFlowReturn ret;  
    GstMapInfo map;  
    ros::Time stamp;
    uint32_t ts;
    struct timespec      tm, now;
    unsigned int        curr_time=0;
    uint16_t sec, msec;

    GstClockTime bt = gst_element_get_base_time(data->src_pipeline);

    g_signal_emit_by_name(sink_elm, "pull-sample", &sample, &ret); 
    if(sample){  

        buffer = gst_sample_get_buffer (sample);  
//printf("on_new_sample_from_jpegenc_sink: buffer pts=%llu dts=%llu duration=%llu\n", buffer->pts, buffer->dts, buffer->duration);
	    clock_gettime(CLOCK_MONOTONIC, &tm);
        curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
        hcc_cnt++;

        if(pre_time == 0)
			pre_time = curr_time ;

		if( curr_time - pre_time > 1000 )
		{
            ROS_DEBUG("hcc_cnt=%d", hcc_cnt);
			hcc_cnt = 0;
			pre_time = curr_time ;
		}
    }  
    else{  
        g_print("sample is NULL \n");  
        return ret;  
    }  

    /* Mapping a buffer can fail (non-readable) */  
    if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {

        stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);
//printf("11: %.9f\n", stamp.toSec());
        //ts = (uint32_t)stamp.toNSec();
        sec = (uint16_t)stamp.sec;
        msec = (uint16_t)(stamp.nsec/1000000L);
        ts = sec << 16 | msec;

//ROS_INFO("image_sink: stamp_sec=(%lu:%d) stamp_msec=(%lu:%d) ts=%x", stamp.sec, sec, stamp.nsec, msec, ts);

#if 0
		image_msg->stamp = ts;

//printf("111: ts=%lu\n", ts);

		image_msg->size = map.size;
		image_msg->seq = image_seq;
		memcpy(&image_msg->buffer, map.data, map.size);

		//yuv420p_to_rgb24(map.data, (unsigned char *)&image_msg.buffer, 1280, 720);

		image_pub.publish(image_msg);
#else
        if(image_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
        {
           image_shared_ptr_id =0;
        }

		image_msg[image_shared_ptr_id]->stamp = ts;
//printf("111: ts=%lu\n", ts);

		image_msg[image_shared_ptr_id]->size = map.size;
        image_msg[image_shared_ptr_id]->seq = image_seq;
		memcpy(&image_msg[image_shared_ptr_id]->buffer, map.data, map.size);
        image_pub.publish(image_msg[image_shared_ptr_id]);
        image_shared_ptr_id++;
#endif
        image_seq++;

#if 0
        if(data->snapshot_flag == 1) {
            if(data->imageDir[0] != '\0') {
               FILE *f;
               char f_buf[MAX_IMAGE_FILENAME_LENGHT + MAX_IMAGE_DIR_PATH_LENGTH] = {0};
               pthread_mutex_lock(&(data->mutex));
               data->snapshot_flag = 0;
			   sprintf(f_buf,"%s/%s", data->imageDir, data->imageFileName);
//			   sprintf(f_buf,"%s/image%d.rgb", data->imageDir, hcc_cnt);
printf("f_buf=%s\n", f_buf);
			   memset(data->imageFileName, 0 , MAX_IMAGE_FILENAME_LENGHT);
			   pthread_mutex_unlock(&(data->mutex));
			
			   f = fopen(f_buf, "wb");
			   if( f != NULL) {
				   fwrite(map.data, 1, map.size, f);
    			   fclose(f);
    		   } // if(f)
    		} // if(imageDir)
    		else {
    		   printf("!!! SnapShot Image Directory == NULL !!!\n");
    		   printf("!!! SnapShot Image Directory == NULL !!!\n");
    		   printf("!!! SnapShot Image Directory == NULL !!!\n");
    		   
    		   pthread_mutex_lock(&(data->mutex));
               data->snapshot_flag = 0;
			   memset(data->imageFileName, 0 , MAX_IMAGE_FILENAME_LENGHT);
			   pthread_mutex_unlock(&(data->mutex));
    		}
   	} // if(snapshot_flag)
#endif
    } // if(gst_buffer_map)

    /* we don't need the appsink sample anymore */  
    gst_buffer_unmap(buffer, &map);
    gst_sample_unref (sample);  
//    hcc_cnt++;

    return GST_FLOW_OK;  
}

//for TopicCam
static void onImageMessageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    GstBuffer *buf;
    void *imgdata;

    GstMapInfo map;
    static GstClockTime timestamp=0;
    GstFlowReturn ret;

    GstCaps *caps;
    char *gst_type, *gst_format=(char *)"";

    if (image_src != NULL) {
        // Set caps from message
        if(image_src_caps_set == 0) {
            get_format(msg->encoding.c_str(), &gst_type, &gst_format);
            caps = gst_caps_new_simple (gst_type,
                    "format", G_TYPE_STRING, gst_format,
                    "width", G_TYPE_INT, msg->width,
                    "height", G_TYPE_INT, msg->height,
                    NULL);

            //gst_app_src_set_caps(image_src, caps);
            g_object_set(image_src, "caps", caps, NULL);
            image_src_caps_set = 1;
        }

        buf = gst_buffer_new_and_alloc(msg->step*msg->height*4);

        gst_buffer_map(buf, &map, GST_MAP_WRITE);   //or _MAP_READ?
        imgdata = map.data;

        GST_BUFFER_PTS(buf) = timestamp;
        GST_BUFFER_DURATION(buf) = gst_util_uint64_scale_int(1, GST_SECOND, 15);
        timestamp += GST_BUFFER_DURATION(buf);

        memcpy(imgdata, &msg->data[0], msg->step*msg->height);

        gst_buffer_unmap(buf, &map);

        //gst_app_src_push_buffer(appsrc_rgb, buf);
        g_signal_emit_by_name (image_src, "push-buffer", buf, &ret);

        gst_buffer_unref(buf);   //not sure
    }

}

unsigned int pre_time1=0;
int hcc_cnt1=0;
/* called when the appsink notifies us that there is a new buffer ready for 
 * processing */  
#if 0
static GstFlowReturn  
on_new_sample_from_sink (GstElement * sink_elm, ProgramData * data)  
{  
    GstSample *sample = NULL;  
    GstBuffer *buffer;   
    GstFlowReturn ret;  
    GstMapInfo map;  
    int i;
    ros::Time stamp;
    uint32_t ts;
    struct timespec      tm, now;
    unsigned int        curr_time=0;
    uint16_t sec, msec;

    GstClockTime bt = gst_element_get_base_time(data->src_pipeline);
   
    /* get the sample from appsink */    
    g_signal_emit_by_name(sink_elm, "pull-sample", &sample, &ret);   
    if(sample){  

        buffer = gst_sample_get_buffer (sample);  
//printf("on_new_sample_from_sink: buffer size = %d pts=%llu dts=%llu duration=%llu\n", gst_buffer_get_size(buffer), buffer->pts, buffer->dts, buffer->duration);
        //g_print("on_new_sample_from_sink() call!; size = %d\n", gst_buffer_get_size(buffer));
    }  
    else {  
        g_print("sample is NULL \n");  
        return ret;  
    }  
    

    /* Mapping a buffer can fail (non-readable) */  
    if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {  

        /* print the buffer data for debug */  
        //printf("\t size = %d, data=[%02x%02x%02x%02x %02x%02x%02x%02x]\n",
        //        map.size, map.data[0],map.data[1],map.data[2],map.data[3],map.data[4],map.data[5],map.data[6],map.data[7]);

//        if (((data->spspps_size==0) && (map.data[0]==0) && (map.data[1]==0) && (map.data[2]==0) && (map.data[3]==1)) && ((map.data[4]==0x27) || (map.data[4]==0x67)))
        if (((map.data[0]==0) && (map.data[1]==0) && (map.data[2]==0) && (map.data[3]==1)) && ((map.data[4]==0x27) || (map.data[4]==0x67)))
        {
            data->spspps_size = map.size;
            if(data->spspps_size > SPS_PPS_SIZE) {
        	data->spspps_size = SPS_PPS_SIZE;
            }

            //printf("Keep sps&pps (%d) in buffer!\n", data->spspps_size);

            memcpy(data->spspps_buf, map.data, data->spspps_size);
//        	ret = SendH264_Ext(map.size, map.data , 213, NULL);
        }
        else if ((map.data[4]==0x25) || (map.data[4]==0x65)) {
            //printf("(I frame) send sps&pps (%d) to vstreamer !\n", data->spspps_size);

//          ret = SendH264_Ext(data->spspps_size, data->spspps_buf , 213, NULL);

            stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);
//printf("22: %.9f\n", stamp.toSec());
            //ts = (uint32_t)stamp.toNSec();
			sec = (uint16_t)stamp.sec;
            msec = (uint16_t)(stamp.nsec/1000000L);
			ts = sec << 16 | msec;

//ROS_INFO("h264bs_sink: stamp_sec=(%lu:%d) stamp_msec=(%lu:%d) ts=%x", stamp.sec, sec, stamp.nsec, msec, ts);

#if 0
            h264bs_msg->stamp = ts;
//printf("222: ts=%lu\n", ts);

            h264bs_msg->size = data->spspps_size;
            h264bs_msg->seq = h264bs_seq;
            memcpy(&h264bs_msg->buffer, data->spspps_buf, data->spspps_size);
            h264bs_pub.publish(h264bs_msg);
printf("h264bs_msg: msg=%x msg=%x seq=%d\n", &h264bs_msg, h264bs_msg, h264bs_seq);
//printf("[1] h264bs_pub.publish: size=%d stamp=%lu\n", data->spspps_size, h264bs_msg->stamp);
#else
            if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
            {
				h264bs_shared_ptr_id = 0;
            }

            h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;
//printf("222: ts=%lu\n", ts);

            h264bs_msg[h264bs_shared_ptr_id]->size = data->spspps_size;
            h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
            memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, data->spspps_buf, data->spspps_size);
            h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);
//printf("[1] h264bs_pub.publish: size=%d stamp=%lu\n", data->spspps_size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
            h264bs_shared_ptr_id++;
#endif
            h264bs_seq++;

            //usleep(10000);

//            if(map.size < H264_MAP_MAX_SIZE)
            {
                //ret = SendH264_Ext(map.size, map.data , 213, NULL);
#if 0
		h264bs_msg->size = map.size;
                h264bs_msg->seq = h264bs_seq;
		memcpy(&h264bs_msg->buffer, map.data, map.size);
	        h264bs_pub.publish(h264bs_msg);
printf("h264bs_msg: msg=%x msg=%x seq=%d\n", &h264bs_msg, h264bs_msg, h264bs_seq);

//printf("[2] h264bs_pub.publish: size=%d stamp=%lu\n", map.size, h264bs_msg->stamp);
#else
                if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
                {
        		h264bs_shared_ptr_id = 0;
                }
                h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;
		h264bs_msg[h264bs_shared_ptr_id]->size = map.size;
                h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
		memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, map.data, map.size);
	        h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);

//printf("[2] h264bs_pub.publish: size=%d stamp=%lu\n", map.size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
                h264bs_shared_ptr_id++;
#endif
                h264bs_seq++;
              
	clock_gettime(CLOCK_MONOTONIC, &tm);
        curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
        hcc_cnt1++;

        if(pre_time1 == 0)
	    pre_time1 = curr_time ;

	if( curr_time - pre_time1 > 1000 )
	{
            printf("hcc_cnt1=%d\n", hcc_cnt1);
	    hcc_cnt1 = 0;
	    pre_time1 = curr_time ;
	}

            }
//            else
//            {
//                memcpy(data->h264_ptr, map.data, map.size);
//                data->h264_len += map.size;
//            }
        }
        else { // include P frame and not have start code case
//                if(map.size < H264_MAP_MAX_SIZE)
                {
                    if(data->h264_len > H264_MAX_SIZE)
                    {
                         printf("h264_len (%d) > 256KB would be skipped\n", data->h264_len);
                         data->h264_len = 0;
                    }
                    else
                    {
                         memcpy(data->h264_ptr+data->h264_len, map.data, map.size);
                         data->h264_len += map.size;
                         if(data->h264_len >= H264_MAP_MAX_SIZE)
                              printf("h264_len=%d\n", data->h264_len);

                        // SendH264_Ext(data->h264_len, data->h264_ptr, 213, NULL);

                        stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);
//printf("33: %.9f\n", stamp.toSec());
//                        ts = (uint32_t)stamp.toNSec();

  		          sec = (uint16_t)stamp.sec;
	               	  msec = (uint16_t)(stamp.nsec/1000000L);
	        	  ts = sec << 16 | msec;

//ROS_INFO("h264bs_sink: stamp_sec=(%lu:%d) stamp_msec=(%lu:%d) ts=%x", stamp.sec, sec, stamp.nsec, msec, ts);

#if 0
                        h264bs_msg->stamp = ts;
//printf("333: ts=%lu\n", ts);

		        h264bs_msg->size = data->h264_len;
                        h264bs_msg->seq = h264bs_seq;
			memcpy(&h264bs_msg->buffer, data->h264_ptr, data->h264_len);
		        h264bs_pub.publish(h264bs_msg);
printf("h264bs_msg: msg=%x msg=%x seq=%d\n", &h264bs_msg, h264bs_msg, h264bs_seq);

//printf("[3] h264bs_pub.publish: size=%d stamp=%lu\n", map.size, h264bs_msg->stamp);
#else
                        if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
                        {
        			h264bs_shared_ptr_id = 0;
                        }
                        h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;
//printf("333: ts=%lu\n", ts);

		        h264bs_msg[h264bs_shared_ptr_id]->size = data->h264_len;
                        h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
			memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, data->h264_ptr, data->h264_len);
		        h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);

//printf("[3] h264bs_pub.publish: size=%d stamp=%lu\n", map.size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
	                h264bs_shared_ptr_id++;
#endif
                        h264bs_seq++;

                        data->h264_len = 0;

	clock_gettime(CLOCK_MONOTONIC, &tm);
        curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
        hcc_cnt1++;

        if(pre_time1 == 0)
	    pre_time1 = curr_time ;

	if( curr_time - pre_time1 > 1000 )
	{
            printf("hcc_cnt1=%d\n", hcc_cnt1);
	    hcc_cnt1 = 0;
	    pre_time1 = curr_time ;
	}
                    }
                }
//                else
//                {
//                    memcpy(data->h264_ptr+data->h264_len, map.data, map.size);
//                    data->h264_len += map.size;
//                }
	    }
    }  // if (gst_buffer_map (buffer, &map, GST_MAP_READ)) 
    
    /* we don't need the appsink sample anymore */  
    gst_buffer_unmap(buffer, &map);
    gst_sample_unref (sample);  

    //g_print("GST_FLOW_OK \n");

    return GST_FLOW_OK;  
}  
#else
static GstFlowReturn  
on_new_sample_from_sink (GstElement * sink_elm, ProgramData * data)  
{  
    GstSample *sample = NULL;  
    GstBuffer *buffer;   
    GstFlowReturn ret;  
    GstMapInfo map;  
    int i, type=0;
    ros::Time stamp;
    uint32_t ts;
    struct timespec      tm, now;
    unsigned int        curr_time=0;
    uint16_t sec, msec;

    GstClockTime bt = gst_element_get_base_time(data->src_pipeline);
   
    /* get the sample from appsink */    
    g_signal_emit_by_name(sink_elm, "pull-sample", &sample, &ret);   
    if(sample){  

        buffer = gst_sample_get_buffer (sample);  
//printf("on_new_sample_from_sink: buffer size = %d pts=%llu dts=%llu duration=%llu\n", gst_buffer_get_size(buffer), buffer->pts, buffer->dts, buffer->duration);
        //g_print("on_new_sample_from_sink() call!; size = %d\n", gst_buffer_get_size(buffer));
    }  
    else {  
        g_print("sample is NULL \n");  
        return ret;  
    }  
    
    /* Mapping a buffer can fail (non-readable) */  
    if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {  

        /* print the buffer data for debug */  
        //printf("\t size = %d, data=[%02x%02x%02x%02x %02x%02x%02x%02x]\n",
        //        map.size, map.data[0],map.data[1],map.data[2],map.data[3],map.data[4],map.data[5],map.data[6],map.data[7]);

		type = process_image(map.data, map.size); //  0 - error, 1 - sps&pps, 2 - I, 3 - P
if(type != 3)
ROS_DEBUG("@@@ type=%d", type);
		if((type == 0 || type > 3) && data->h264_len == 0) // type is error, and is new frame
		{
	        g_print("process_image is error \n");  
ROS_WARN("process_image is error"); 
			gst_buffer_unmap(buffer, &map);
			gst_sample_unref (sample);  
			return GST_FLOW_ERROR;
		}

		if((type == 1 || type == 2 || type == 3) && data->h264_len != 0) // previous frame size is >= 64k
		{
			stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);
			sec = (uint16_t)stamp.sec;
			msec = (uint16_t)(stamp.nsec/1000000L);
			ts = sec << 16 | msec;

			if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
			{
				h264bs_shared_ptr_id = 0;
			}
			h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;
			h264bs_msg[h264bs_shared_ptr_id]->size = data->h264_len;
			h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
			memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, data->h264_ptr, data->h264_len);
			h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);
ROS_DEBUG("[4] h264bs_pub.publish: size=%d stamp=%lu", data->h264_len, h264bs_msg[h264bs_shared_ptr_id]->stamp);
			h264bs_shared_ptr_id++;
			h264bs_seq++;
			data->h264_len=0;
              
			clock_gettime(CLOCK_MONOTONIC, &tm);
			curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
			hcc_cnt1++;

			if(pre_time1 == 0)
				pre_time1 = curr_time ;

			if( curr_time - pre_time1 > 1000 )
			{
				ROS_DEBUG("hcc_cnt1=%d", hcc_cnt1);
				hcc_cnt1 = 0;
				pre_time1 = curr_time ;
			}
		}

        if (type == 1) // sps& pps
        {
            data->spspps_size = map.size;
            if(data->spspps_size > SPS_PPS_SIZE) {
        	data->spspps_size = SPS_PPS_SIZE;
            }

            ROS_DEBUG("Keep sps&pps (%d) in buffer!", data->spspps_size);

            memcpy(data->spspps_buf, map.data, data->spspps_size);
        }
        else if (type == 2) { // I frame
            ROS_DEBUG("(I frame) send sps&pps (%d) to vstreamer ", data->spspps_size);

			if(data->spspps_size != 0)
			{
				stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);
				sec = (uint16_t)stamp.sec;
				msec = (uint16_t)(stamp.nsec/1000000L);
				ts = sec << 16 | msec;

//ROS_INFO("h264bs_sink: stamp_sec=(%lu:%d) stamp_msec=(%lu:%d) ts=%x", stamp.sec, sec, stamp.nsec, msec, ts);

				if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
				{
					h264bs_shared_ptr_id = 0;
				}

				h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;

				h264bs_msg[h264bs_shared_ptr_id]->size = data->spspps_size;
				h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
				memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, data->spspps_buf, data->spspps_size);
				h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);
ROS_DEBUG("[1] h264bs_pub.publish: size=%d stamp=%lu", data->spspps_size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
				h264bs_shared_ptr_id++;
				h264bs_seq++;
			}

            if(map.size < H264_MAP_MAX_SIZE)
            {
                if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
                {
	        		h264bs_shared_ptr_id = 0;
                }
                h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;
				h264bs_msg[h264bs_shared_ptr_id]->size = map.size;
                h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
				memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, map.data, map.size);
				h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);
ROS_DEBUG("[2] h264bs_pub.publish: size=%d stamp=%lu", map.size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
                h264bs_shared_ptr_id++;
                h264bs_seq++;
				data->h264_len=0;
              
				clock_gettime(CLOCK_MONOTONIC, &tm);
				curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
				hcc_cnt1++;

				if(pre_time1 == 0)
					pre_time1 = curr_time ;

				if( curr_time - pre_time1 > 1000 )
				{
					ROS_DEBUG("hcc_cnt1=%d", hcc_cnt1);
					hcc_cnt1 = 0;
					pre_time1 = curr_time ;
				}
            }
            else
            {
//printf("1 - map.size >= 64k\n");
                memcpy(data->h264_ptr, map.data, map.size);
                data->h264_len += map.size;
            }
        }
        else 
		{	// P frame and has not start code
			if(map.size < H264_MAP_MAX_SIZE)
			{
				if(data->h264_len > H264_MAX_SIZE)
				{
					ROS_WARN("h264_len (%d) > 256KB would be skipped", data->h264_len);
					data->h264_len = 0;
				}
				else
				{
					memcpy(data->h264_ptr+data->h264_len, map.data, map.size);
					data->h264_len += map.size;
					if(data->h264_len >= H264_MAP_MAX_SIZE)
						printf("h264_len=%d\n", data->h264_len);

					stamp = ros::Time(GST_TIME_AS_USECONDS(buffer->pts+bt)/1e6+time_offset_);

					sec = (uint16_t)stamp.sec;
					msec = (uint16_t)(stamp.nsec/1000000L);
					ts = sec << 16 | msec;

//ROS_INFO("h264bs_sink: stamp_sec=(%lu:%d) stamp_msec=(%lu:%d) ts=%x", stamp.sec, sec, stamp.nsec, msec, ts);

					if(h264bs_shared_ptr_id >= MAX_SHARED_PTR_ARRAY_SIZE)
					{
			   			h264bs_shared_ptr_id = 0;
		            }
			        h264bs_msg[h264bs_shared_ptr_id]->stamp = ts;

			        h264bs_msg[h264bs_shared_ptr_id]->size = data->h264_len;
					h264bs_msg[h264bs_shared_ptr_id]->seq = h264bs_seq;
					memcpy(&h264bs_msg[h264bs_shared_ptr_id]->buffer, data->h264_ptr, data->h264_len);
					h264bs_pub.publish(h264bs_msg[h264bs_shared_ptr_id]);
//printf("h264bs_msg(%d): msg=%x seq=%d\n", h264bs_shared_ptr_id, h264bs_msg[h264bs_shared_ptr_id], h264bs_seq);
//printf("[3] h264bs_pub.publish: size=%d stamp=%lu\n", map.size, h264bs_msg[h264bs_shared_ptr_id]->stamp);
					h264bs_shared_ptr_id++;
					h264bs_seq++;

					data->h264_len = 0;

					clock_gettime(CLOCK_MONOTONIC, &tm);
					curr_time = tm.tv_sec * 1000 + tm.tv_nsec / 1000000L;
					hcc_cnt1++;

					if(pre_time1 == 0)
						pre_time1 = curr_time ;

					if( curr_time - pre_time1 > 1000 )
					{
						ROS_DEBUG("hcc_cnt1=%d", hcc_cnt1);
						hcc_cnt1 = 0;
						pre_time1 = curr_time ;
					}
				}
			}
            else
            {
//printf("2 - map.size >= 64k\n");
                memcpy(data->h264_ptr+data->h264_len, map.data, map.size);
                data->h264_len += map.size;
            }
	    }
    }  // if (gst_buffer_map (buffer, &map, GST_MAP_READ)) 
    
    /* we don't need the appsink sample anymore */  
    gst_buffer_unmap(buffer, &map);
    gst_sample_unref (sample);  

    //g_print("GST_FLOW_OK \n");

    return GST_FLOW_OK;  
}  
#endif

/* called when we get a GstMessage from the source pipeline when we get EOS, we 
 * notify the appsrc of it. */  
static gboolean  
on_appsink_message (GstBus * bus, GstMessage * message, ProgramData * data)  
{  
    switch (GST_MESSAGE_TYPE (message)) {  
        case GST_MESSAGE_EOS:  
            g_print ("The source got dry\n");  
            break;  
        case GST_MESSAGE_ERROR:  
            g_print ("Received error\n");  
            g_main_loop_quit (data->loop);  
            break;  
        default:  
            break;  
    }  
    return TRUE;  
}  

void writeLog(char * stringToWrite)
{
    FILE *f;
    if( stringToWrite != NULL) {
        f = fopen("./gst_vstreamer.log", "w");
    	fwrite(stringToWrite, sizeof(char), strlen(stringToWrite), f);
    	fflush(f);
    }
    else {
        f = fopen("./gst_vstreamer.log", "w+");
    }
    fclose(f);
}

void LoopForever()
{
    while(1)
       sleep(10);
}

//int main (int argc, char *argv[])  
void do_gscam()
{
#define TEMP_BUFFER_SIZE 64
    ProgramData *data = NULL;  
    gchar *string = NULL;  
    GstBus *bus = NULL;  
    GstElement *h264enc_sink = NULL;  
    GstElement *image_sink = NULL;  
    char temp[TEMP_BUFFER_SIZE]={0};
    gchar *vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=1280, height=720, framerate=30/1");
    gchar *h264_profile = (gchar *)"baseline";
    gchar *h264_bitrate_control = (gchar *)"default";
    int h264_bitrate = DEFAULT_720P_BITRATE; // 1.2 Mb
    int i;

printf("------- main -------------------\n");
    ROS_INFO("gscam: main\n");

    //ros::init(argc, argv, "gscam");
    ros::NodeHandle n;

    std::string path = ros::package::getPath("gscam_vstreamer");

    printf("package path=%s\n", path.c_str());

    // Calibration between ros::Time and gst timestamps
    GstClock * clock = gst_system_clock_obtain();
    ros::Time now = ros::Time::now();
    GstClockTime ct = gst_clock_get_time(clock);
    gst_object_unref(clock);
    time_offset_ = now.toSec() - GST_TIME_AS_USECONDS(ct)/1e6;
    ROS_INFO("Time offset: %.3f",time_offset_);

    for(i=0; i < MAX_SHARED_PTR_ARRAY_SIZE; i++)
    {
        image_msg[i] = (topic_ptr)(new gscam_vstreamer::topic);
        h264bs_msg[i] = (topic_ptr)(new gscam_vstreamer::topic);
    }

    h264bs_pub = n.advertise<gscam_vstreamer::topic>("h264bs", PUBLISH_QUEUE_SIZE);
    image_pub = n.advertise<gscam_vstreamer::topic>("image", PUBLISH_QUEUE_SIZE);

    //gst_init (&argc, &argv);  
    gst_init (NULL, NULL);  
    writeLog((char*)"");
/*
    if (argc > 1) {
        //printf("too many arguments");
        writeLog((char*)"too many arguments !\n");
        LoopForever();
    }
*/    

    data = g_new0 (ProgramData, 1);    
    data->loop = g_main_loop_new (NULL, FALSE); 
    data->h264_ptr = (unsigned char *)malloc(H264_MAX_SIZE); // MAX_SIZE=256KB for SendH264_Ext()
    data->h264_len = 0;

    g_print("setup signal !\n");
    
    signal( SIGPIPE, SIG_IGN );
    signal( SIGTERM, handle_sigterm );
    signal( SIGINT, handle_sigterm );
    signal( SIGUSR1,handle_sigterm );
    signal( SIGALRM, SIG_IGN);
   
    pthread_mutex_init(&(data->mutex), NULL);

    data->video_width = atoi(VSRC_WIDTH);
    data->video_height = atoi(VSRC_HEIGHT);
    data->video_framerate = atoi(VSRC_FRAMERATE);
#if 0   
    // read gst_vstreamer.conf (JSON)
    {
    	FILE *f = fopen(GST_VSTREAMER_CONFIG_FILE, "r");
		if( f == NULL ) {
	   		printf("Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
	   		sprintf(temp, "Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
       		writeLog(temp);
		}
		else {
			uint8_t json_buffer[256] = {0};
			size_t readSize = fread( (char*)json_buffer, sizeof(uint8_t), sizeof(json_buffer), f );
			fclose(f);
			printf("gst_vstreamer.conf (Size:%d)\n%s \n", readSize, json_buffer);
			const nx_json* json=nx_json_parse(json_buffer, 0);
    		if (json) {
    	 		//printf("Version=%s\n", nx_json_get(json, "Version")->text_value);
         		//printf("SnapShot_Image_Directory=%s\n", nx_json_get(json, "SnapShot_Image_Directory")->text_value);
         		sprintf(data->imageDir,"%s", nx_json_get(json, "SnapShotImageDirectory")->text_value);
         		printf("Snapshot Image Directory: %s\n", data->imageDir);   
         		nx_json_free(json);
    		} // if(json)
    	} // if(f)
    }
#else
    // read gst_vstreamer.conf (JSON)
    {
        char path_str[200]="";

        sprintf(path_str, "%s/%s", path.c_str(),GST_VSTREAMER_CONFIG_FILE);
        printf("$$$ path=%s\n", path_str);

 
    	FILE *f = fopen(path_str/*GST_VSTREAMER_CONFIG_FILE*/, "r");
		if( f == NULL ) {
	   		printf("Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
	   		sprintf(temp, "Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
       		writeLog(temp);
		}
		else {
			uint8_t json_buffer[256] = {0};
			size_t readSize = fread( (char*)json_buffer, sizeof(uint8_t), sizeof(json_buffer), f );
			fclose(f);
			printf("gst_vstreamer.conf (Size:%d)\n%s \n", readSize, json_buffer);
			const nx_json* json=nx_json_parse((char *)json_buffer, 0);
    		if (json) {
    	 		//printf("Version=%s\n", nx_json_get(json, "Version")->text_value);
         		//printf("SnapShot_Image_Directory=%s\n", nx_json_get(json, "SnapShot_Image_Directory")->text_value);
         		sprintf(data->imageDir,"%s", nx_json_get(json, "SnapShotImageDirectory")->text_value);
         		printf("Snapshot Image Directory: %s\n", data->imageDir);   
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "VideoResolution")->text_value);
         		printf("Video Resolution: %s\n", temp);
         		if(strcmp(temp,"1080p") == 0){
         		   data->video_width = 1920;
    			   data->video_height = 1080;
         		}
         		data->video_framerate = nx_json_get(json, "VideoFrameRate")->int_value;
         		if((data->video_framerate < 1) ||  (data->video_framerate > 30)){
         			printf("INVALID Video Frame Rate: %d\n", data->video_framerate);
         			data->video_framerate = atoi(VSRC_FRAMERATE);
         		}
         		else {
 	         		printf("Video Frame Rate: %d\n", data->video_framerate);
 	         	}
         		vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=%d, height=%d, framerate=%d/1", data->video_width, data->video_height, data->video_framerate);
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264Profile")->text_value);
         		printf("H264Profile: %s\n", temp);
         		if((strcmp(temp,"main") == 0) || (strcmp(temp,"high") == 0)){
         		   h264_profile = g_strdup_printf("%s", temp);
         		}    
         		h264_bitrate = nx_json_get(json, "H264BitRate")->int_value;  
         		if((h264_bitrate < 10) || (h264_bitrate > 10000000)) {
         			printf("INVALID H.264 bitrate: %d\n", h264_bitrate);
         			if(data->video_width == 1920) {
	         			h264_bitrate = DEFAULT_1080P_BITRATE;
	         		}
         			else {
	         			h264_bitrate = DEFAULT_720P_BITRATE;
	         		}
         		}
         		else {
	         		printf("H.264 bitrate: %d\n", h264_bitrate);	
	         	}
	         	memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264BitRateControl")->text_value);
         		printf("H264BitRateControl: %s\n", temp);
         		if((strcmp(temp,"variable") == 0) || (strcmp(temp,"constant") == 0) || (strcmp(temp,"constant-skip-frames") == 0)\
         		 || (strcmp(temp,"variable-skip-frames") == 0) || (strcmp(temp,"default") == 0) || (strcmp(temp,"disable") == 0)){
         			h264_bitrate_control = g_strdup_printf("%s", temp);
         		}
         		nx_json_free(json);
    		} // if(json)
    	} // if(f)
    }
#endif


    string =
        g_strdup_printf
// h264bs+image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! videoconvert ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! appsink name=image_sink",
//         h264_profile);
// h264bs+image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! videoconvert ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! appsink name=image_sink",
//         h264_profile);
#if 0
        ("-v v4l2src device=/dev/video0 do_timestamp=true io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! appsink name=image_sink",
         h264_profile);
#elif 0
        ("-v v4l2src device=/dev/video0 do_timestamp=true io-mode=2 ! %s ! tee name=video_source \
          ! omxh264enc target-bitrate=%d control-rate=%s ! video/x-h264, profile=%s, format=byte-stream \
          ! appsink name=h264enc_sink video_source. \
          ! queue ! appsink name=image_sink", \
        vsrc_out_caps, h264_bitrate, h264_bitrate_control, h264_profile);		 
#else
        ("-v v4l2src device=/dev/video0 do_timestamp=true io-mode=2 ! %s ! tee name=video_source \
          ! omxh264enc preset-level=0 profile=%s insert-sps-pps=1 bitrate=%d control-rate=1 iframeinterval=10 ! video/x-h264, stream-format=byte-stream \
          ! appsink name=h264enc_sink video_source. \
          ! queue ! appsink name=image_sink", \
        vsrc_out_caps, h264_profile, h264_bitrate );		 

#endif	 
		 
// h264bs
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink",
//         h264_profile);
// h264bs
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-h264, width=1280,height=720,framerate=30/1, profile=%s, format=byte-stream ! appsink name=h264enc_sink", h264_profile);
// image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! appsink name=image_sink",
//         h264_profile);
// h264bs+jpeg
//        ("--v v4l2src device=/dev/video0 io-mode=2 ! tee name=h264Enc_path ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=baseline, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! jpegenc ! appsink name=image_sink"
//     );
    
    g_print("Gstremaer PipeLine String:\n%s\n", string);
        
    /* setting up source pipeline, we read from a file and convert to our desired 
    * caps. */  
    data->src_pipeline = gst_parse_launch (string, NULL);  
    g_free (string);  
  
    if (data->src_pipeline == NULL) {  
        g_print ("Bad source\n");  
        return;  
    }  
  
    gst_pipeline = data->src_pipeline;
    
    /* to be notified of messages from this pipeline, mostly EOS */  
    bus = gst_element_get_bus (data->src_pipeline);  
    gst_bus_add_watch (bus, (GstBusFunc) on_appsink_message, data);  
    gst_object_unref (bus);  
  
    /* we use appsink in push mode, it sends us a signal when data is available 
    * and we pull out the data in the signal callback. We want the appsink to 
    * push as fast as it can, hence the sync=false */  
    h264enc_sink = gst_bin_get_by_name (GST_BIN (data->src_pipeline), "h264enc_sink");  
    g_object_set (G_OBJECT (h264enc_sink), "emit-signals", TRUE, "sync", FALSE, NULL);  
    g_signal_connect (h264enc_sink, "new-sample", G_CALLBACK (on_new_sample_from_sink), data);  
    gst_object_unref (h264enc_sink);  
    
    // ---- image sink
    image_sink = gst_bin_get_by_name (GST_BIN (data->src_pipeline), "image_sink");  
    g_object_set (G_OBJECT (image_sink), "emit-signals", TRUE, "sync", FALSE, NULL);  
    g_signal_connect (image_sink, "new-sample", G_CALLBACK (on_new_sample_from_image_sink), data);  
    gst_object_unref (image_sink);
    
    /* setting up sink pipeline, we push audio data into this pipeline that will 
    * then play it back using the default audio sink. We have no blocking 
    * behaviour on the src which means that we will push the entire file into 
    * memory. */  
  
    /* launching things */  
    gst_element_set_state (data->src_pipeline, GST_STATE_PLAYING);  
  
    /* let's run !, this loop will quit when the sink pipeline goes EOS or when an 
    * error occurs in the source or sink pipelines. */  
    g_print ("Let's run!\n");  
    g_main_loop_run (data->loop);  
    g_print ("Going out\n");  
  
    gst_element_set_state (data->src_pipeline, GST_STATE_NULL);  
  
    gst_object_unref (data->src_pipeline);  
    g_main_loop_unref (data->loop);  
    g_free (data);  
 
}  

//int main (int argc, char *argv[])  
void do_topiccam()
{
#define TEMP_BUFFER_SIZE 64
    ProgramData *data = NULL;  
    gchar *string = NULL;  
    GstBus *bus = NULL;  
    GstElement *h264enc_sink = NULL;  

    image_src = NULL;

    char temp[TEMP_BUFFER_SIZE]={0};
    gchar *vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=1280, height=720, framerate=30/1");
    gchar *h264_profile = (gchar *)"baseline";
    gchar *h264_bitrate_control = (gchar *)"default";
    int h264_bitrate = DEFAULT_720P_BITRATE; // 1.2 Mb
    int i;

printf("------- main topic cam-------------------\n");
    ROS_INFO("topiccam: main\n");

    //ros::init(argc, argv, "gscam");
    ros::NodeHandle n;

    std::string path = ros::package::getPath("gscam_vstreamer");

    printf("package path=%s\n", path.c_str());

    // Calibration between ros::Time and gst timestamps
    GstClock * clock = gst_system_clock_obtain();
    ros::Time now = ros::Time::now();
    GstClockTime ct = gst_clock_get_time(clock);
    gst_object_unref(clock);
    time_offset_ = now.toSec() - GST_TIME_AS_USECONDS(ct)/1e6;
    ROS_INFO("Time offset: %.3f",time_offset_);

    for(i=0; i < MAX_SHARED_PTR_ARRAY_SIZE; i++)
    {
        h264bs_msg[i] = (topic_ptr)(new gscam_vstreamer::topic);
    }

    h264bs_pub = n.advertise<gscam_vstreamer::topic>("h264bs", PUBLISH_QUEUE_SIZE);

    image_transport::ImageTransport it(n);
    image_sub = it.subscribe("/uav/camera1/image_raw", 3, onImageMessageCallback);

    //gst_init (&argc, &argv);  
    gst_init (NULL, NULL);  
    writeLog((char*)"");
/*
    if (argc > 1) {
        //printf("too many arguments");
        writeLog((char*)"too many arguments !\n");
        LoopForever();
    }
*/    

    data = g_new0 (ProgramData, 1);    
    data->loop = g_main_loop_new (NULL, FALSE); 
    data->h264_ptr = (unsigned char *)malloc(H264_MAX_SIZE); // MAX_SIZE=256KB for SendH264_Ext()
    data->h264_len = 0;

    g_print("setup signal !\n");
    
    signal( SIGPIPE, SIG_IGN );
    signal( SIGTERM, handle_sigterm );
    signal( SIGINT, handle_sigterm );
    signal( SIGUSR1,handle_sigterm );
    signal( SIGALRM, SIG_IGN);
   
    pthread_mutex_init(&(data->mutex), NULL);

    data->video_width = atoi(VSRC_WIDTH);
    data->video_height = atoi(VSRC_HEIGHT);
    data->video_framerate = atoi(VSRC_FRAMERATE);
#if 0   
    // read gst_vstreamer.conf (JSON)
    {
    	FILE *f = fopen(GST_VSTREAMER_CONFIG_FILE, "r");
		if( f == NULL ) {
	   		printf("Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
	   		sprintf(temp, "Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
       		writeLog(temp);
		}
		else {
			uint8_t json_buffer[256] = {0};
			size_t readSize = fread( (char*)json_buffer, sizeof(uint8_t), sizeof(json_buffer), f );
			fclose(f);
			printf("gst_vstreamer.conf (Size:%d)\n%s \n", readSize, json_buffer);
			const nx_json* json=nx_json_parse(json_buffer, 0);
    		if (json) {
    	 		//printf("Version=%s\n", nx_json_get(json, "Version")->text_value);
         		//printf("SnapShot_Image_Directory=%s\n", nx_json_get(json, "SnapShot_Image_Directory")->text_value);
         		sprintf(data->imageDir,"%s", nx_json_get(json, "SnapShotImageDirectory")->text_value);
         		printf("Snapshot Image Directory: %s\n", data->imageDir);   
         		nx_json_free(json);
    		} // if(json)
    	} // if(f)
    }
#else
    // read gst_vstreamer.conf (JSON)
    {
        char path_str[200]="";

        sprintf(path_str, "%s/%s", path.c_str(),GST_VSTREAMER_CONFIG_FILE);
        printf("$$$ path=%s\n", path_str);

 
    	FILE *f = fopen(path_str/*GST_VSTREAMER_CONFIG_FILE*/, "r");
		if( f == NULL ) {
	   		printf("Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
	   		sprintf(temp, "Fail to open %s !\n", GST_VSTREAMER_CONFIG_FILE);
       		writeLog(temp);
		}
		else {
			uint8_t json_buffer[256] = {0};
			size_t readSize = fread( (char*)json_buffer, sizeof(uint8_t), sizeof(json_buffer), f );
			fclose(f);
			printf("gst_vstreamer.conf (Size:%d)\n%s \n", readSize, json_buffer);
			const nx_json* json=nx_json_parse((char *)json_buffer, 0);
    		if (json) {
    	 		//printf("Version=%s\n", nx_json_get(json, "Version")->text_value);
         		//printf("SnapShot_Image_Directory=%s\n", nx_json_get(json, "SnapShot_Image_Directory")->text_value);
         		sprintf(data->imageDir,"%s", nx_json_get(json, "SnapShotImageDirectory")->text_value);
         		printf("Snapshot Image Directory: %s\n", data->imageDir);   
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "VideoResolution")->text_value);
         		printf("Video Resolution: %s\n", temp);
         		if(strcmp(temp,"1080p") == 0){
         		   data->video_width = 1920;
    			   data->video_height = 1080;
         		}
         		data->video_framerate = nx_json_get(json, "VideoFrameRate")->int_value;
         		if((data->video_framerate < 1) ||  (data->video_framerate > 30)){
         			printf("INVALID Video Frame Rate: %d\n", data->video_framerate);
         			data->video_framerate = atoi(VSRC_FRAMERATE);
         		}
         		else {
 	         		printf("Video Frame Rate: %d\n", data->video_framerate);
 	         	}
         		vsrc_out_caps = g_strdup_printf("video/x-raw, format=I420, width=%d, height=%d, framerate=%d/1", data->video_width, data->video_height, data->video_framerate);
         		memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264Profile")->text_value);
         		printf("H264Profile: %s\n", temp);
         		if((strcmp(temp,"main") == 0) || (strcmp(temp,"high") == 0)){
         		   h264_profile = g_strdup_printf("%s", temp);
         		}    
         		h264_bitrate = nx_json_get(json, "H264BitRate")->int_value;  
         		if((h264_bitrate < 10) || (h264_bitrate > 10000000)) {
         			printf("INVALID H.264 bitrate: %d\n", h264_bitrate);
         			if(data->video_width == 1920) {
	         			h264_bitrate = DEFAULT_1080P_BITRATE;
	         		}
         			else {
	         			h264_bitrate = DEFAULT_720P_BITRATE;
	         		}
         		}
         		else {
	         		printf("H.264 bitrate: %d\n", h264_bitrate);	
	         	}
	         	memset(temp, 0, TEMP_BUFFER_SIZE);
         		sprintf(temp,"%s", nx_json_get(json, "H264BitRateControl")->text_value);
         		printf("H264BitRateControl: %s\n", temp);
         		if((strcmp(temp,"variable") == 0) || (strcmp(temp,"constant") == 0) || (strcmp(temp,"constant-skip-frames") == 0)\
         		 || (strcmp(temp,"variable-skip-frames") == 0) || (strcmp(temp,"default") == 0) || (strcmp(temp,"disable") == 0)){
         			h264_bitrate_control = g_strdup_printf("%s", temp);
         		}
         		nx_json_free(json);
    		} // if(json)
    	} // if(f)
    }
#endif


    string =
        g_strdup_printf
// h264bs+image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! videoconvert ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! appsink name=image_sink",
//         h264_profile);
// h264bs+image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! videoconvert ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! appsink name=image_sink",
//         h264_profile);
#if 0
        ("-v v4l2src device=/dev/video0 do_timestamp=true io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! tee name=h264Enc_path ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! appsink name=image_sink",
         h264_profile);
#elif 0
        ("-v v4l2src device=/dev/video0 do_timestamp=true io-mode=2 ! %s ! tee name=video_source \
          ! omxh264enc target-bitrate=%d control-rate=%s ! video/x-h264, profile=%s, format=byte-stream \
          ! appsink name=h264enc_sink video_source. \
          ! queue ! appsink name=image_sink", \
        vsrc_out_caps, h264_bitrate, h264_bitrate_control, h264_profile);		 
#else
        ("-v appsrc name=imagesrc is-live=true  \
          ! videoconvert ! omxh264enc preset-level=0 profile=%s insert-sps-pps=1 bitrate=%d control-rate=1 iframeinterval=10 ! video/x-h264, stream-format=byte-stream\
          ! appsink name=h264enc_sink", 
           h264_profile, h264_bitrate);

#endif   

		 
// h264bs
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=%s, format=byte-stream ! appsink name=h264enc_sink",
//         h264_profile);
// h264bs
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-h264, width=1280,height=720,framerate=30/1, profile=%s, format=byte-stream ! appsink name=h264enc_sink", h264_profile);
// image
//        ("-v v4l2src device=/dev/video0 io-mode=2 ! video/x-raw, format=RGB, width=1280, height=720, framerate=30/1 ! appsink name=image_sink",
//         h264_profile);
// h264bs+jpeg
//        ("--v v4l2src device=/dev/video0 io-mode=2 ! tee name=h264Enc_path ! video/x-raw, format=I420, width=1280, height=720, framerate=30/1 ! omxh264enc ! video/x-h264, profile=baseline, format=byte-stream ! appsink name=h264enc_sink h264Enc_path. ! queue ! jpegenc ! appsink name=image_sink"
//     );
    
    g_print("Gstremaer TopicCam PipeLine String:\n%s\n", string);
        
    /* setting up source pipeline, we read from a file and convert to our desired 
    * caps. */  
    data->src_pipeline = gst_parse_launch (string, NULL);  
    g_free (string);  
  
    if (data->src_pipeline == NULL) {  
        g_print ("Bad source\n");  
        return;  
    }  
  
    gst_pipeline = data->src_pipeline;
    
    /* to be notified of messages from this pipeline, mostly EOS */  
    bus = gst_element_get_bus (data->src_pipeline);  
    gst_bus_add_watch (bus, (GstBusFunc) on_appsink_message, data);  
    gst_object_unref (bus);  
  
    /* we use appsink in push mode, it sends us a signal when data is available 
    * and we pull out the data in the signal callback. We want the appsink to 
    * push as fast as it can, hence the sync=false */  
    h264enc_sink = gst_bin_get_by_name (GST_BIN (data->src_pipeline), "h264enc_sink");  
    g_object_set (G_OBJECT (h264enc_sink), "emit-signals", TRUE, "sync", FALSE, NULL);  
    g_signal_connect (h264enc_sink, "new-sample", G_CALLBACK (on_new_sample_from_sink), data);  
    gst_object_unref (h264enc_sink);  
    
    // ---- image sink
    image_src = gst_bin_get_by_name (GST_BIN (data->src_pipeline), "imagesrc");
    
    /* setting up sink pipeline, we push audio data into this pipeline that will 
    * then play it back using the default audio sink. We have no blocking 
    * behaviour on the src which means that we will push the entire file into 
    * memory. */  
  
    /* launching things */  
    gst_element_set_state (data->src_pipeline, GST_STATE_PLAYING);  
  
    /* let's run !, this loop will quit when the sink pipeline goes EOS or when an 
    * error occurs in the source or sink pipelines. */  
    g_print ("Let's run!\n");  
    g_main_loop_run (data->loop);  
    g_print ("TopicCam Going out\n");  
  
    gst_element_set_state (data->src_pipeline, GST_STATE_NULL);  
  
    gst_object_unref (data->src_pipeline);  
    g_main_loop_unref (data->loop);  
    g_free (data);  
 
}  


namespace gscam_vstreamer_nodelet {
  GSCamNodelet::GSCamNodelet() :
    nodelet::Nodelet()
  {
  }

  GSCamNodelet::~GSCamNodelet() 
  {
    gscam_thread->join();
  }

  void GSCamNodelet::onInit()
  {
    gscam_thread.reset(new boost::thread(boost::bind(&do_gscam)));
  }

  TopicCamNodelet::TopicCamNodelet() :
    nodelet::Nodelet()
  {
  }

  TopicCamNodelet::~TopicCamNodelet() 
  {
    topiccam_thread->join();
  }

  void TopicCamNodelet::onInit()
  {
    topiccam_thread.reset(new boost::thread(boost::bind(&do_topiccam)));
  }

}
