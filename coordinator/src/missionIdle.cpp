

#include "missionIdle.h"
 
namespace Coordinator{




//MissionIdle

MissionState* MissionIdle:: Instance()
{
      ROS_DEBUG("MissionIdle::new Instance()-----------------------------------------------------");
      return new MissionIdle();
}
 
void MissionIdle::HandleMsg(Coordinator* fd)
{
    ROS_DEBUG("MissionIdle::HandleMsg()-----------------------------------------------------");

    Abort(fd);

     
}

void MissionIdle::ActivateGoROI(Coordinator* fd)
{
    ChangeState(fd, fd->getROI_state());  
}

 
void MissionIdle::ActivateGoSurround(Coordinator* fd)
{

    ROS_DEBUG(" MissionIdle::ActivateGoSurround===================================");
    ChangeState(fd, fd->getSurround_state());  
}



void MissionIdle::Abort(Coordinator* fd)
{
    fd->Abort();
    
}


 






} 