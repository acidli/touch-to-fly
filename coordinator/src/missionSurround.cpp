#include "missionSurround.h"

 
namespace Coordinator{





MissionGoSurround::MissionGoSurround()
{
  ROS_DEBUG("MissionGoSurround::MissionGoSurround():------333-----------------------------------------------");
     
}


MissionGoSurround::~MissionGoSurround(){}

//MissionGoSurround


MissionState* MissionGoSurround::Instance()
{
      ROS_DEBUG("MissionGoSurround::new Instance()---------222--------------------------------------------");
     
      return new MissionGoSurround();
}
 
void MissionGoSurround::HandleMsg(Coordinator* fd)
{
    ROS_DEBUG("MissionGoSurround::HandleMsg()-----------------------------------------------------");

    GoSurround(fd);
    
    
}

void MissionGoSurround::ActivateGoIdle(Coordinator* fd)
{
   ChangeState(fd, fd->getIdle_state()); 
  
}


void MissionGoSurround::ActivateGoROI(Coordinator* fd)
{
     ChangeState(fd, fd->getROI_state());   
}


void MissionGoSurround::GoSurround(Coordinator* fd)
{
     
    ROS_DEBUG("MissionGoSurround: fd->GoSurround();------333-----------------------------------------------");
     
    fd->GoSurround2();
}


 




} 