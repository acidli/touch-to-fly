
 
#include "missionROI.h"

 
namespace Coordinator{





MissionGoROI::MissionGoROI():
isNewGoROIStart(true)
{
  ROS_DEBUG("MissionGoROI::MissionGoROI():-------222----------------------------------------------");
     
}


MissionGoROI::~MissionGoROI(){}

//MissionGoROI


MissionState* MissionGoROI:: Instance()
{
      ROS_DEBUG("MissionGoROI::new Instance()---------222--------------------------------------------");
     
      return new MissionGoROI();
}
 
void MissionGoROI::HandleMsg(Coordinator* fd)
{
    ROS_DEBUG("MissionGoROI::HandleMsg()-----------------------------------------------------");

    GoROI(fd);
    
    
}

void MissionGoROI::ActivateGoIdle(Coordinator* fd)
{
   ROS_DEBUG("MissionGoROI::ActivateGoIdle()-----------------------------------------------------");
   ChangeState(fd, fd->getIdle_state()); 
  
}


void MissionGoROI::ActivateGoROI(Coordinator* fd)
{
    ros::Duration dur(20);


    ROS_DEBUG(" ========dur  %f==========now %f ......NewGoROIStartTime   %f  ",dur.toSec(),ros::Time::now().toSec(),NewGoROIStartTime.toSec());
        
    if((ros::Time::now().toSec() - NewGoROIStartTime.toSec())>dur.toSec()){
        ROS_DEBUG("DUR>5 =============now %f ......NewGoROIStartTime   %f  ",ros::Time::now().toSec(),NewGoROIStartTime.toSec());
        
        StartNewGoROI(fd);
    }
    else
        fd->setNewGoROIStart(false);

}


void MissionGoROI::GoROI(Coordinator* fd)
{

    ROS_DEBUG("MissionGoROI::GoROI()-----------------------------------------------------");
    if(isNewGoROIStart)  
    {
        NewGoROIStartTime = ros::Time::now();
        isNewGoROIStart = false;
        
    }
    fd->GoROI2();
}




void MissionGoROI::ActivateGoSurround(Coordinator* fd)
{

    ROS_DEBUG(" MissionGoROI::ActivateGoSurround===================================");
    ChangeState(fd, fd->getSurround_state());  
}




void MissionGoROI::StartNewGoROI(Coordinator* fd)
{
    isNewGoROIStart = true;
    fd->setNewGoROIStart(isNewGoROIStart);
    fd->Abort();
}




} 