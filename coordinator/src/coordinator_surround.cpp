
#include "missionstate.h"
#include "missionROI.h"
#include "missionIdle.h"
#include "missionSurround.h"

namespace Coordinator{

 



void Coordinator::surroundCallback(const cmd_publisher::surround::ConstPtr& msg) 
{
    ActivateGoSurround();  
    isNewGoSurround_time =  ros::Time::now();

    
      
    ROS_INFO(" =======================Coordinator::SurroundCallback   %d  %f %f",msg->Dir, msg->Degree,msg->radius);
    plannedSurDir = msg->Dir;
    plannedSurDegree = msg->Degree;
    plannedSurRadius = msg->radius;
    plannedSurROI = msg->AgainstROI; 

    if(plannedSurROI)
    {
        if(plannedSurRadius==0)
            isGoSurroundROIwithCurrent = true;
        else if(plannedSurRadius>0)
            isGoSurroundROIwithCurrent = false;
        else
            ROS_ERROR("WRONG plannedSurRadius");
    }
        


    sur_planned_turn_diff = plannedSurDegree; 
  

   
} 




void Coordinator::get_sur_start_ez_yaw()
{
    if(sur_isFirst_start_ez_yaw==false)
    {
        sur_start_ez_yaw = ez_yaw;
        sur_isFirst_start_ez_yaw=true;
    }

}




  
//TODO:NEED TO ADD COMMENTS
//surround radius is related to command update interval
//1.ezdrone rotate with some angular velocity for 5 seconds is not equal to 
//2.ezdrone rotate with some angular velocity for 1 seconds for 5 times
//for the 1st one second, ezdrone will roate more amount
//which causes 2. rotate more amount than 1.
void Coordinator::PlanSurCmdToEZflyer()
{

    ROS_INFO("PlanSurCmdToEZflyer======================================");

    sur_heading_angular_velocity_mov_by_vel = sur_heading_angular_velocity_mov_by_vel_base*sur_r_base_mov_by_vel/plannedSurRadius;
  
    if(plannedSurDir==1)  //counterclockwise
    {
        plannedTurnHeading = -sur_heading_angular_velocity_mov_by_vel*sur_scale_angular_velocity_mov_by_vel;
        plannedSurroundVy = 0.5;
    }
    else if(plannedSurDir==0)//clockwise
    {
        plannedTurnHeading = sur_heading_angular_velocity_mov_by_vel*sur_scale_angular_velocity_mov_by_vel;
        plannedSurroundVy = -0.5;
    }
    else
        ROS_ERROR("plannedSurDir VALUE NOT RIGHT");

}

void Coordinator::SendSurCmdToEZflyer()
{

    ROS_INFO("SendSurCmdToEZflyer======================================");
    setMission("mov_by_vel",0, plannedSurroundVy, 0,plannedTurnHeading, planned_surround_dur, 0, 0);
    publishMission();
    lastCMDTime = ros::Time::now();


}



void Coordinator::SendIdleCmdToEZflyer()
{
    setMission("abort",0, 0, 0, 0, 0, 0, 0); 
    publishMission();
    lastCMDTime = ros::Time::now();



}


void Coordinator::PlanAndSendSurCmdToEZflyer()
{
   
    PlanSurCmdToEZflyer();

    SendSurCmdToEZflyer();
    
}

void Coordinator::reset_sur_start_values()
{   
        sur_isFirst_start_ez_yaw=false;
        sur_isFirst_start_gpslocal = false;
}

bool Coordinator::isReachedIntentSurround()
{

    get_sur_start_ez_yaw();

    sur_turn_diff= (ez_yaw-sur_start_ez_yaw)*180/PI;
    ROS_INFO("==[%f %f]=== turn_diff= [ %f] ",ez_yaw,sur_start_ez_yaw,sur_turn_diff);


    if(fabs(sur_turn_diff)>sur_planned_turn_diff)
    {
        
        ROS_WARN("Coordinator::Abort()------------------------------------------");
     
        ActivateGoIdle();   
        SendIdleCmdToEZflyer();
        reset_sur_start_values();

        return true;
    }

    return false;

}




void Coordinator::get_sur_start_gpslocal()
{
    if(sur_isFirst_start_gpslocal==false)
    {
        
        start_gpslocalx=gpslocalx;
        start_gpslocaly=gpslocaly;
        sur_isFirst_start_gpslocal = true;

    }

}


void  Coordinator::EstimateSurRadius()
{
    get_sur_start_gpslocal();

   
    double deltax = gpslocalx - start_gpslocalx;
    double deltay = gpslocaly - start_gpslocaly;
    double distance = sqrt (deltax*deltax+deltay*deltay);
    double deltatheta = ez_yaw-sur_start_ez_yaw;
    double estimated_sur_R = fabs(distance/2/sin(deltatheta/2));

    ROS_DEBUG("distance[%f]",distance);
    ROS_INFO("tracked_roi_yaw_body[%f]gpslocalx[%f]turn_diff[%f]estimated_sur_R[%f]=======",tracked_roi_yaw_body,gpslocalx,sur_turn_diff,estimated_sur_R);           

}

void Coordinator::GoSurroundAnyPointWithDesignatedRadius() 
{ 
    ROS_INFO("GoSurroundAnyPointWithDesignatedRadius");

    if(isReachedIntentSurround())
    {
         ROS_WARN("GoSurroundAnyPointClockwise  is Accomplished!");
       
        if(isKeepEstimateSurROIActualRadius)
        {
            plannedSurROI = 1;
            ActivateGoSurround(); 
            GoSurroundROIwithActualDistanceToROI();
        }
        else
            ROS_WARN("2 GoSurroundAnyPointClockwise  is Accomplished!");


    
    }
    else
    {


        if((ros::Time::now().toSec() - lastCMDTime.toSec())>=sur_command_update_interval)//send new command to drone every command_update_interval second)
        {
            PlanAndSendSurCmdToEZflyer();
            EstimateSurRadius();
        }
    }

}

void Coordinator::GoSurroundAnyPointCounterClockwise()
{
    
   

}
 




void Coordinator::EstimateCurrSurROIRadius()
{
   
    double total_turn_heading = fabs(plannedSurDegree);
    double tan_of_tracked_roi_yaw_body = tan(fabs(tracked_roi_yaw_body)*PI/180);
    double sin_of_total_turn_heading = sin(total_turn_heading*PI/180);
    double cos_of_total_turn_heading = cos(total_turn_heading*PI/180);
    double the_diff_real_and_estimate;
    

    
    the_diff_real_and_estimate = max_estimated_sur_radius/(sin_of_total_turn_heading/tan_of_tracked_roi_yaw_body+cos_of_total_turn_heading);

    ROS_INFO("tracked_roi_yaw_body....%f",tracked_roi_yaw_body);
    ROS_INFO("total_turn_heading....%f",total_turn_heading);
    ROS_INFO("sin_of_total_turn_heading....%f",sin_of_total_turn_heading);
    ROS_INFO("cos_of_total_turn_heading....%f",cos_of_total_turn_heading);
    ROS_INFO("tan_of_tracked_roi_yaw_body....%f",tan_of_tracked_roi_yaw_body);

    
    max_estimated_sur_radius = max_estimated_sur_radius-the_diff_real_and_estimate;

    ROS_WARN("max_estimated_sur_radius======   %f",max_estimated_sur_radius);

       
}

void Coordinator::GoBacktoLastPoseAfterEstCurrRadius()
{
    plannedSurDegree = -sur_turn_diff;
    plannedSurROI = 0;
    plannedSurDir = (plannedSurDir+1)%2;

    ROS_WARN("GoBacktoLastPoseAfterEstCurrRadius...........plannedSurDegree %f",plannedSurDegree);
    ActivateGoSurround(); 
   
    GoSurroundAnyPointWithDesignatedRadius();


}



bool Coordinator::isEnoughSwayforEstimateNewRadiusForSurROIwithActual()
{

    get_sur_start_ez_yaw();

    sur_turn_diff= (ez_yaw-sur_start_ez_yaw)*180/PI;
    ROS_INFO("==[%f %f]=== turn_diff= [ %f] ",ez_yaw,sur_start_ez_yaw,sur_turn_diff);

    double min_planned_tracked_roi_yaw_body = 2;
    double max_planned_tracked_roi_yaw_body = 35;
    double actual_radius_attained_threshold = 5;


  
    sur_planned_turn_diff = 20;
    
    


    if(fabs(sur_turn_diff)>sur_planned_turn_diff)
    {
        if(fabs(tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius)>min_planned_tracked_roi_yaw_body)
        {
            ROS_WARN("Coordinator::Abort()-----------------%f,%f",sur_turn_diff,tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius);
            isKeepEstimateSurROIActualRadius = true;
            ActivateGoIdle();   
            SendIdleCmdToEZflyer();
            reset_sur_start_values();

            return true;
        }
        else
            ROS_INFO("TBC");
       
    } 
    else
        ROS_INFO("TBC");



    if(fabs(tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius)>max_planned_tracked_roi_yaw_body)
    {
            ROS_WARN("reached max_planned_tracked_roi_yaw_body %f,%f",sur_turn_diff,tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius);
            isKeepEstimateSurROIActualRadius = true;
            ActivateGoIdle();   
            SendIdleCmdToEZflyer();
            reset_sur_start_values();

            return true;
        
    } 
    else
        ROS_INFO("TBC");



   

    return false;

}




bool Coordinator::isReachedIntentGoSurroundROIwithActualDistance()
{

    get_sur_start_ez_yaw();

    sur_turn_diff= (ez_yaw-sur_start_ez_yaw)*180/PI;
    ROS_INFO("==[%f %f]=== turn_diff= [ %f] ",ez_yaw,sur_start_ez_yaw,sur_turn_diff);

    double still_point_to_tracked_roi_yaw_body = 2;
    double max_planned_tracked_roi_yaw_body = 35; 

  
    sur_planned_turn_diff = 20;
    
   
    if(fabs(sur_turn_diff)>sur_planned_turn_diff)
    {
        if(fabs(tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius)<=still_point_to_tracked_roi_yaw_body)
        {
            ROS_WARN("VERY CLOSE TO ACTUAL RADIUS==========%f,%f",sur_turn_diff,tracked_roi_yaw_body-first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius);
            isKeepEstimateSurROIActualRadius = false;


            if(fabs(sur_turn_diff)>=90)
            {
                ActivateGoIdle();   
                SendIdleCmdToEZflyer();
                reset_sur_start_values();

                return true;
            }  
      
            return false;
        }


    }

    return false;
}


void Coordinator::EstimateNewRadiusForSurROIwithActual()
{
    is_first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius = true;
    EstimateCurrSurROIRadius();
    
    if(isKeepEstimateSurROIActualRadius == false)
    {
        ROS_WARN("very close close");
    }
    else
    {
        isKeepEstimateSurROIActualRadius = true;
        GoBacktoLastPoseAfterEstCurrRadius();
    }
}

void Coordinator::GoSurroundROIwithEstimatedDistanceToROI()
{
    if(is_first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius)
    {
        first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius = tracked_roi_yaw_body;
        is_first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius = false;
    }
 

    if((ros::Time::now().toSec() - lastCMDTime.toSec())>=sur_command_update_interval)//send new command to drone every command_update_interval second)
    {
        PlanAndSendSurCmdToEZflyer();
        EstimateSurRadius();
    }
}


void Coordinator::GoSurroundROIwithActualDistanceToROI()
{
  
    ROS_INFO("GoSurroundROIwithActualDistanceToROI====plannedSurDegree   %f",plannedSurDegree);
  
   
    plannedSurRadius = max_estimated_sur_radius;
    prev_max_estimated_sur_radius = max_estimated_sur_radius;



    if(isReachedIntentGoSurroundROIwithActualDistance())
    {
        ROS_WARN("GoSurroundROIwithActualDistanceToROI is Accomplished");
    }
    else
    {

        if(isEnoughSwayforEstimateNewRadiusForSurROIwithActual())
            EstimateNewRadiusForSurROIwithActual();
        else
            GoSurroundROIwithEstimatedDistanceToROI();
    }


    





 

}

void Coordinator::GoSurroundROIwithPlannedSurRadius()
{

}


void Coordinator::GoSurroundROI() 
{
  if(isGoSurroundROIwithCurrent)
    GoSurroundROIwithActualDistanceToROI();
  else
    GoSurroundROIwithPlannedSurRadius();



}


void Coordinator::GoSurround2() 
{
   
    ROS_INFO("GoSurround2============================================");
    if(plannedSurROI==1)
        GoSurroundROI();
    else if(plannedSurROI==0)
        GoSurroundAnyPointWithDesignatedRadius();
    else
        ROS_ERROR("againstROI value should be 0 or 1");


}



void Coordinator::GoSurround() 
{
 
    double start_ez_yaw = ez_yaw;
    double command_update_interval = 0.5;
    double turn_diff_abort_threshold=85;
    double heading_angular_velocity_mov_by_vel=37;
    double r_base_mov_by_vel = 10;
    double scale_angular_velocity_mov_by_vel = 2.25;
    int stop_GoSurroundCnt=5;
    isGoSurround = true;




   

    if(plannedSurROI==1)
    {
        if(plannedSurRadius==0)
        {
            ROS_DEBUG("to be continued");
        }
        else
        {
            guessSurRadius = plannedSurRadius;
            plannedSurDir = 0;
            ROS_DEBUG("guessSurRadius............%f",guessSurRadius);

        }
    }
    else
        ROS_DEBUG("to be continued");

   


   
    heading_angular_velocity_mov_by_vel = heading_angular_velocity_mov_by_vel*r_base_mov_by_vel/guessSurRadius;



    ROS_DEBUG("GoSurround=============start_ez_yaw[%f]==========================",start_ez_yaw);



   
   // planned_surround_dur = 1;
    if(plannedSurDir==1)  //counterclockwise
    {
        plannedTurnHeading = -heading_angular_velocity_mov_by_vel*scale_angular_velocity_mov_by_vel;
        plannedSurroundVy = 0.5;
    }
    else if(plannedSurDir==0)
    {
        plannedTurnHeading = heading_angular_velocity_mov_by_vel*scale_angular_velocity_mov_by_vel;
        plannedSurroundVy = -0.5;
    }
    else
        ROS_ERROR("plannedSurDir VALUE NOT RIGHT");



    setMission("mov_by_vel",0, plannedSurroundVy, 0,plannedTurnHeading, planned_surround_dur, 0, 0);
    publishMission();
    lastCMDTime = ros::Time::now();

    double start_gpslocalx=gpslocalx;
    double start_gpslocaly=gpslocaly;

   
    while(isGoSurround)
    {
        
        if((ros::Time::now().toSec() - lastCMDTime.toSec())>=command_update_interval)//send new command to drone every command_update_interval second)
        {
    
        

            setMission("mov_by_vel",0, plannedSurroundVy, 0,plannedTurnHeading, planned_surround_dur, 0, 0);
            publishMission();
            lastCMDTime = ros::Time::now();


            double turn_diff= (ez_yaw-start_ez_yaw)*180/PI;

         ////estimated_sur_R

          
            double deltax = gpslocalx - start_gpslocalx;
            double deltay = gpslocaly - start_gpslocaly;
            double distance = sqrt (deltax*deltax+deltay*deltay);
            double deltatheta = ez_yaw-start_ez_yaw;

            double estimated_sur_R = fabs(distance/2/sin(deltatheta/2));

            ROS_DEBUG("distance[%f]",distance);
            ROS_INFO("tracked_roi_yaw_body[%f]gpslocalx[%f]turn_diff[%f]estimated_sur_R[%f]=======",tracked_roi_yaw_body,gpslocalx,turn_diff,estimated_sur_R);          
       
         
            
            if(plannedSurROI==1)
            {
                if(plannedSurRadius==0)
                {
                    ROS_DEBUG("to be continued");
                }
                else
                {
                    if(plannedSurDir==0) 
                    {
                        if((tracked_roi_yaw_body)<-10)
                        {
                            
                            sur_total_detect_time =  ros::Time::now().toSec() - isNewGoSurround_time.toSec();
                            ROS_INFO("UnderEstimated UnderEstimated UnderEstimated Radius -----------sur_total_detect_time---------- %f",sur_total_detect_time);

                            plannedVx=0.5; 
                            isFirstGoFwdmore=false;

                            #if 0
                            if(sur_total_detect_time<10)
                            #endif
                            ActivateGoROI();


                            #if 0
                            else
                            {
                                ROS_WARN("might have reached correct radius......................");
                                ActivateGoIdle();
                                setMission("abort",0, 0, 0, 0, 0, 0, 0); 
                                publishMission();
                            }     
                            #endif


                            break;

                        }
                        else if((tracked_roi_yaw_body)>10)
                        {
                            ROS_INFO("OverEstimated OverEstimated OverEstimated Radius --------------------- ");
                        
                            ActivateGoIdle();
                            setMission("abort",0, 0, 0, 0, 0, 0, 0); 
                            publishMission();
            
                            break;

                        }
                    }
                    else if(plannedSurDir==1)
                    {

                    }
                    else
                        ROS_DEBUG("to be continued");

                }
            }
            else
                ROS_DEBUG("to be continued");

            


            ROS_DEBUG("==[%f %f]=== turn_diff= [ %f] ",ez_yaw,start_ez_yaw,turn_diff);
            if(fabs(turn_diff)>turn_diff_abort_threshold){
                
                ROS_WARN("Coordinator::Abort()------------------------------------------");
                
         
                ActivateGoIdle();
                setMission("abort",0, 0, 0, 0, 0, 0, 0); 
                publishMission();
 
                break;
            }
        }

        ros::Duration(0.01).sleep(); 

    }
   
} 






}