 

#include "missionstate.h"
#include "missionROI.h"
#include "missionIdle.h"
#include "missionSurround.h"


namespace Coordinator{

 
 

Coordinator::Coordinator():
nh_(),
private_node_handle("~"),
poi_sub( nh_.subscribe("/poi", 1000,&Coordinator::poiCallback, this)),
roi_sub( nh_.subscribe("cmd_publisher/roi", 1000,&Coordinator::roiCallback, this)),
abort_sub( nh_.subscribe("cmd_publisher/abort", 1000,&Coordinator::abortCallback, this)),
turnHeading_sub( nh_.subscribe("cmd_publisher/turnHeading", 1000,&Coordinator::turnHeadingCallback, this)),
shiftMov_sub( nh_.subscribe("cmd_publisher/shiftMov", 1000,&Coordinator::shiftMovCallback, this)),
surround_sub( nh_.subscribe("cmd_publisher/surround", 1000,&Coordinator::surroundCallback, this)),
tracked_roi_sub( nh_.subscribe("/object_tracking/tracked_roi", 1,&Coordinator::trackedroiCallback, this)),
camera_info_sub (nh_.subscribe("/camera_info/camera_info", 100, &Coordinator::camerainfoCallback, this) ),
//gps_info_sub(nh_.subscribe("/mavros/global_position/global", 100, &Coordinator::gpsInfoCallback, this) ),
gps_info_sub(nh_.subscribe("/mavros/global_position/local", 100, &Coordinator::gpslocalCallback, this) ),
//imu_info_sub(nh_.subscribe("/mavros/imu/data", 100, &Coordinator::imuInfoCallback, this) ),
ezjobstatus_sub(nh_.subscribe("ezflyer/ezjobstatus", 10, &Coordinator::ezjobstatusCallback, this) ),
iscamerainfo(false), 
isFirstMov_by_vel(false),
isFirstTurn(true),
isStartMovebyvelOnly(false),
isDestVxAttained(false),
isFirstTrackedRoi(false),
missionabort(false), 
ez_exe_yaw_inc(0.0),
TweakTurnHeading(0.0),
isNewGoROIStart(true),
deviation_tracked_roi_pitch_body(0),
prev_tracked_roi_pitch_body(0), 
prev_tracked_roi_yaw_body(0),
isFirstCmdDroneMovebyVel(true),
isDestVxAttainedTime(false),
plannedVx(0.5),
plannedVy(0),
plannedVz(0),
scaleonPlanVz(1.2),
planned_goroi_dur(2),
plannedTurnHeading(60),//turnheading sent to drone; tried  =5,10, when vz increases slowly, even =15 make drone look like it almost not turned
MaxplannedVxVzProportion(0.7),
MinumumplannedHeight(1.0),
StartCheckingHeight(MinumumplannedHeight+2.0),
scaleonPlanVzInc(0.2),
isMaxplannedVzAttained(false),
MaxplannedVz(0),
gpslocalx(0),
gpslocaly(0),
guessSurRadius(10),
plannedSurDir(1),
planned_surround_dur(1.0),
tracked_roi_yaw_body_attain_center(2),
isveryFirstGoROICMDTimed(false), 
isFirstGoFwdmore(false),
GoSurroundCnt(0),
plannedSurroundVy(0.5),
prev_prev_tracked_roi_yaw_body(0.0),
isApproachingTarget(false),
sur_isFirst_start_ez_yaw(false),
sur_heading_angular_velocity_mov_by_vel_base(37),
sur_r_base_mov_by_vel(10),
sur_scale_angular_velocity_mov_by_vel(1.9),//2.25
sur_isFirst_start_gpslocal(false),
sur_command_update_interval(0.5),
isKeepEstimateSurROIActualRadius(false),
max_estimated_sur_radius(30),
is_first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius(false)   
{ 
       
    timer = nh_.createTimer(ros::Duration(1),  &Coordinator::timerCallback,this);
    thread_fd= new boost::thread( boost::bind(&Coordinator::FlightDirector, this));
 
    _state = Idle_state = MissionIdle::Instance(); 
    ROI_state = MissionGoROI::Instance();
    Surround_state = MissionGoSurround::Instance();
  
    mission_pub = nh_.advertise<coordinator::mission>("coordinator/mission", 1000); 
            
    #if (LOG_FOR_GIMBAL_COMPENSATION)
        myfile.open ("/home/nvidia/touch-to-fly-project/catkin_ws/src/touch-to-fly/coordinator/LOG/log_tracked_roi.txt");
    #endif      

}




Coordinator::~Coordinator()
{

    #if (LOG_FOR_GIMBAL_COMPENSATION)
        myfile.close();
    #endif  
}

 
void Coordinator::timerCallback(const ros::TimerEvent&)
{

    deviation_tracked_roi_pitch_body = tracked_roi_pitch_body - prev_tracked_roi_pitch_body;
    ROS_DEBUG("pitch[%f,%f,%f] ",prev_tracked_roi_pitch_body,tracked_roi_pitch_body,deviation_tracked_roi_pitch_body);
    prev_tracked_roi_pitch_body = tracked_roi_pitch_body;
 
}




void Coordinator::ezjobstatusCallback(const ezflyer::ezjobstatus::ConstPtr& msg) 
{
 

    ez_cur_time = msg->header.stamp.toSec() ;
    ez_mission=msg->mission;
    ez_drone_status=msg->drone_status;
    ez_height = msg->height;
    ez_vx = msg->vx;
    ez_vy = msg->vy;
    ez_vz =  msg->vz;
    ez_roll = msg->roll;
    ez_pitch =msg->pitch;
    ez_yaw =msg->yaw;
    ez_des_height =msg->des_height;
    ez_des_vx =msg->des_vx;
    ez_des_vy =msg->des_vy ;
    ez_des_vz =msg->des_vz;
    ez_des_dur= msg->des_dur;
    ez_des_velocity = msg->des_velocity;
    ez_des_height =msg->des_height;

//ROS_WARN("ez_yaw  %f",ez_yaw);
/*
calculate MaxplannedVxVzProportion based on takeoff height
this is a workaround for issue:Descending too fast would create a horrible wobble
for now
*/


#if 0
    if(ez_des_height>90) 
    {
        MaxplannedVxVzProportion = 0.6;
        ROS_DEBUG("HAVEN'T TESTED FOR THIS CASE");
        ROS_DEBUG("ez_des_height[%f] MaxplannedVxVzProportion:   [%f]================   ",ez_des_height,MaxplannedVxVzProportion);
   
     
    }
    else if(ez_des_height>80) 
    {
        MaxplannedVxVzProportion = 0.73;
     
    }
    else if(ez_des_height>70) 
    {
        MaxplannedVxVzProportion = 0.75;
     
    }
    else if(ez_des_height>60) 
    {
        MaxplannedVxVzProportion = 0.8;
     
    }
    else
        MaxplannedVxVzProportion = 1.0;
#endif

     ROS_DEBUG("ez_des_height[%f] MaxplannedVxVzProportion:   [%f]================   ",ez_des_height,MaxplannedVxVzProportion);
    
  
    if(msg->exe_start==true)
    {
          ez_exe_start_time = msg->header.stamp.toSec();
          ez_exe_start_yaw  = ez_yaw;
          ez_exe_start_fd = true; // let flight director know its command to ezflyer has just been executed
          ROS_DEBUG("ezflyer has received NEW command");
    }

    exe_time_elapsed =ez_cur_time - ez_exe_start_time;

    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_drone_status   %s   ez_mission %s   ",ez_drone_status.c_str(),ez_mission.c_str());

   
 /*
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_exe_start_time   %f   ",ez_exe_start_time);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_cur_time   %f   ",ez_cur_time);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_drone_status   %s   ez_mission %s   ",ez_drone_status.c_str(),ez_mission.c_str());
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  exe_time_elapsed   %f   ",exe_time_elapsed);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_des_vx %f ez_des_vy %f ez_des_vz %f   ",ez_des_vx,ez_des_vy,ez_des_vz);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_des_dur  %f ez_des_velocity  %f ez_des_height  %f ", ez_des_dur,ez_des_velocity,ez_des_height);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_vx %f ez_vy %f ez_vz  %f ",ez_vx,ez_vy,ez_vz);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_roll %f  ez_pitch  %f   ez_yaw     %f ",ez_roll,ez_pitch,ez_yaw);
    ROS_DEBUG(" Coordinator::ezjobstatusCallback  ez_height %f  ",ez_height);
*/
 

#if (LOG_FOR_GIMBAL_COMPENSATION) 
     
       
    myfile<<"stamp "<<msg->header.stamp<<std::endl;
    myfile<<"drone_status "<< ez_drone_status<<std::endl;
    myfile<<"mission"<<std::endl;

    myfile<<"   mission "<< ez_mission<<std::endl;
    myfile<<"   des_vx "<< ez_des_vx<<std::endl;
    myfile<<"   des_vy "<< ez_des_vy<<std::endl;
    myfile<<"   des_vz "<< ez_des_vz<<std::endl;
    myfile<<"   des_dur "<< ez_des_dur<<std::endl;
    myfile<<"   des_velocity "<< ez_des_velocity<<std::endl;
    myfile<<"   des_height "<< ez_des_height<<std::endl;
    myfile<<"position"<<std::endl;

    myfile<<"   height "<< ez_height<<std::endl;

    myfile<<"velocity"<<std::endl;
    myfile<<"   vx "<< ez_vx<<std::endl;
    myfile<<"   vy "<< ez_vy<<std::endl;
    myfile<<"   vz "<< ez_vz<<std::endl;
    myfile<<"orientation"<<std::endl;
    myfile<<"   roll "<< ez_roll<<std::endl;
    myfile<<"   pitch "<< ez_pitch<<" tracked_roi_pitch "<<tracked_roi_pitch_body<<std::endl;
    myfile<<"   yaw "<< ez_yaw<<std::endl;
    myfile<<"   tracked_roi_yaw "<< tracked_roi_yaw_body<<std::endl;
    myfile<<std::endl;

         

#endif


}
 


  

void Coordinator::poiCallback(const cmd_publisher::poi::ConstPtr& msg) 
{
    
    poi_X=msg->x;
    poi_Y=msg->y;
    ROS_DEBUG(" Coordinator::poiCallback  %d  %d  ",poi_X,poi_Y );

    if(iscamerainfo)
        poi_rpy_body();

} 
 



void  Coordinator::poi_rpy_body()
{
    poi_pitch_body = -atan2((poi_Y-cy),fy)* 180 / PI;  //rotate wrt cam_x axis(roll) = rotate wrt drone_y (pitch)
    poi_yaw_body = atan2((poi_X-cx),fx)* 180 / PI; //rotate wrt cam_y axis(pitch) = rotate wrt drone_z (yaw)
    
}



void Coordinator::abortCallback(const cmd_publisher::abort::ConstPtr& msg) 
{

    ActivateGoIdle();

    missionabort=msg->Abort; 
//    std::cout <<"-----------------------------abortCallback:  "<< std::boolalpha << missionabort << std::endl;

    setMission("abort",0, 0, 0, 0, 0, 0, 0); 
    
    ROS_DEBUG("Coordinator::Abort()------------------------------------------");
   
        
    publishMission();
   
} 



void Coordinator::turnHeadingCallback(const cmd_publisher::turnHeading::ConstPtr& msg) 
{

   // ActivateGoIdle(); 
    TweakTurnHeading = atan2(msg->disp_horizontal,fx)* 180 / PI; //rotate wrt cam_y axis(pitch) = rotate wrt drone_z (yaw)

      
    ROS_DEBUG(" =======================Coordinator::turnHeadingCallback   %d  %d TweakTurnYaw  %f",msg->disp_horizontal, msg->image_width,TweakTurnHeading);
    GoTweakTurnHeading();
} 



void Coordinator::GoTweakTurnHeading()
{

ROS_DEBUG(" =======================Coordinator::GoTweakTurnHeading     TweakTurnHeading  %f", TweakTurnHeading);
 
    setMission("turn_heading",TweakTurnHeading, 0, 1);
    publishMission();

}


void Coordinator::shiftMovCallback(const cmd_publisher::shiftMov::ConstPtr& msg) 
{

   
      
    ROS_DEBUG(" =======================Coordinator::shiftMovCallback   %f  %f %f",msg->speedy, msg->speedz,msg->duration);
    plannedShiftVy = msg->speedy;
    plannedShiftVz = msg->speedz;
    planned_shift_dur = msg->duration;

    GoTweakShift();

   
} 




void Coordinator::GoTweakShift() 
{
 
    ROS_DEBUG("GoTweakShift=======================================");
    setMission("mov_by_vel",0, plannedShiftVy, plannedShiftVz,0, planned_shift_dur, 0, 0); 
    publishMission();

   
} 






void Coordinator::roiCallback(const cmd_publisher::roi::ConstPtr& msg) 
{
    ActivateGoROI();

    if(isNewGoROIStart){


            
        ROS_DEBUG(" =====================================================================Coordinator::roiCallback  %d  %d  %d  %d",msg->x, msg->y, msg->roi_width, msg->roi_height);
        roi_TL_X=msg->x;
        roi_TL_Y=msg->y;
        roi_Width=msg->roi_width;
        roi_Height=msg->roi_height;
        
        
    //     ROS_DEBUG(" Coordinator::roiCallback  %d  %d  %d  %d",roi_TL_X,roi_TL_Y, roi_Width, roi_Height);

        calroiCenter();

        if(iscamerainfo)
            roi_rpy_body();
    }

} 


void Coordinator::trackedroiCallback(const object_tracking::tracked_roi::ConstPtr& msg)
{

 //   ROS_DEBUG(" Coordinator::trackedroiCallback  %d  %d  %d  %d",msg->x, msg->y, msg->width, msg->height);
    tracked_roi_TL_X = msg->x;
    tracked_roi_TL_Y = msg->y;
    tracked_roi_Width = msg->width;
    tracked_roi_Height =  msg->height;



    caltrackedroiCenter();

    if(iscamerainfo)
        tracked_roi_rpy_body();

}

void Coordinator::camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg)
{
    /*

    #     [fx  0 cx]
    # K = [ 0 fy cy]
    #     [ 0  0  1]

    */

    //   ROS_DEBUG(" Coordinator::camerainfoCallback  %d  %d  fx %f  fy %f  cx %f  cy %f", msg->width, msg->height,msg->K[0],msg->K[4],msg->K[2],msg->K[5]);



    fx = msg->K[0];
    fy = msg->K[4];
    cx = msg->K[2];
    cy = msg->K[5];
    Image_W = msg->width;
    Image_H = msg->height;

//    ROS_DEBUG(" Coordinator::camerainfoCallback  %d  %d  fx %f  fy %f  cx %f  cy %f", Image_W,Image_H,fx,fy,cx,cy);
    
    iscamerainfo=true;

}



void Coordinator::calroiCenter()
{

    roi_Center_x=roi_TL_X+0.5*roi_Width;
    roi_Center_y=roi_TL_Y+0.5*roi_Height;

//      ROS_DEBUG(" ::calroiCenter  %d  %d ",roi_Center_x, roi_Center_y);

}



void Coordinator::caltrackedroiCenter()
{

    tracked_roi_Center_x=tracked_roi_TL_X+0.5*tracked_roi_Width;
    tracked_roi_Center_y=tracked_roi_TL_Y+0.5*tracked_roi_Height;

//       ROS_DEBUG(" tracked::calroiCenter  %d  %d ",tracked_roi_Center_x, tracked_roi_Center_y);

}

void  Coordinator::roi_rpy_body()
{
    roi_roll_cam = atan2((roi_Center_y-cy),fy)* 180 / PI;  //rotate wrt cam_x axis(roll) = rotate wrt drone_y (pitch)
    roi_pitch_cam = atan2((roi_Center_x-cx),fx)* 180 / PI; //rotate wrt cam_y axis(pitch) = rotate wrt drone_z (yaw)
    
    roi_pitch_body = - roi_roll_cam;
    roi_yaw_body = roi_pitch_cam;
 
 
    ROS_DEBUG(" roi_pitch_body  %f   roi_yaw_body %f ",roi_pitch_body, roi_yaw_body);

}


void  Coordinator::tracked_roi_rpy_body()
{
    isFirstTrackedRoi=true;
    tracked_roi_roll_cam = atan2((tracked_roi_Center_y-cy),fy)* 180 / PI;  //rotate wrt cam_x axis(roll) = rotate wrt drone_y (pitch)
    tracked_roi_pitch_cam = atan2((tracked_roi_Center_x-cx),fx)* 180 / PI; //rotate wrt cam_y axis(pitch) = rotate wrt drone_z (yaw)



    tracked_roi_pitch_body = -tracked_roi_roll_cam;  //bottom to top as positive;correspond to tracked_roi_roll_cam;
    tracked_roi_yaw_body = tracked_roi_pitch_cam;    //left to right as positive;correspomd to  tracked_roi_pitch_cam;
    
 //   ROS_DEBUG(" tracked_roi_pitch_body  %f   tracked_roi_yaw_body %f ",tracked_roi_pitch_body, tracked_roi_yaw_body);
    ROS_INFO("tracked_roi_yaw_body[%f] ----------------",tracked_roi_yaw_body); 
    
}

 

//   void static_tf_cam_flight
//   {
    /*DONT DELETE !!THIS EXPLAINs HOW TO MOVE BODY TO ATTAIN SAME EFFECT CAMERA ROTATE  WRT CAMERA Y AXIS

    cam=Ccb*body;
    Ccb=[    0     -1    0
            -sin20    0  cos20
            -cos20    0  -sin20
        ]
        =[
            0     -1    0
            -0.9129  0   0.408
            -0.408   0   -0.9129
        ]


        axis/angle
    mathematically how to achieve rotate around     axis=-sin20*Xa+cos20*Za in drone body coordinate system
    

1.
Tcam: cam position in "body" coord   =(16,
                                    0,
                                    11)
Tbody:body position in "camera" coord = Ccb*[0,
                                        -10.1184,
                                        -16.5699]

                                        atan(10.11,16.5699)=31.38

angle between vector (camera cemter to body center) and ycam axis = 90-31.38 = 58.62

Rotation radius=16.5699
phi is defined as negtive when rotate clockwise wrt ycam
current body will move to (R*sin(phi),-10,R*cod(phi)) in camera coord

2.
next body position relative to current body reference frame

Cbc*(R*sin(phi),
-10,
R*cod(phi)) 

3. 
body rotate phi degree wrt ycam =new body frame rotate negtive phi around z axis in old body frame

suppose drone is horizontolly placed 
and initital orietation is (0,0,sin(the/2),cos(the/2))
it should have orietation (0,0,sin(phi/2+the/2),cos(phi/2+the/2)) at new position
if new body orientation is  (0,0,sin(rho/2),cos(rho/2))
it will need to move yaw  phi/2+the/2-rho/2

4.a more general case(drone doesn;t pose horizontally)?


    
    */



//    }


void Coordinator::gpsInfoCallback(const sensor_msgs::NavSatFixConstPtr &msg)
{
/*
    ROS_DEBUG("gps  %f",msg->latitude);
    ROS_DEBUG("gps  %f",msg->longitude);
    ROS_DEBUG("gps  %f",msg->altitude);
    */

}


void Coordinator::gpslocalCallback(const nav_msgs::OdometryConstPtr &msg)
{

  
 
//    ROS_WARN("gpslocalCallback orientation  %f,  %f,  %f,  %f",msg->pose.pose.orientation.x,msg->pose.pose.orientation.y,msg->pose.pose.orientation.z,msg->pose.pose.orientation.w);
//    ROS_WARN("gpslocalCallback position [%f,  %f,  %f]",msg->pose.pose.position.x,msg->pose.pose.position.y,msg->pose.pose.position.z);

    gpslocalx = msg->pose.pose.position.x;
    gpslocaly = msg->pose.pose.position.y;
  //  double radius = sqrt (x*x+y*y);
  //  ROS_WARN("RADIUS==========================%f",radius);


}

void Coordinator::imuInfoCallback(const sensor_msgs::ImuConstPtr &msg)
{
    

 //   ROS_DEBUG("imu orientation  %f,  %f,  %f,  %f",msg->orientation.x,msg->orientation.y,msg->orientation.z,msg->orientation.w);
   // ROS_DEBUG("imu angular_velocity  %f,  %f,  %f",msg->angular_velocity.x,msg->angular_velocity.y,msg->angular_velocity.z);
    
    double r=0,p=0,y=0;
    double& roll=r;
    double& pitch=p;
    double& yaw=y;
    toEulerAngle(msg->orientation.x,msg->orientation.y,msg->orientation.z,msg->orientation.w,roll,pitch,yaw);
 

}


void Coordinator::toEulerAngle(double x,double y,double z,double w, double& roll, double& pitch, double& yaw)
{
	// roll (x-axis rotation)
	double sinr_cosp = +2.0 * (w * x + y * z);
	double cosr_cosp = +1.0 - 2.0 * (x * x + y * y);
	roll = atan2(sinr_cosp, cosr_cosp);

	// pitch (y-axis rotation)
	double sinp = +2.0 * (w * y - z * x);
	if (fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	double siny_cosp = +2.0 * (w * z + x *y);
	double cosy_cosp = +1.0 - 2.0 * (y * y + z * z);  
	yaw = atan2(siny_cosp, cosy_cosp);
}



void Coordinator::publishCamBodyTF()
{ //TODO:publish TF

    tf::StampedTransform transform;
    try{
    listener.lookupTransform("/base_link", "/camera",  
                            ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
    }


    //   ROS_DEBUG("quaternion----[%f %f %f %f]",transform.getRotation().x(),transform.getRotation().y(),transform.getRotation().z(),transform.getRotation().w()  );
    tf::Quaternion q(transform.getRotation().x(),transform.getRotation().y(), transform.getRotation().z(), transform.getRotation().w());
    tf::Matrix3x3 m(q);//rotation matrix

    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    /*
    R:rotate with old X
    P:rotate with new Y
    Y:rotate with new Z
    
    
    std::cout << "Roll: " << roll << ", Pitch: " << pitch << ", Yaw: " << yaw << std::endl;
    std::cout << m[0][0] << "," << m[0][1] <<","<< m[0][2] << std::endl;
    std::cout << m[1][0] << "," << m[1][1] <<","<< m[1][2] << std::endl;
    std::cout << m[2][0] << "," << m[2][1] <<","<< m[2][2] << std::endl;
    
    
    0                 -1                 0
    
    -0.3420           0                0.9396  

    -0.9396           0                -0.3420 


    Ccb=[    0     -1    0
            -sin20    0  cos20
            -cos20    0  -sin20
                ]
    */


}






void Coordinator::setMission(std::string mission,float z,bool use_ned,bool wait_done)
{

    clearMission();
    missionmsg.mission=mission;
    missionmsg.z = z;  
    missionmsg.use_ned = use_ned;
    missionmsg.wait_done = wait_done;
}

 




void  Coordinator::setMission(std::string mission,float x,float y,float z,float velocity,float duration,bool use_ned,bool wait_done)
{
    clearMission();
    missionmsg.mission=mission;
    missionmsg.x = x;
    missionmsg.y = y;
    missionmsg.z = z; 
    missionmsg.velocity = velocity;
    missionmsg.duration = duration;
    missionmsg.use_ned = use_ned;
    missionmsg.wait_done = wait_done;

}




void  Coordinator::clearMission()
{
 
    missionmsg.mission.clear();
    missionmsg.x = 0;
    missionmsg.y = 0;
    missionmsg.z = 0; 
    missionmsg.velocity = 0;
    missionmsg.duration = 0;
    missionmsg.use_ned = false;
    missionmsg.wait_done = false;

} 





void Coordinator:: publishMission()
{
    mission_pub.publish(missionmsg);
}

 


void Coordinator::Turn_Heading(double roi_yaw_body,double heading_increment)
{
    if(roi_yaw_body>2||roi_yaw_body<-2)
    {
      
    
        if(roi_yaw_body>heading_increment){
            setMission("turn_heading",heading_increment, 0, 1);
       //     ROS_DEBUG("turn_heading(%f,%f)",roi_yaw_body,heading_increment);

        }
        else if(roi_yaw_body<-heading_increment){
            setMission("turn_heading",-heading_increment, 0, 1);
     //       ROS_DEBUG("turn_heading(%f,%f)",roi_yaw_body,-heading_increment);

        }
        else{
            setMission("turn_heading",roi_yaw_body, 0, 1);
     //       ROS_DEBUG("turn_heading(%f,%f)",roi_yaw_body,roi_yaw_body);
        }

 
        
        publishMission();
      

      
    }
     
       
       

    


}
 

void Coordinator::Move_by_Vel(double roi_pitch_body,double v_h,double v_p,double dur)
{  

    if(fabs(roi_pitch_body)>2)
    {
        setMission("mov_by_vel",v_h, 0, v_p,0, dur, 0, 0); 
        ROS_DEBUG("1 v_h %f v_p %f",v_h,v_p);

    } 
    else  
    {
        setMission("mov_by_vel",v_h, 0, 0,0,  dur, 0, 0);
        ROS_DEBUG("2 v_h %f ",v_h ); 
    }
    
 
    
    publishMission();
           
}
 

double  Coordinator::planofVz(double plannedVx,double scale_on_plan_vz)
{
    
    
    if(isMaxplannedVzAttained)
    {
        plannedVz = MaxplannedVz;
        
        return plannedVz;

       
    }
    else
    {
        plannedVz=-plannedVx*tan(tracked_roi_pitch_body*PI/180.0)*scale_on_plan_vz;
        ROS_DEBUG("2 =========================plannedVz z [%f]",plannedVz);
       
    }
       



          
    if(ReachingMinHeight())
    {
        plannedVz = 0;
        ROS_DEBUG("ReachingMinHeight...............................%f",MinumumplannedHeight);

    }
    else
    {
        
        if((plannedVz/plannedVx)>=MaxplannedVxVzProportion)
        {
            ROS_DEBUG("planofVz x,z [%f,%f]",plannedVx,plannedVz);

            plannedVz = MaxplannedVxVzProportion*plannedVx;
            isMaxplannedVzAttained = true;
            MaxplannedVz = plannedVz;
            ROS_DEBUG("1 ==========================plannedVz z [%f]",plannedVz);
        
        }
        else
        {
            
            ROS_DEBUG("4 =========================plannedVz z [%f]",plannedVz);


        }
    }

    return plannedVz;

}

 
 

void Coordinator::ChangeState(MissionState* s)
{
    ROS_DEBUG("Coordinator::ChangeState()-----------------------------------------------------");
    _state = s; 
}

void Coordinator::HandleMsg()
{
    ROS_DEBUG("Coordinator::HandleMsg()-----------------------------------------------------");    
    _state->HandleMsg(this);
}


void Coordinator::ActivateGoIdle()
{    
      ROS_INFO("Coordinator::ActivateGoIdle()-----------------------------------------------------");
     _state->ActivateGoIdle(this); 
} 

void Coordinator::ActivateGoROI()
{    
      ROS_INFO("Coordinator::ActivateGoROI()-----------------------------------------------------");
     _state->ActivateGoROI(this); 
} 


void Coordinator::ActivateGoSurround()
{    
      ROS_INFO("Coordinator::ActivateGoSurround()-----------------------------------------------------");
     _state->ActivateGoSurround(this); 
} 



void Coordinator::Abort()
{
    isFirstTrackedRoi = false;
 //   isStartMovebyvelOnly = false;
 //   isFirstTurn = true;
    isGoSurround = false;
   
      
    


}


 

void Coordinator::CmdDroneMovebyVel(double plannedVx,double plannedVy,double plannedVz,double plannedTurnHeading,double planned_dur)
{
    #if 0
    ROS_WARN("EXPECTED_tracked_roi_yaw_body[%f] ----------------[%f]",expected_tracked_roi_yaw_body); 
    #endif  
    if(tracked_roi_yaw_body>tracked_roi_yaw_body_attain_center)
    {
        ROS_INFO("1=yaw[%f] send cmd [plannedVx,plannedVz,plannedTurnHeading]----------------[%f,%f,%f]",tracked_roi_yaw_body,plannedVx,plannedVz,plannedTurnHeading); 
        setMission("mov_by_vel",plannedVx, plannedVy, plannedVz,plannedTurnHeading, planned_dur, 0, 0); 
    }
    else if(tracked_roi_yaw_body<-tracked_roi_yaw_body_attain_center) 
    {
        ROS_INFO("2=yaw[%f] send cmd [plannedVx,plannedVz,plannedTurnHeading]----------------[%f,%f,%f]",tracked_roi_yaw_body,plannedVx,plannedVz,-plannedTurnHeading); 
        setMission("mov_by_vel",plannedVx, plannedVy, plannedVz,-plannedTurnHeading, planned_dur, 0, 0); 
    }
    else
    {
        ROS_INFO("3=yaw[%f] send cmd [plannedVx,plannedVz,plannedTurnHeading]----------------[%f,%f,%f]",tracked_roi_yaw_body,plannedVx,plannedVz,0.0); 
        setMission("mov_by_vel",plannedVx, plannedVy, plannedVz,0, planned_dur, 0, 0);

         
    }
    
    
    publishMission();

}
   


void Coordinator::EstimateTrackedRoiYaw()
{
  //  double deltaT = 1.0;
     
    double accu_rotation=(ez_yaw-init_ez_yaw)*180/PI;
    double estimated_dist = 13;
    double accu_gpslocalx = gpslocalx-init_gpslocalx;
    double accu_gpslocaly = gpslocaly-init_gpslocaly;
    
     
    double s1 = estimated_dist*sin(init_tracked_roi_yaw_body*PI/180)- accu_gpslocalx;
    double s2 = estimated_dist*cos(init_tracked_roi_yaw_body*PI/180)- accu_gpslocaly;
    
ROS_DEBUG("S1,S2, ====================[%f,%f ]==atan2[%f],ACCUMULATE ROTATION[%f]",s1,s2,atan2(s1,s2)*180/PI,accu_rotation);

    expected_tracked_roi_yaw_body = atan2(s1,s2)*180/PI - accu_rotation;
    
}

  


void Coordinator::GoROITillApproachingTartget()
{

#if 1
plannedVx = 0;
plannedVy = 0;
plannedVz = 0;


#endif
    CmdDroneMovebyVel(plannedVx,plannedVy,plannedVz,plannedTurnHeading,planned_goroi_dur);
 

 
}

void Coordinator::GoTrackedROI()
{
    if(isApproachingTarget)
        ROS_DEBUG("Approaching the Target...............");
    else
        GoROITillApproachingTartget();
}

void Coordinator::GoROI2()
{

    if( isFirstTrackedRoi==true) 
        GoTrackedROI();
    else
        ROS_WARN("haven't received Tracked Roi,Not to plan mission yet");

}



void Coordinator::GoROI()
{
 
    ROS_DEBUG("MissionGoROI::GoROI()-----------------------------------------------------");
  
    
    double goroi_command_update_interval = 1;
    double GoFwdmore_time = 2.0;
 
    if( isFirstTrackedRoi==true) 
    { 

        #if 4
        if(plannedSurROI==1)
        {
         
            if(plannedSurRadius==0)
                ROS_INFO("TO BE CONTINUED");
            else
            { 
                if(fabs(tracked_roi_yaw_body)<=tracked_roi_yaw_body_attain_center)
                {
                    if(isFirstGoFwdmore==false)
                    {
                        FirstGoFwdmore_start_time = ros::Time::now();
                        isFirstGoFwdmore = true;
                    }
                    else
                    {
                        if((ros::Time::now().toSec() - FirstGoFwdmore_start_time.toSec())>=GoFwdmore_time)//send new command to drone every second
                        {
                            ActivateGoSurround();
                            isNewGoSurround_time =  ros::Time::now();

                        }
                        else
                            ROS_DEBUG("keep go roi");
                    }

                    

                    /*
                    if(!isFirstGoFwdmore)
                    {                    
                        init_gpslocalx = gpslocalx;
                        init_gpslocaly = gpslocaly;
                        isFirstGoFwdmore = true;
                        ROS_INFO("reached roi center.....init_gpslocalx[%f]init_gpslocaly[%f]",init_gpslocalx,init_gpslocaly);
                        ROS_INFO("reached roi center.....init_gpslocalx[%f]init_gpslocaly[%f]",init_gpslocalx,init_gpslocaly);

                    }
                }
                else
                    ROS_DEBUG("TO BE CONTINUED");

                if(isFirstGoFwdmore)
                {
                    accu_gpslocalx = gpslocalx-init_gpslocalx;
                    accu_gpslocaly = gpslocaly-init_gpslocaly;
                    ROS_INFO("accu_gpslocalx[%f]accu_gpslocaly[%f]",accu_gpslocalx,accu_gpslocaly);
                
                    double d=accu_gpslocalx*accu_gpslocalx+accu_gpslocaly*accu_gpslocaly;
                    double moveFwdDist=2;
 
                    if(d>=moveFwdDist*moveFwdDist)
                    {
                        ROS_INFO("MOVED FWD  MOVED FWD MOVED FWD..........%f",sqrt(d));
                        isFirstGoFwdmore = false;
                        ActivateGoSurround();
                       
                        isNewGoSurround_time =  ros::Time::now();
                    }
                }
                else
                    ROS_DEBUG("TO BE CONTINUED");
*/
                }
            }
            
        }
        else
            ROS_DEBUG("TO BE CONTINUED");
        #endif   
            
          
        if(isFirstCmdDroneMovebyVel)
        { 
            
            #if 0
            init_tracked_roi_yaw_body = tracked_roi_yaw_body;
            init_ez_yaw  = ez_yaw;
            init_gpslocalx = gpslocalx;
            init_gpslocaly = gpslocaly;

            #endif

            plannedVz = planofVz(plannedVx,scaleonPlanVz); 
            ROS_INFO("=====START NEW PLAN======= CmdDroneMovebyVel [plannedVx, plannedVz] = [%f,%f] ",plannedVx,plannedVz);

 


  
     
            CmdDroneMovebyVel(plannedVx,plannedVy,plannedVz,plannedTurnHeading,planned_goroi_dur);
            lastCMDTime = ros::Time::now();

     
     
       #if 0            
            
            if(!isveryFirstGoROICMDTimed){
                veryFirstGoROICMDTime = ros::Time::now();
                isveryFirstGoROICMDTimed = true;
            }
            
          
            ROS_DEBUG("veryFirstGoROICMDTime......%f", veryFirstGoROICMDTime.toSec());


       #endif  


            isFirstCmdDroneMovebyVel=false;
        }   
        else
        {
            if((ros::Time::now().toSec() - lastCMDTime.toSec())>=goroi_command_update_interval)//send new command to drone every second
            {

                #if 0  
          
                EstimateTrackedRoiYaw();
                #endif  
 
            
                if(CheckifDestinedVelocityisAttained())//desitned velocity just attained,wait for 2 seconds
                {
                    DestVxAttainedTime = ros::Time::now();
            
                    ROS_INFO("desitned velocity has attained,  planning a new scaleonPlanVz is allowed");
                    if(isMaxplannedVzAttained)
                        ROS_INFO("MaxplannedVz  is Attained,NOT allowed TO make a new scaleonPlanVz anymore...");
                    else
                    {
                        if(fabs(deviation_tracked_roi_pitch_body)<1.0)   //object is very far, increase vx                     
                        {
                            #if 0
                            isFirstCmdDroneMovebyVel = true;
                            isDestVxAttainedTime = false;

                        
                            scaleonPlanVz = scaleonPlanVz+scaleonPlanVzInc;
                            #endif
                            ROS_INFO("deviation of tracked pitch is less than 1,object is very far,make a new plan,increase scaleonPlanVz");
                        }
                        else
                            ROS_INFO("deviation of tracked pitch is not less than 1,NOT TO make a new scaleonPlanVz yet...");
                    }

                }
                else
                    ROS_INFO("desitned velocity has not attained,NOT TO make a new scaleonPlanVz yet...");
        

                plannedVz = planofVz(plannedVx,scaleonPlanVz);
                    
             
                CmdDroneMovebyVel(plannedVx,plannedVy,plannedVz,plannedTurnHeading,planned_goroi_dur);
             
                
                lastCMDTime = ros::Time::now();
                ROS_INFO("=======CMD SENT TO DRONE==== plannedVx plannedVz [%f,%f] ",plannedVx,plannedVz);
            }
            else
                ROS_INFO("====1 sec has not passed since last command was sent,Not to send new command yet");
        }
        
        
       
    }
    else
        ROS_WARN("haven't received Tracked Roi,Not to plan mission yet");
   


                  

}


bool Coordinator::ReachingMinHeight()
{

    if(ez_height<=StartCheckingHeight){

        ROS_DEBUG("[%f] is Reaching MinHeight [%f]============",ez_height,MinumumplannedHeight);

        return true;
    }
    else
        return false;
}

bool Coordinator::CheckifDestinedVelocityisAttained()
{
    if(ez_vx>(ez_des_vx-0.2))  
        if(fabs(ez_vz)>(fabs(ez_des_vz)-0.2))
            isDestVxAttained = true;

    return isDestVxAttained;
}

   
 

 

void  Coordinator::FlightDirector( )
{
  
 
 //std::cout<<"=======FlightDirector====rrr======"<<ez_drone_status<<std::endl; 
 // int publish_rate=1;
 // ros::NodeHandlePtr node = boost::make_shared<ros::NodeHandle>();
 // ros::Publisher pub_b = node->advertise<std_msgs::String>("topic_b", 10);
  ros::Rate loop_rate(10);
  
  
  //const static double PI=3.14159265;
  //double  ez_exe_yaw_inc=0.0;
  
  while (ros::ok())
  {
      ROS_DEBUG("=======FlightDirector============%s==",ez_drone_status.c_str());
 //   std::cout<<"=======FlightDirector=========="<<ez_drone_status<<std::endl;  
  //  ROS_DEBUG("fabs(p->tracked_roi_yaw_body) %f ",fabs(p->tracked_roi_yaw_body));
  //DON'T DELETE   std::cout <<"p->isStartMovebyvelOnly:  "<< std::boolalpha << p->isStartMovebyvelOnly << std::endl;

 
  
    HandleMsg(); 

    loop_rate.sleep();
  }

}



}


void set_logger_level(int level)
{


    enum{Debug=0,Info=1,Warn=2,Error=3,Fatal=4,None=5};

    switch(level)
    {
        case  Debug: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Info: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Warn: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Warn)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Error: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Error)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
      //  case  None: 
     //       if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::None)) 
    //            ros::console::notifyLoggerLevelsChanged(); 
     //   break;

        default:
 
        break;

    }
   
    
}



int main(int argc, char **argv)
{ 
  
  ros::init(argc, argv, "coordinator"); 
  set_logger_level(2);
 

  Coordinator::Coordinator coordinator;
 

  ros::Rate loop_rate(10); 
  while (ros::ok())
  {
   
   
// process any incoming messages in this thread
    ros::spinOnce(); 
    loop_rate.sleep();  
      
  }


  return 0;
}
 
 
