#ifndef MISSION_SURROUND_H_
#define MISSION_SURROUND_H_

 

#include "missionstate.h"

namespace Coordinator{
 
 


class MissionGoSurround:public MissionState{
public:

MissionGoSurround();
~MissionGoSurround();

static MissionState* Instance();
virtual void HandleMsg(Coordinator*); 
virtual void GoSurround(Coordinator*);
virtual void ActivateGoIdle(Coordinator* fd);
virtual void ActivateGoROI(Coordinator* fd); 
 

private: 


};

 

}
#endif
