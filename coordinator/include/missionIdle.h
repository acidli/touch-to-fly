#ifndef MISSION_IDLE_H_
#define MISSION_IDLE_H_

 
#include "missionstate.h"

namespace Coordinator{
 
 


class MissionIdle:public MissionState{
public:
static MissionState* Instance();
virtual void HandleMsg(Coordinator*);
virtual void ActivateGoROI(Coordinator* fd);
virtual void ActivateGoSurround(Coordinator* fd);
virtual void Abort(Coordinator*);
 

};


 
 


}
#endif
