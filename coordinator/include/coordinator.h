#ifndef COMMAND_COORDINATOR_H_
#define COMMAND_COORDINATOR_H_



#include "ros/ros.h"
#include "cmd_publisher/poi.h" 
#include "cmd_publisher/roi.h" 
#include "cmd_publisher/abort.h" 
#include "cmd_publisher/turnHeading.h" 
#include "cmd_publisher/shiftMov.h" 
#include "cmd_publisher/surround.h" 

#include "object_tracking/tracked_roi.h"
#include <camera_info_manager/camera_info_manager.h>     
#include <math.h>       /* atan2 */
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>

#include <cv_bridge/cv_bridge.h>

#include <tf/transform_listener.h> 
 
#include "coordinator/command.h"
#include "coordinator/mission.h" 
#include <std_msgs/Int16.h>
#include <std_msgs/String.h>
 

#include "ezflyer/ezjobstatus.h"
#include <fstream>  

//#define LOG_FOR_GIMBAL_COMPENSATION 1
//#define  TEST_ROI_TURN_HEADING 1
 


 
 
namespace Coordinator{

class MissionState;




class Coordinator
{

public:

    Coordinator();
    ~Coordinator();
   
    
    void poiCallback(const cmd_publisher::poi::ConstPtr& msg);
    void roiCallback(const cmd_publisher::roi::ConstPtr& msg);
    void abortCallback(const cmd_publisher::abort::ConstPtr& msg);
    void turnHeadingCallback(const cmd_publisher::turnHeading::ConstPtr& msg);
    void shiftMovCallback(const cmd_publisher::shiftMov::ConstPtr& msg);
    void surroundCallback(const cmd_publisher::surround::ConstPtr& msg);
 
    
    void trackedroiCallback(const object_tracking::tracked_roi::ConstPtr& msg);
    void camerainfoCallback(const sensor_msgs::CameraInfo::ConstPtr&  msg);
    void gpsInfoCallback(const sensor_msgs::NavSatFixConstPtr &msg);
    void gpslocalCallback(const nav_msgs::OdometryConstPtr &msg);
    void imuInfoCallback(const sensor_msgs::ImuConstPtr &msg);
    void ezjobstatusCallback(const ezflyer::ezjobstatus::ConstPtr& msg);


    void timerCallback(const ros::TimerEvent&);

    void calroiCenter();
    void poi_rpy_body();
    void roi_rpy_body();
    void caltrackedroiCenter();
    void tracked_roi_rpy_body();
 
    void publishCamBodyTF();
    

    void setMission(std::string mission,float z,bool use_ned,bool wait_done); 
    void setMission(std::string mission,float x,float y,float z,float velocity,float duration,bool use_ned,bool wait_done);
    void clearMission();
    void publishMission(); 
    void toEulerAngle(double x,double y,double z,double w, double& roll, double& pitch, double& yaw);

    void Turn_Heading(double roi_yaw_body,double heading_increment);   
    void Move_by_Vel(double roi_pitch_body,double v_h,double v_p,double dur);

    void GoROI();
    void goROItimerCallback(const ros::TimerEvent&);
    void GoROITillApproachingTartget();
    void GoTrackedROI(); 
    bool isApproachingTarget;





    void GoROI2();
    void Abort();
    void GoTweakTurnHeading();
    void GoTweakShift();
    void GoSurround();
    void GoSurround2();
    void GoSurroundAnyPointWithDesignatedRadius();
    void GoSurroundAnyPointClockwise();
    void GoSurroundAnyPointCounterClockwise();
    void GoSurroundROI();
    
    void GoSurroundROIwithActualDistanceToROI();
    bool isReachedIntentGoSurroundROIwithActualDistance();
    bool isEnoughSwayforEstimateNewRadiusForSurROIwithActual();
    void EstimateNewRadiusForSurROIwithActual();
    void GoSurroundROIwithEstimatedDistanceToROI();
    
    void GoSurroundROIwithPlannedSurRadius();
    void PlanAndSendSurCmdToEZflyer();
    bool isReachedIntentSurround();
   
    void EstimateSurRadius();
    void EstimateCurrSurROIRadius();
    void GoBacktoLastPoseAfterEstCurrRadius();


    void get_sur_start_ez_yaw();
    void get_sur_start_gpslocal();
    void reset_sur_start_values();
    void PlanSurCmdToEZflyer();
    void SendSurCmdToEZflyer();
    void SendIdleCmdToEZflyer();


  
    void FlightDirector();
    bool CheckifDestinedVelocityisAttained();
    void CmdDroneMovebyVel(double plannedVx,double plannedVy,double plannedVz,double plannedTurnHeading,double planned_dur);
    double planofVz(double plannedVx,double scale_on_plan_vz);
    bool ReachingMinHeight();
    void EstimateTrackedRoiYaw();




     
    void HandleMsg();
    void ActivateGoIdle();
    void ActivateGoROI();
    void ActivateGoSurround();
    MissionState* getROI_state() 
    {
        ROS_DEBUG(" MissionState* getROI_state() ============================");
        return ROI_state;
    };
    MissionState* getIdle_state() {return Idle_state;};
    MissionState* getSurround_state() {return Surround_state;};
    void setNewGoROIStart(bool s){ isNewGoROIStart=s; };
   
 

private:


    const static double PI=3.14159265;
    ros::Timer timer;
    ros::Timer goROItimer;


    ros::Time lastCMDTime;

    ros::Time DestVxAttainedTime;
    bool isFirstCmdDroneMovebyVel;
    bool isDestVxAttainedTime;

 
    double MaxplannedVxVzProportion;
    double MinumumplannedHeight;
    double StartCheckingHeight;

    double plannedVx;
    double plannedVy;
    double plannedVz;
    bool   isMaxplannedVzAttained;
    double MaxplannedVz;

    double plannedShiftVy;
    double plannedShiftVz;

    double scaleonPlanVz;
    double scaleonPlanVzInc;

    double planned_goroi_dur;
    double planned_shift_dur;
    double plannedTurnHeading; 


    double planned_surround_dur;
    unsigned int   plannedSurDir ;
    double plannedSurDegree ;
    double plannedSurRadius ;
    unsigned int  plannedSurROI;
    double guessSurRadius ;
    bool sur_isFirst_start_ez_yaw;
    bool sur_isFirst_start_gpslocal;
    bool isGoSurroundROIwithCurrent;
    double max_estimated_sur_radius ;
    double prev_max_estimated_sur_radius;
    bool isKeepEstimateSurROIActualRadius;
    bool is_first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius;
    double first_tracked_roi_yaw_body_go_sur_ROI_with_curr_radius;


    double sur_start_ez_yaw;
    double sur_planned_turn_diff;//=85;
    double sur_heading_angular_velocity_mov_by_vel;//=37;
    double sur_r_base_mov_by_vel;// = 10;
    double sur_scale_angular_velocity_mov_by_vel;// = 2.25;
    double sur_heading_angular_velocity_mov_by_vel_base;
    double sur_turn_diff;
    double sur_command_update_interval;
   

    

    double start_gpslocalx;
    double start_gpslocaly;

    bool isFirstGoFwdmore;
    ros::Time FirstGoFwdmore_start_time;



    double gpslocalx;
    double gpslocaly;
    double accu_gpslocalx ;
    double accu_gpslocaly ;
 
    std::string ez_drone_status;//FIXME
    boost::thread* thread_fd;

 
     

    ros::NodeHandle nh_;
    ros::NodeHandle private_node_handle;
    ros::Subscriber poi_sub;
    ros::Subscriber roi_sub;
    ros::Subscriber abort_sub;
    ros::Subscriber turnHeading_sub;
    ros::Subscriber shiftMov_sub;
    ros::Subscriber surround_sub;
    ros::Subscriber ezjobstatus_sub; 
    ros::Subscriber tracked_roi_sub; 
    ros::Subscriber camera_info_sub; 
    ros::Subscriber gps_info_sub; 
    ros::Subscriber imu_info_sub;  

     

    int poi_X,poi_Y;
    int roi_TL_X,roi_TL_Y;
    int roi_Width,roi_Height;
    int roi_Center_x,roi_Center_y;

    int tracked_roi_TL_X,tracked_roi_TL_Y;
    int tracked_roi_Width,tracked_roi_Height;
    int tracked_roi_Center_x,tracked_roi_Center_y;


    double fx,fy,cx,cy;
    int Image_W,Image_H;

    double  roi_roll_cam;
    double  roi_pitch_cam;

    double  tracked_roi_roll_cam;
    double  tracked_roi_pitch_cam;



    double  poi_pitch_body;
    double  poi_yaw_body;



    double  roi_pitch_body;   //bottom to top as positive;correspond to  roi_roll_cam;
    double  roi_yaw_body;     //left to right as positive;correspond to  roi_pitch_cam;
    
    double  tracked_roi_pitch_body;  //bottom to top as positive;correspond to tracked_roi_roll_cam;
    double  tracked_roi_yaw_body;    //left to right as positive;correspomd to  tracked_roi_pitch_cam;

    double prev_prev_tracked_roi_yaw_body;
    int GoSurroundCnt;
    bool isGoSurround;
    double plannedSurroundVy;
    ros::Time isNewGoSurround_time;
    double sur_total_detect_time;


    double deviation_tracked_roi_pitch_body;
    double prev_tracked_roi_pitch_body;
    double deviation_tracked_roi_yaw_body;
    double prev_tracked_roi_yaw_body;
    double tracked_roi_yaw_body_attain_center;
    ros::Time veryFirstGoROICMDTime;
    bool isveryFirstGoROICMDTimed;
    double  init_tracked_roi_yaw_body;    //left to right as positive;correspomd to  tracked_roi_pitch_cam;
    double expected_tracked_roi_yaw_body;
    double init_ez_yaw; 
    double init_gpslocalx;
    double init_gpslocaly;
 
    bool iscamerainfo;

    cv::Mat Ccb;
    tf::TransformListener listener;

    
    coordinator::command commandmsg;
    coordinator::mission missionmsg;

 
    ros::Publisher mission_pub;  

    #if (LOG_FOR_GIMBAL_COMPENSATION)
    std::ofstream myfile;
    #endif
 
    bool ez_exe_start_fd;    
    double ez_cur_time;
    double ez_exe_start_time;
    double ez_exe_start_yaw;
    std::string ez_mission;
    double ez_height;
    double ez_vx,ez_vy,ez_vz;
    double ez_roll,ez_pitch,ez_yaw;
    double ez_des_height;
    double ez_des_vx, ez_des_vy, ez_des_vz;
    double ez_des_dur;
    double ez_des_velocity;
    double exe_time_elapsed;
    bool isFirstTurn;
    bool isFirstMov_by_vel;
    bool isStartMovebyvelOnly;
    bool isDestVxAttained; 
    bool isFirstTrackedRoi;
    bool missionabort;
   
    double  ez_exe_yaw_inc;

    bool isNewGoROIStart;
    double TweakTurnHeading;

 

   

     
    friend class MissionState;
    void ChangeState(MissionState* ); 
    MissionState* _state;
    MissionState* Idle_state;
    MissionState* ROI_state;
    MissionState* Surround_state;

 




};
 

}






#endif
