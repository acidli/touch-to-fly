#ifndef MISSION_STATE_H_
#define MISSION_STATE_H_

 

#include "coordinator.h"

namespace Coordinator{



class MissionState{

public:
 
virtual void HandleMsg(Coordinator*);
virtual void GoROI(Coordinator*);
virtual void GoSurround(Coordinator*);
virtual void ActivateGoIdle(Coordinator* fd);
virtual void ActivateGoROI(Coordinator* fd);
virtual void ActivateGoSurround(Coordinator* fd);
 



protected:
void ChangeState(Coordinator*,MissionState*);

};
 

 
 


}
#endif
