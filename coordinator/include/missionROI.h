#ifndef MISSION_ROI_H_
#define MISSION_ROI_H_

 

#include "missionstate.h"

namespace Coordinator{
 
 


class MissionGoROI:public MissionState{
public:

MissionGoROI();
~MissionGoROI();

static MissionState* Instance();
virtual void HandleMsg(Coordinator*); 
virtual void GoROI(Coordinator*);
virtual void ActivateGoIdle(Coordinator* fd);
virtual void ActivateGoROI(Coordinator* fd);
virtual void ActivateGoSurround(Coordinator* fd);
    
void StartNewGoROI(Coordinator* fd);

private:
ros::Time NewGoROIStartTime;
bool isNewGoROIStart;


};
 


}
#endif
