#include <ros/ros.h> 
#include "object_tracking.h"

  


void set_logger_level(int level)
{


    enum{Debug=0,Info=1,Warn=2,Error=3,Fatal=4,None=5};

    switch(level)
    {
        case  Debug: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Info: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Warn: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Warn)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
        case  Error: 
            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Error)) 
                ros::console::notifyLoggerLevelsChanged(); 
        break;
      //  case  None: 
     //       if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::None)) 
    //            ros::console::notifyLoggerLevelsChanged(); 
     //   break;

        default:
 
        break;

    }
   
    
}




int main(int argc, char** argv)
{
  ros::init(argc, argv, "object_tracking");
  
  set_logger_level(3);
  Object_Tracking::ObjectTracking ot;
  ros::spin();
  return 0;
}
