#include "object_tracking.h"
#include "TrackerPYDNN.h"
#include <opencv2/core/core.hpp>

namespace Object_Tracking {

ObjectTracking::ObjectTracking(): 
    nh_(),
    private_node_handle("~"),
    it_(nh_),
    image_sub_(),
#if 1
    image_pub_(it_.advertise("/object_tracking/output_video", 1)),
#endif
    roi_sub( nh_.subscribe("cmd_publisher/roi", 1000,&ObjectTracking::roiCallback, this)),
    trackedroi_pub(nh_.advertise<object_tracking::tracked_roi>("/object_tracking/tracked_roi", 1000)),
    roi(0,0,0,0),
    isNewInitialROI(false),
    image_in("/image_rgb"),
    tracker_method(8),
    tracker(),
    m(),
    isStartNewTracking(false),
    tracking_output_video(false)
{    
 
    private_node_handle.param<std::string>("image_in", image_in, "/image_rgb");
    private_node_handle.param<int>("tracker_method", tracker_method, 0.0);  
    private_node_handle.param<bool>("tracking_output_video", tracking_output_video,false);



#if 1  //JOE: should only be valid when debugging object_tracking is ON, which the video is input from file
        //and the ROI in this case is activated from the very start
    int _x, _y, _width, _height, _debug=0;
    private_node_handle.param<int>("debug_tracking", _debug, 0);
    private_node_handle.param<int>("roi_x", _x, 0);
    private_node_handle.param<int>("roi_y", _y, 0);
    private_node_handle.param<int>("roi_width", _width, 0);
    private_node_handle.param<int>("roi_height", _height, 0);

    if(_debug == 1) {
        tracking_output_video = true;
        roi=cv::Rect2d( _x, _y, _width, _height);
    
        if(_width>0 && _height>0) {
            isNewInitialROI=true;
            isStartNewTracking=false;
        }
        printf("\n\n\nyayayayayaa\n\n\n");
    }

    
#endif

    image_sub_ = it_.subscribe(image_in, 1,&ObjectTracking::imageCallback, this);
    tracker =trackerMethod(); 
}

 
ObjectTracking::~ObjectTracking(){} 
   

void ObjectTracking::roiCallback(const cmd_publisher::roi::ConstPtr& msg) 
{
  //  m.lock();
    roi=cv::Rect2d( msg->x, msg->y, msg->roi_width, msg->roi_height);
  //  m.unlock();
    
    if(msg->roi_width!=0&& msg->roi_height!=0)
    { 
        isNewInitialROI=true;
        isStartNewTracking=false;
    }
}
  

void ObjectTracking::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
 
    
    if(isNewInitialROI)
    {  
      tracker->clear();
      cv::Ptr<cv::Tracker> trackerNew =trackerMethod();
      tracker = trackerNew;
      tracker->init(cv_ptr->image,roi); 
      isNewInitialROI=false;
      isStartNewTracking=true;
    }
    else if(isStartNewTracking)
    {
 
  //    m.lock();
      tracker->update(cv_ptr->image,roi);
 //     m.unlock();
         

      publishTrackedRoi(roi);

    
    } 

    //#if 1
    if(tracking_output_video) {
        rectangle( cv_ptr->image, roi, cv::Scalar( 255, 0, 0 ), 2, 1 );
 
        image_pub_.publish(cv_ptr->toImageMsg());
    }
  //  #endif
}




void ObjectTracking::publishTrackedRoi(cv::Rect2d roi) 
{
       
      object_tracking::tracked_roi trackedroimsg;
      trackedroimsg.x=roi.x;
      trackedroimsg.y=roi.y;
      trackedroimsg.width=roi.width;
      trackedroimsg.height=roi.height;

      trackedroi_pub.publish(trackedroimsg);
}



     
cv::Ptr<cv::Tracker> ObjectTracking::trackerMethod()
{
printf("\n\n method ====== %d\n\n\n", tracker_method);
    
    switch(tracker_method)
    {
        
        case  MIL:
        tracker = cv::TrackerMIL::create();
        ROS_DEBUG("MIL----------------------------");
        break;
        case  Boosting:
        tracker = cv::TrackerBoosting::create();
        ROS_DEBUG("Boosting----------------------------");
        break;
        case  MedianFlow:
        tracker = cv::TrackerMedianFlow::create();
        ROS_DEBUG("MedianFlow----------------------------");
        break;
        case  TLD:
        tracker = cv::TrackerTLD::create();
        ROS_DEBUG("TLD----------------------------");
        break;
        case  KCF:
printf("\n\n!!KCF KCF\n\n");
        tracker = cv::TrackerKCF::create();
        ROS_DEBUG("KCF----------------------------");
        break;
        case  GOTURN:
        tracker = cv::TrackerGOTURN::create();
        ROS_DEBUG("GOTURN----------------------------");
        break;
        case  MOSSE:
        tracker = cv::TrackerMOSSE::create();
        ROS_DEBUG("MOSSE----------------------------");
        break;
        case  CSRT:
printf("\n\n!!CSRT  CSRT\n\n");
        tracker = cv::TrackerCSRT::create();
        ROS_DEBUG("CSRT----------------------------");
        break;
        case  PYDNN:
printf("\n\n!!PYDNN  PYDNN\n\n");
        tracker = cv::TrackerPYDNN::create();
        ROS_WARN("PYDNN----------------------------");
        break;
        
        default:
            ROS_DEBUG("CSRT----------------------------");
            tracker = cv::TrackerCSRT::create();
#if 1
            cv::FileStorage fs("param.xml", cv::FileStorage::WRITE);
            tracker->write(fs);
#endif
            break;

    }

    return tracker;
}



}
