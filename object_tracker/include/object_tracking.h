#ifndef OBJECT_TRACKING_H_
#define OBJECT_TRACKING_H_

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>
#include "cmd_publisher/roi.h" 
#include "object_tracking/tracked_roi.h"
 
#include <mutex>         
 
namespace Object_Tracking {

class ObjectTracking
{ 
public:
    ObjectTracking();
    ~ObjectTracking();

    void roiCallback(const cmd_publisher::roi::ConstPtr& msg);
    void imageCallback(const sensor_msgs::ImageConstPtr& msg);
    cv::Ptr<cv::Tracker> trackerMethod();
    void publishTrackedRoi(cv::Rect2d roi); 

private:
    enum{MIL=1,Boosting=2,
	MedianFlow=3,TLD=4,KCF=5,
	GOTURN=6,MOSSE=7,
	CSRT=8, PYDNN=9};
    ros::NodeHandle nh_;
    ros::NodeHandle private_node_handle;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    image_transport::Publisher image_pub_;
    ros::Subscriber roi_sub; 
    ros::Publisher trackedroi_pub ; 
    cv::Rect2d roi;  
 //   bool isFirstImg;
    bool isNewInitialROI;
    std::string image_in; 
    int tracker_method;
    cv::Ptr<cv::Tracker> tracker; 
   
    std::mutex m;
    bool isStartNewTracking;
    bool tracking_output_video;
};

}
 #endif
     

