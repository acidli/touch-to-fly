#ifndef CAMERA_INFO_H_
#define CAMERA_INFO_H_
 
#include <ros/ros.h> 
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/distortion_models.h>
#include <cv_bridge/cv_bridge.h>


namespace Camera_Info{

class CameraInfo
{ 

  

public:
    CameraInfo();
    ~CameraInfo();
    
    
    void publishCamInfo(sensor_msgs::CameraInfoPtr camInfoMsg,ros::Publisher pubCamInfo, ros::Time t); 
    void fillCamInfo(sensor_msgs::CameraInfoPtr cam_info_msg, std::string FrameId) ;
    sensor_msgs::CameraInfoPtr getleftCamInfoMsg(){return leftCamInfoMsg;};
    ros::Publisher getpubLeftCamInfo(){return pubLeftCamInfo;};
    int parseCalibFile();

private:
    ros::NodeHandle nh;
    ros::NodeHandle private_node_handle;
    sensor_msgs::CameraInfoPtr leftCamInfoMsg;
    ros::Publisher pubLeftCamInfo;
    std::string leftCam_in,frameID,calibpath; 
    cv::Mat I,D;
    int width,height;

      
};



}

#endif