#include "camerainfo.h"


namespace Camera_Info {

CameraInfo::CameraInfo():
  nh(),
  private_node_handle("~"),
  leftCamInfoMsg((new sensor_msgs::CameraInfo())),
  pubLeftCamInfo(),
  leftCam_in(),
  frameID(),
  calibpath(),
  I(),
  D(),
  width(1280),
  height(720)
{    
  
    private_node_handle.param<std::string>("leftCam_in",leftCam_in,"/camera/camera_info");
    private_node_handle.param<std::string>("frameID",frameID,"tx2cam");
    private_node_handle.param<std::string>("calibpath",calibpath,"/home/nvidia/touch-to-fly-project/catkin_ws/src/touch-to-fly/camera_info/aux_cam_coeff.yaml");

    pubLeftCamInfo = nh.advertise<sensor_msgs::CameraInfo>(leftCam_in, 1);

    parseCalibFile();
    fillCamInfo( leftCamInfoMsg,frameID );
 
}

 
CameraInfo::~CameraInfo(){} 


int CameraInfo::parseCalibFile()
{
    std::string filename=calibpath;
                            
    cv::FileStorage fs;
    fs.open(filename,cv::FileStorage::READ);

    if(!fs.isOpened())
    {
        std::cout<<"failed to open  "<<filename<<std::endl;
        return 0;
    } 

    
  
    fs["I"]>>I;
    fs["D"]>>D;
 
    int width2,height2;

    fs["width"]>>width2;
    fs["height"]>>height2;

    if(width2==0||height2==0)
    {
        std::cout<<"image size should not be 0, will use default values"<<std::endl;

       
    }else{
        width=width2;
        height=height2;

    }



}

 



void CameraInfo::publishCamInfo(sensor_msgs::CameraInfoPtr camInfoMsg,ros::Publisher pubCamInfo, ros::Time t) 
{
        static int seq = 0;
        camInfoMsg->header.stamp = t;//Header timestamp should be acquisition time of image
        camInfoMsg->header.seq = seq;
        pubCamInfo.publish(camInfoMsg);
        seq++;
}

 
                

void CameraInfo::fillCamInfo(sensor_msgs::CameraInfoPtr left_cam_info_msg, std::string FrameId) 
{

// The distortion model used. Supported models are listed in
// sensor_msgs/distortion_models.h. For most cameras, "plumb_bob" - a
// simple model of radial and tangential distortion - is sufficient.
        left_cam_info_msg->distortion_model =
            sensor_msgs::distortion_models::PLUMB_BOB;
         

        left_cam_info_msg->D.resize(5);
        
        left_cam_info_msg->D[0] = D.at<double>(0,0);// -2.8895059859418731e-01;   // k1
        left_cam_info_msg->D[1] = D.at<double>(0,1);// 7.3860168029320092e-02;   // k2
        left_cam_info_msg->D[2] = D.at<double>(0,4);//-7.4905935282403866e-03 ;   // k3
        left_cam_info_msg->D[3] = D.at<double>(0,2);// 4.2125535172559574e-04;   // p1
        left_cam_info_msg->D[4] = D.at<double>(0,3);// 5.0210250200373868e-03;   // p2
         


        left_cam_info_msg->K.fill(0.0);
        
        left_cam_info_msg->K[0] = I.at<double>(0,0);// 5.0436993855110325e+02; //fx
        left_cam_info_msg->K[2] = I.at<double>(0,2);// 6.1464481023739484e+02; //cx
        left_cam_info_msg->K[4] = I.at<double>(1,1);// 5.0644141188648308e+02; //fy
        left_cam_info_msg->K[5] = I.at<double>(1,2);//3.1140179481504163e+02; //cy
        left_cam_info_msg->K[8] = 1.0;
         
         /*
         
            # Rectification matrix (stereo cameras only)
            # A rotation matrix aligning the camera coordinate system to the ideal
            # stereo image plane so that epipolar lines in both stereo images are
            # parallel.

         */

        left_cam_info_msg->R.fill(0.0);
        
        for (int i = 0; i < 3; i++) {
            // identity
            
            left_cam_info_msg->R[i + i * 3] = 1;
        }

         

        left_cam_info_msg->P.fill(0.0);
        
        left_cam_info_msg->P[0] = I.at<double>(0,0);// 5.0436993855110325e+02;
        left_cam_info_msg->P[2] = I.at<double>(0,2);// 6.1464481023739484e+02;
        left_cam_info_msg->P[5] = I.at<double>(1,1);// 5.0644141188648308e+02;
        left_cam_info_msg->P[6] = I.at<double>(1,2);// 3.1140179481504163e+02;
        left_cam_info_msg->P[10] = 1.0;
        
        left_cam_info_msg->width = width;
        left_cam_info_msg->height = height;

        left_cam_info_msg->header.frame_id = FrameId;// # Header frame_id should be optical frame of camera
        

}



}
   