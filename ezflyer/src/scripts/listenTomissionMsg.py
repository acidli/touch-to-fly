#!/usr/bin/env python
import rospy

import std_msgs.msg  
from std_msgs.msg import String
from std_msgs.msg import Int16
from rospy_tutorials.msg import HeaderString
#from ezdrone.msg import input_roi
from cmd_publisher.msg import mission
#from poi.msg import poi
from ezflyer.msg import ezjobstatus



from ezdrone.droneproxy import EZDrone
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] [%(asctime)s] [%(thread)d]: %(module)s: %(message)s')



drone = None 
lastStatus = None
 

def onMsgCallback(data):
    #message = data.data
    #header = data.header
    #timestamp = header.stamp.to_sec()
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s @ %12f", data.data, timestamp)
    
 #   rospy.loginfo(rospy.get_caller_id() + "I heard mission=(%s,%f,%f,%f,%f,%f,%d,%d) ", data.mission,data.x, data.y, data.z,data.velocity, data.duration,data.use_ned,data.wait_done)
    
    if data.mission=="takeoff":
        rospy.logwarn("\n\ntakeoff---------------------------------------------------------------")
        takeoff(data.z,data.wait_done)
    elif data.mission=="mov_to_pos":
        rospy.logwarn("\n\nmov_to_pos------------------------------------------------------------")
        mov_to_pos(data.x, data.y, data.z,data.velocity,data.use_ned,data.wait_done)
    elif data.mission=="mov_by_vel":
      #  rospy.logwarn("\n\nmov_by_vel------------------------------------------------------------")
        heading=data.velocity
        mov_by_vel(data.x, data.y, data.z,heading,data.duration,data.use_ned,data.wait_done)
    elif data.mission=="turn_heading":
        rospy.logwarn("\n\nturn_heading----------------------------------------------------------")
        turn_heading(data.z,data.use_ned,data.wait_done)
    elif data.mission=="land":
        rospy.logwarn("\n\nlanding---------------------------------------------------------------")
        land(data.wait_done)
    elif data.mission=="abort":
        rospy.logwarn("\n\nabort-----------------------------------------------------------------")
        mov_by_vel(0,0,0,0,0,0,0)
    elif data.mission=="guided":
        rospy.logwarn("\n\nguided----------------------------------------------------------------")
        switchtoguided()
   

 
def setDroneStatus(status):
    msg.drone_status=status
    msg.exe_start = False
    
 

def setDroneMission(mission,x,y,z,duration):
    msg.mission=mission
    
    setDroneStatus("Preparing")
    msg.exe_start = True
    if  mission=="takeoff":
        msg.des_height=z
    elif mission=="mov_by_vel":
        msg.mission="mov_by_vel"
        msg.des_vx = x
        msg.des_vy = y
        msg.des_vz = z
        msg.des_dur=duration 
    elif mission=="mov_to_pos":
        msg.mission="mov_to_pos"
        msg.des_vx = x
        msg.des_vy = y
        msg.des_vz = z
        msg.des_velocity=duration 
    elif mission=="turn_heading":
        msg.mission="turn_heading"
        msg.des_vz = z 
    elif mission=="land":
        msg.mission="land"
    elif mission=="abort":
        msg.mission="abort"
    elif mission=="guided":
        msg.mission="guided"
        




def setDroneKinematics():  
    
    msg.vx=drone.velocity[0]
    msg.vy=drone.velocity[1]
    msg.vz=drone.velocity[2]
    msg.roll = drone.attitude[0]
    msg.pitch = drone.attitude[1]
    msg.yaw = drone.attitude[2]
    msg.height= drone.height


def clearAllDestCtrl():
    msg.mission="None"
    msg.des_vx = 0
    msg.des_vy = 0
    msg.des_vz = 0
    msg.des_velocity=0 
 #   msg.des_height=0
    msg.des_dur=0




def switchtoguided():
    drone_status = drone.system_status 
    clearAllDestCtrl()
   # rospy.logwarn('SwitchToGUIDED==============================')
 
    setDroneMission("SwitchToGUIDED",0,0,0,0)
     
    ezjobstatus_publisher(msg)

    # Switch to the flight mode you want to test
    test_mode = 'GUIDED' 
    return_code = drone.switch_mode(test_mode) 
    rospy.logdebug('GUIDED return_code==============={}'.format(return_code))
 

def takeoff(height,wait_done):
    drone_status = drone.system_status 
    clearAllDestCtrl()
 
    setDroneMission("takeoff_GUIDED",0,0,0,0)
     
    ezjobstatus_publisher(msg)

    # Switch to the flight mode you want to test
    test_mode = 'GUIDED' 
    return_code = drone.switch_mode(test_mode) 
    rospy.logdebug('GUIDED return_code============================================{}'.format(return_code))
 

    flight_mode = drone.mode 
    
    setDroneMission("takeoff_Arm",0,0,0,0)
    ezjobstatus_publisher(msg)

   

    # Arm the drone and takeoff 
    return_code = drone.arm_disarm(True) 
    rospy.logdebug('Arm return_code==============================================={}'.format(return_code))

    takeoff_height = height

  
    setDroneMission("takeoff",0,0,takeoff_height,0)
    ezjobstatus_publisher(msg)
     
    
    return_code = drone.takeoff(takeoff_height) 
    rospy.logdebug('takeoff return_code==========================================={}'.format(return_code))



    current_height = drone.height 
  


def mov_to_pos(x,y,z,velocity,use_ned,wait_done):
    drone_status = drone.system_status
    clearAllDestCtrl()
   
   
    setDroneMission("mov_to_pos",x,y,z,velocity)
    ezjobstatus_publisher(msg) 
     
   
    return_code = drone.move_to_position(x, y, z, velocity, use_ned=use_ned)
   

        


def mov_by_vel(vx,vy,vz,heading,duration,use_ned,wait_done):
    drone_status = drone.system_status
    clearAllDestCtrl()
    maxVx = 3
    minVx = -3
    maxVy = 3
    minVy = -3
    maxVz = 2
    minVz = -3
    
    if  vx > maxVx:
        rospy.logdebug('vx [{r:5.2f}]   exceeds and set to  max vx [{s:5.2f}]'.format(r=vx,s=maxVx))   
        vx = maxVx    
    elif vx < minVx:
        rospy.logdebug('vx [{r:5.2f}]   less than  and set to  min vx [{s:5.2f}]'.format(r=vx,s=minVx))   
        vx = minVx    
    
    if vy > 3:
        rospy.logdebug('vy [{r:5.2f}]   exceeds and set to  max vy [{s:5.2f}]'.format(r=vy,s=maxVy))   
        vy = 3
    elif vy < -3:
        rospy.logdebug('vy [{r:5.2f}]   less than and set to  min vy [{s:5.2f}]'.format(r=vy,s=minVy))   
        vy = -3  
    
    
    if vz > 2: # preventing wobbling caused by descending with high velocity  and  moving forward at the same time
        rospy.logdebug('vz [{r:5.2f}]   exceeds and set to  max vz [{s:5.2f}], set vx=0'.format(r=vz,s=maxVz))   
        vz = 2
        vx = 0
    elif vz < -3:
        rospy.logdebug('vz [{r:5.2f}]   less than and set to   min vz [{s:5.2f}]'.format(r=vz,s=minVz))   
        vz = -3  



    
    
    setDroneMission("mov_by_vel",vx,vy,vz,duration)
   
    ezjobstatus_publisher(msg) 
     
     
    PI=3.14159265
     
  #  rospy.logdebug('turn heading in mov_by_vel=====================[{r:5.6f},{s:5.6f}]'.format(r=heading,s=heading*PI/180))   
       

 #   rospy.logdebug('vx [{t:5.2f}] vy [{s:5.2f}] vz [{r:5.2f}] heading [{u:5.2f}] duration [{d:5.2f}] '.format(t=vz,s=vz,r=vz,u=heading,d=duration))       
    task_id = drone.move_by_velocity(
        vx, vy, vz, heading*PI/180,duration, use_ned=use_ned, wait_done=False)
    

     

    if wait_done == True:
        while drone.get_task_state(task_id) != drone.TASK_DONE: 
            time.sleep(1)
  
   


def turn_heading(yaw,use_ned,wait_done): 
    drone_status = drone.system_status
    clearAllDestCtrl()
     
    setDroneMission("turn_heading",0,0,yaw,0)
    ezjobstatus_publisher(msg) 
     
  
 

    # Rotate the drone heading
    heading, use_ned = yaw, False
  
    return_code = drone.turn_heading(heading, use_ned,wait_done=True)
    
  



def land(wait_done): 
    drone_status = drone.system_status
    clearAllDestCtrl()

      
    setDroneMission("land",0,0,0,0)
    ezjobstatus_publisher(msg) 
     
  
 
    
    task_id = drone.land(auto_disarm=True, wait_done=False)
   
    task_state = drone.get_task_state(task_id)
    while task_state is not None and task_state != drone.TASK_DONE:
    
        time.sleep(1)
        task_state = drone.get_task_state(task_id)
    

 
    drone_status = drone.system_status
    rospy.loginfo('Current drone status: {}'.format(drone_status))
   
  

 
    

def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    global drone 
    global msg 
    global ezjob_pub
    global lastStatus

 
    rospy.init_node('ezdrone', anonymous=True,log_level=rospy.WARN) 


    rospy.loginfo('Connecting to drone')
    drone = EZDrone(connection_string='127.0.0.1:14557')

    drone_status = drone.system_status
  #  rospy.loginfo('Current drone status: {}'.format(drone_status))

    rospy.logwarn('\n\nstarting ezdrone=============================================================')
   # rospy.init_node('ezdrone', anonymous=True,log_level=rospy.WARN) 


    
    rospy.Subscriber("/cmd_publisher/mission", mission, onMsgCallback)
    rospy.Subscriber("/coordinator/mission", mission, onMsgCallback,queue_size=1) 

 
    msg=ezjobstatus()
    msg.header = std_msgs.msg.Header()  

    ezjob_pub = rospy.Publisher('ezflyer/ezjobstatus', ezjobstatus, queue_size=10)


    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
  
        if  drone.moving==True:
            lastStatus = drone.moving
            
            setDroneStatus("moving")
        elif  drone.moving==None:
            setDroneStatus("rest")
        else:
            if  lastStatus==True:
                setDroneStatus("accomplished")
                rospy.logwarn('mission accomplished=============================================={}\n\n'.format(lastStatus))
                lastStatus = False
            else:
                setDroneStatus("hovering")
         
            

     #   print('moving  {}'.format(drone.moving))

        setDroneKinematics()
        ezjobstatus_publisher(msg) 
     

        rate.sleep() 

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

 


def ezjobstatus_publisher(ezjobstatus):
 #   r = rospy.Rate(10) # 10hz 
    msg.header.stamp = rospy.Time.now() # Note you need to call rospy.init_node() before this will work
   
 #   print('{}'.format(ezjobstatus))

  #  print('sec={},nsec={},mission={},status={}'.format(msg.header.stamp.secs, msg.header.stamp.nsecs,msg.mission, msg.drone_status))
  #  print('des_vx={},des_vy={},des_vz={},des_dur={},des_height={},des_velocity={},vx={},vy={},vz={},roll={},pitch={},yaw={}'.format(
  #     msg.des_vx, msg.des_vy,msg.des_vz,msg.des_dur,msg.des_height,msg.des_velocity,msg.vx,msg.vy,msg.vz,msg.roll,msg.pitch,msg.yaw))

 #   print('[{},{},{},{}]'.format(msg.header.stamp.secs, msg.header.stamp.nsecs,msg.mission, msg.drone_status))

    #[sec,nsec,mission,status]des[des_vx,des_vy,des_vz,des_dur,des_height,des_velocity],K[vx,vy,vz,roll,pitch,yaw]

    PI=3.14159265

#    print('[{},{},{},{}],des[{},{},{},{},{},{}],V[{},{},{}]O[{},{},{}]H[{}]'.format(msg.header.stamp.secs, msg.header.stamp.nsecs,msg.mission, msg.drone_status,
#       msg.des_vx, msg.des_vy,msg.des_vz,msg.des_dur,msg.des_height,msg.des_velocity,msg.vx,msg.vy,msg.vz,msg.roll,msg.pitch,msg.yaw,msg.height))
   
    rospy.logdebug('[{start}][{mission},{drone_status}],des[{des_vx:5.2f},{des_vy:5.2f},{des_vz:5.2f},{des_dur:5.2f},{des_height:5.2f},{des_velocity:5.2f}],Vec[{vx:5.2f},{vy:5.2f},{vz:5.2f}]Orien[{r:5.2f},{p:5.2f},{y:5.2f}]H[{h:5.3f}]'.format(start=msg.exe_start,mission=msg.mission, 
    drone_status=msg.drone_status,
       des_vx=msg.des_vx, des_vy=msg.des_vy,des_vz=msg.des_vz,des_dur=msg.des_dur,des_height=msg.des_height,des_velocity=msg.des_velocity,
       vx=msg.vx,vy=msg.vy,vz=msg.vz,r=msg.roll/PI*180,p=msg.pitch/PI*180,y=msg.yaw/PI*180,h=msg.height))
   
    #print('[{s},{r:5.5f},{p:5.5f},{y:5.5f}]'.format(s=msg.header.stamp.secs,r=msg.roll,p=msg.pitch,y=msg.yaw))
       
    ezjob_pub.publish(ezjobstatus)
  
 

if __name__ == '__main__':
     
    listener()
    
   


 