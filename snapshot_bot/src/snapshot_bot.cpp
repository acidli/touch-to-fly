
 
#include "snapshot_bot.h"


namespace SnapshotBot {

SnapshotBot::SnapshotBot(): 
    nh_(),
    private_node_handle("~"),
    it_(nh_),
    interval(1000), count(0), isFirstMsg(1)
{    
    private_node_handle.param<int>("interval", interval, 1000);
    private_node_handle.getParam("image_in", topic_name);
    private_node_handle.getParam("path", path);

    image_sub_= it_.subscribe(topic_name, 1,&SnapshotBot::imageCallback, this) ;

}

 
SnapshotBot::~SnapshotBot(){} 
   


void SnapshotBot::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  if(isFirstMsg){
    isFirstMsg = 0;
    lastSnapshotTime = msg->header.stamp;
  }
  else {
    ros::Duration dur = msg->header.stamp - lastSnapshotTime;
    if( dur.sec < 1) {
      return ;
    }
  }

  //ROS_INFO("an image message");
  
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    count++;
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC3);

    char fn[64];
    sprintf(fn, "%s/snapshot-%04d.jpg", path.c_str(), count);
    cv::imwrite(fn, cv_ptr->image  );

    lastSnapshotTime = msg->header.stamp;
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
 


}



}
