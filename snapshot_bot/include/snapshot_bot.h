#ifndef OBJECT_TRACKING_H_
#define OBJECT_TRACKING_H_

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>

 
namespace SnapshotBot {

class SnapshotBot
{ 
public:
    SnapshotBot();
    ~SnapshotBot();

    void imageCallback(const sensor_msgs::ImageConstPtr& msg);

private:
    ros::NodeHandle nh_;
    ros::NodeHandle private_node_handle;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;

    int  interval;
    int  count;
    int  isFirstMsg;
    std::string topic_name;
    std::string path;

    ros::Time lastSnapshotTime;
};

}
 #endif
     

