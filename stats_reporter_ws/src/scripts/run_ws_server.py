#!/usr/bin/env python
import rospy

import std_msgs.msg
from std_msgs.msg import String
from std_msgs.msg import Int16
from rospy_tutorials.msg import HeaderString
from object_tracking.msg import tracked_roi
from cmd_publisher.msg import mission

from websocket_server import WebsocketServer
from threading import Thread
from time import sleep

from sensor_msgs.msg import CameraInfo

tracked_roi_reporting = tracked_roi()
tracked_roi_dirty = False

# Called for every client connecting (after handshake)
def new_client(client, server):
	print("New client connected and was given id %d" % client['id'])
	server.send_message_to_all("Hey all, a new client has joined us")


# Called for every client disconnecting
def client_left(client, server):
	print("Client(%d) disconnected" % client['id'])


# Called when a client sends a message
def message_received(client, server, message):
	if len(message) > 200:
		message = message[:200]+'..'
	print("Client(%d) said: %s" % (client['id'], message))

'''
def garbage_producer(ws_server, garbage_id):
    while(1):
        #message = "garbage message : %d" % garbage_id
        message = "{\"x\" : \"100\", \"y\" : \"150\", \"w\" : \"20\", \"h\" : \"30\"}"
        print(message)
        ws_server.send_message_to_all(message)
        garbage_id += 1
        sleep(1)
'''

def onTracked_roiCallback(data):
    global tracked_roi_reporting
    global tracked_roi_dirty
    rospy.loginfo("message tracked_roi : (x=%d, y=%d, w=%d, h=%d)\n", data.x, data.y, data.width, data.height )
    tracked_roi_reporting.x=data.x
    tracked_roi_reporting.y=data.y
    tracked_roi_reporting.width=data.width
    tracked_roi_reporting.height=data.height
    tracked_roi_dirty = True



def onCameraInfoCallback(data):
    
    rospy.loginfo("message CameraInfo : ( w=%d, h=%d)\n", data.width, data.height )
    

    Image_W = data.width
    Image_H = data.height
 

def onMsgCallback(data):
    rospy.loginfo("message mission")

def startListener(ws_server):
    rospy.loginfo('starting stats_reporter_ws')
     
    global tracked_roi_reporting
    global tracked_roi_dirty

    global Image_W 
    global Image_H 

    Image_W = -1
    Image_H = -1 

    rospy.init_node('stats_reporter_es',log_level=rospy.WARN)

    rospy.Subscriber("/object_tracking/tracked_roi", tracked_roi, onTracked_roiCallback)
    rospy.Subscriber("/coordinator/mission", mission, onMsgCallback)
    rospy.Subscriber("/camera_info/camera_info", CameraInfo, onCameraInfoCallback)
   
    
   



    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        #print("nothing special..\n")
        if (tracked_roi_dirty == True) and  not(( tracked_roi_reporting.x ==0 and tracked_roi_reporting.y==0 and
              tracked_roi_reporting.width ==0 and tracked_roi_reporting.height == 0)):
            message = "{\"type\" : \"tracked_roi\", \"x\" : \"%d\", \"y\" : \"%d\", \"w\" : \"%d\", \"h\" : \"%d\"}" % (tracked_roi_reporting.x, tracked_roi_reporting.y, tracked_roi_reporting.width, tracked_roi_reporting.height)
            rospy.loginfo("ws reporting..%s" % message)
            ws_server.send_message_to_all(message)
            tracked_roi_dirty = False
        rate.sleep()

    rospy.spin()

def wsServerStartEntry(ws_server):
    ws_server.run_forever()

if __name__ == "__main__"  :
    PORT=9001
    SERVER="0.0.0.0"

    ws_server = WebsocketServer(PORT, host="0.0.0.0")
    ws_server.set_fn_new_client(new_client)
    ws_server.set_fn_client_left(client_left)
    ws_server.set_fn_message_received(message_received)

    
    ws_thread = Thread(target = wsServerStartEntry, args=(ws_server,))
    ws_thread.daemon=True
    ws_thread.start()

    startListener(ws_server)

    #ros_thread = Thread(target = startListener)
    #ros_thread.start()

    #ws_server.run_forever()
    #ros_thread.join()

